<html class="no-js">
<head>
	<link rel="manifest"
	href="https://retina.agorafinancial.com/wp-content/plugins/onesignal-free-web-push-notifications/sdk_files/manifest.json.php?gcm_sender_id="/>
	<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>    
	<script>

	window.OneSignal = window.OneSignal || [];

	OneSignal.push( function() {
		OneSignal.SERVICE_WORKER_UPDATER_PATH = "OneSignalSDKUpdaterWorker.js.php";
		OneSignal.SERVICE_WORKER_PATH = "OneSignalSDKWorker.js.php";
		OneSignal.SERVICE_WORKER_PARAM = { scope: '/' };

		OneSignal.setDefaultNotificationUrl("https://retina.agorafinancial.com");
		var oneSignal_options = {};
		window._oneSignalInitOptions = oneSignal_options;

		oneSignal_options['wordpress'] = true;
		oneSignal_options['appId'] = '0aec3049-8f71-4a2b-aef6-4f9e59ab8280';
		oneSignal_options['autoRegister'] = true;
		oneSignal_options['httpPermissionRequest'] = { };
		oneSignal_options['httpPermissionRequest']['enable'] = true;
		oneSignal_options['welcomeNotification'] = { };
		oneSignal_options['welcomeNotification']['title'] = "Daily Reckoning";
		oneSignal_options['welcomeNotification']['message'] = "";
		oneSignal_options['path'] = "https://research.dailyreckoning.com/wp-content/plugins/onesignal-free-web-push-notifications/sdk_files/";
		oneSignal_options['safari_web_id'] = "web.onesignal.auto.514888af-c9d7-482b-90d4-9de98d872128";
		oneSignal_options['promptOptions'] = { };
		oneSignal_options['promptOptions']['exampleNotificationMessageDesktop'] = 'New Article Just Released';
		oneSignal_options['promptOptions']['exampleNotificationTitleMobile'] = 'New Article Just Released';
		oneSignal_options['promptOptions']['exampleNotificationMessageMobile'] = 'New Article Just Released';
		oneSignal_options['promptOptions']['siteName'] = 'Daily Reckoning';
		oneSignal_options['notifyButton'] = { };
		oneSignal_options['notifyButton']['enable'] = true;
		oneSignal_options['notifyButton']['position'] = 'bottom-right';
		oneSignal_options['notifyButton']['theme'] = 'default';
		oneSignal_options['notifyButton']['size'] = 'medium';
		oneSignal_options['notifyButton']['prenotify'] = true;
		oneSignal_options['notifyButton']['showCredit'] = false;
		oneSignal_options['notifyButton']['text'] = {};
		oneSignal_options['notifyButton']['text']['dialog.main.title'] = 'Daily Reckoning notification for when a new post is published';
		<!-- OneSignal: Using custom SDK initialization. -->
	});

	function documentInitOneSignal() {
		var oneSignal_elements = document.getElementsByClassName("OneSignal-prompt");

		var oneSignalLinkClickHandler = function(event) { OneSignal.push(['registerForPushNotifications']); event.preventDefault(); };        for(var i = 0; i < oneSignal_elements.length; i++)
		oneSignal_elements[i].addEventListener('click', oneSignalLinkClickHandler, false);
	}

	if (document.readyState === 'complete') {
		documentInitOneSignal();
	}
	else {
		window.addEventListener("load", function(event){
			documentInitOneSignal();
		});
	}

	var pubcode;

	function displayMessage (evt) {

		pubcode = evt.data.toUpperCase();

		OneSignal.push(function() {
            
	    //OneSignal.showHttpPrompt();
	    //OneSignal.showHttpPermissionRequest();

	    /* Never call init() more than once. An error will occur. */
	    OneSignal.init(window.oneSignal_options);

		    OneSignal.getTags(function(tags) {
		    	
		        if(typeof tags.Publication == 'string') {

		        	console.log(tags.Publication);
		            console.log(pubcode);

		            if (tags.Publication == pubcode) {

		                OneSignal.sendTag("Publication Conversion", pubcode, function (e) {
		              
		                }); 
		            }
		        }

		    });

	    });

	}

	if (window.addEventListener) {

		// For standards-compliant web browsers
		window.addEventListener("message", displayMessage, false);
	}
	else {
		window.attachEvent("onmessage", displayMessage);
	}

</script>

</head>
<body>
</body>
</html>