<?php 

class Retina_Theme {

	public $theme_dir;
    public $retina_domain;
    public $retina_root_theme_dir;
    public $retina_protocol;

	public function __construct() {
		$this->theme_dir = site_url('/wp-content/themes/retina/');
        $this->retina_protocol = getProtocol();
        $this->retina_root_theme_dir = 'https://retina.agorafinancial.com/wp-content/themes/retina/';
        $this->retina_domain =  $this->retina_protocol.$_SERVER['HTTP_HOST'];
		$this->hooks();
	}

	public function hooks()
	{
		//theme support
		add_theme_support( 'post-thumbnails' ); 
        add_action('wp_head', array($this, 'global_header_code'));
        add_action('wp_footer', array($this, 'global_footer_code'));

        //tinymce extentions
        add_filter( 'tiny_mce_before_init', array($this, 'format_TinyMCE'));

        //remove visual editor
        add_filter( 'user_can_richedit' , '__return_false', 50 );

        //change styles for admin editor
        add_action('admin_head', array($this, 'remove_ed_bar') );

        //wysiwyg overrides
        remove_filter( 'the_content', 'wpautop' );
        remove_filter( 'the_excerpt', 'wpautop' );
        remove_filter( 'the_content', 'wptexturize' );
    
        //remove wp generator tags
        remove_action('wp_head', 'wp_generator');
        remove_action ('wp_head', 'rsd_link');
        remove_action( 'wp_head', 'wlwmanifest_link');
        remove_action( 'wp_head', 'wp_shortlink_wp_head');
        remove_action( 'wp_head', 'rest_output_link_wp_head');
        remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
        remove_action( 'wp_head', 'wp_oembed_add_discovery_links');

        //remove prefetch for emoji as well... sigh...
        remove_action( 'wp_head', 'wp_resource_hints', 2 );

        //remove emoji support
        add_action( 'init', array($this, 'disable_wp_emojicons') );

	}

    public function remove_ed_bar() {
        echo '<style>
            .quicktags-toolbar {
              display:none;
            } 
          </style>';
    }

      

    public function disable_wp_emojicons() {

      //remove vc tag
      remove_action('wp_head', array(visual_composer(), 'addMetaData'));

      // all actions related to emojis
      remove_action( 'admin_print_styles', 'print_emoji_styles' );
      remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
      remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
      remove_action( 'wp_print_styles', 'print_emoji_styles' );
      remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
      remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
      remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

      // filter to remove TinyMCE emojis
      add_filter( 'tiny_mce_plugins', array ($this, 'disable_emojicons_tinymce') );
    }

    function disable_emojicons_tinymce( $plugins ) {
        if ( is_array( $plugins ) ) {
            return array_diff( $plugins, array( 'wpemoji' ) );
        } else {
            return array();
        }
    }

    public function format_TinyMCE ($in)
    {
        $in['fontsize_formats'] = "8px 10px 12px 14px 18px 24px 36px 48px 60px 72px 100px 120px 150px 175px 200px 250px 300px";
        return $in;
    }

    public function global_header_code()
    {
        $scripts = get_field('global_header_scripts', 'option');
        echo $scripts;
    }

    public function global_footer_code()
    {
        $scripts = get_field('global_footer_scripts', 'option');
        echo $scripts;
    }



    public function header_html($props = null)
    {


        global $retina_lytics;

        ?>

            <!DOCTYPE html>
            <!--[if lt IE 7]>
            <html class="no-js lt-ie9 lt-ie8 lt-ie7">
            <![endif]-->
            <!--[if IE 7]>
            <html class="no-js lt-ie9 lt-ie8">
            <![endif]-->
            <!--[if IE 8]>
            <html class="no-js lt-ie9">
            <![endif]-->
            <!--[if gt IE 8]><!-->
            <html class="no-js">
            <!--<![endif]-->

                <head>
                    <meta charset="utf-8" />
                    <meta content="True" name="HandheldFriendly" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
                    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">   

                    <title><?php wp_title( '|', true, 'right' ); ?></title>

                    <?php if (isset($props['favicon']) && $props['favicon'] == '') { ?>

                    <link id="favicon" rel="shortcut icon" type="image/png" href="/wp-content/themes/retina/images/favicon_af.ico" />

                    <?php } else { ?>

                    <link id="favicon" rel="shortcut icon" type="image/png" href="<?php echo $props['favicon']; ?>" />

                    <?php } ?>

                    <?php if ($props['controlcss']) { ?>

                    <link rel='stylesheet' href='<?php echo $this->retina_root_theme_dir; ?>stylesheets/app.css' type='text/css' media='all' />

                    <?php } else if ($props['maincss']) { ?>

                    <link rel='stylesheet' href='<?php echo $this->retina_root_theme_dir; ?>stylesheets/control.css' type='text/css' media='all' />

                    <?php } ?>

                    <!--<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js'></script>-->
                   
                    <?php if (isset($props['custom_scripts']) && $props['custom_scripts'] !== '') { ?>
                        <?php echo $props['custom_scripts']; ?>
                    <?php } ?>

                    <script>
                        // domready()
                        !function(a,ctx,b){typeof module!="undefined"?module.exports=b():typeof define=="function"&&typeof define.amd=="object"?define(b):ctx[a]=b()}("domready",this,function(a){function m(a){l=1;while(a=b.shift())a()}var b=[],c,d=!1,e=document,f=e.documentElement,g=f.doScroll,h="DOMContentLoaded",i="addEventListener",j="onreadystatechange",k="readyState",l=/^loade|c/.test(e[k]);return e[i]&&e[i](h,c=function(){e.removeEventListener(h,c,d),m()},d),g&&e.attachEvent(j,c=function(){/^c/.test(e[k])&&(e.detachEvent(j,c),m())}),a=g?function(c){self!=top?l?c():b.push(c):function(){try{f.doScroll("left")}catch(b){return setTimeout(function(){a(c)},50)}c()}()}:function(a){l?a():b.push(a)}});

                        //mobile detection
                        var isMobile = false;
                        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
                    
                        //for ajaxrequests
                        var themeAjaxUrl = '/wp-admin/admin-ajax.php',
                            retinaUrl = '<?php echo $this->retina_domain; ?>',
                            callbacks = {};

                        //lytics page tracking
                        window.jstag=function(e){var t={_q:[],_c:{},ts:(new Date).getTime()},e=!1,i=(window,document),n="/static/io",s=".min.js",r=Array.prototype.slice,a="//c.lytics.io",c="//c.lytics.io",o="io";return t.init=function(e){return c=e.url||c,s=e.min===!1?".js":s,o=e.tag||o,t._c=e,this},t.load=function(){var t,r=i.getElementsByTagName("script")[0];return e=!0,i.getElementById(n)?this:(t=i.createElement("script"),n=a+"/static/"+o+s,t.id=n,t.src=n,r.parentNode.insertBefore(t,r),this)},t.bind=function(t){e||this.load(),this._q.push([t,r.call(arguments,1)])},t.ready=function(){e||this.load(),this._q.push(["ready",r.call(arguments)])},t.send=function(){return e||this.load(),this._q.push(["ready","send",r.call(arguments)]),this},t}(),window.jstag.init({ cid: "7633a10cce24ede709377546c8e3146d", url:'//c.lytics.io', min:false }),function(t){var e=document.createElement("script");e.type="text/javascript",e.async=!0,e.src="//c.lytics.io/api/tag/"+t+"/lio.js";var i=document.getElementsByTagName("script")[0];i.parentNode.insertBefore(e,i)}("7633a10cce24ede709377546c8e3146d");

                        <?php echo $retina_lytics->get_lytics_page_tracking_values(); ?>

                    </script>


                    <?php wp_head(); ?>

                </head>

                <body>

        <?php 
    }

    public function footer_html($props = null)
    {

        if (isset($props['footertext'])) { 

            //this is where we can globally add the footer but its depreciated right now. 
            //see shortcodes

        } ?>

        <?php wp_footer(); ?>
        
        <iframe id="retina-iframe" width="0" height="0" frameborder="0" src="<?php echo WP_SITEURL; ?>/wp-content/themes/retina/iframe.php"></iframe>
        <script type='text/javascript' src='<?php echo $this->retina_root_theme_dir; ?>/bower_components/foundation-sites/dist/foundation.min.js'></script>
        <script type='text/javascript' src='<?php echo $this->retina_root_theme_dir; ?>js/app.js'></script>

        </body>
        </html>

        <?php 
    }

}

$retina_theme = new Retina_Theme;

?>