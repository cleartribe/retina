<?php 

class Retina_Shortcodes{

    public $page_query_vars;

	public function __construct() {

        $this->page_query_vars = (isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : false);

		$this->hooks();  
	}

	public function hooks()
	{

        global $if_shortcode_filter_prefix;

		//shortcodes
		add_action( 'init', array($this, 'register_shortcodes') ); 

        //enable shortcodes in raw html
        add_filter('the_content', 'do_shortcode');

        //extending "if" shortcode plugin
        add_filter($if_shortcode_filter_prefix.'lytics_user_has_pub', array($this, 'lytics_user_has_pub'));
        add_filter($if_shortcode_filter_prefix.'lytics_user_has_visted_before', array($this, 'lytics_user_has_visted_before'));
        add_filter($if_shortcode_filter_prefix.'promocode_is', array($this, 'promocode_is'));
        add_filter($if_shortcode_filter_prefix.'promocode_is_not', array($this, 'promocode_is_not'));

        //dependant script
        add_action( 'wp_enqueue_scripts', array($this, 'retina_shortcode_wp_enqueue_scripts' ));

	}

    public function retina_shortcode_wp_enqueue_scripts() {

        global $retina_theme;
        wp_register_script( 'get-waypoints', $retina_theme->theme_dir . 'bower_components/waypoints/lib/jquery.waypoints.min.js', array(), '1.0.0', false );
    }


    public function lytics_user_has_pub($pubcode)
    {

        global $retina_lytics;

        $data = $retina_lytics->lytics_entity_call('circstatus_pubcode');

        if (isset($data->circstatus_pubcode)) {
            foreach ($data->circstatus_pubcode as $key => $value) {
                if ($pubcode == $key) {
                    $actives = array('R', 'P', 'U');
                    if (in_array($value, $actives)) {
                        return true;
                    }
                }
            }
        }
    }

    public function promocode_is($promocodes)
    {

        //[if promocode_is=EWEKTA40,EWEKTA55]This is the promocode[else]This is not the promocode[/else][/if]
        $promocode_query = get_query_var('promocode');
        $promocodes = str_replace(' ', '', $promocodes);
        $promocodes = explode(",",$promocodes);
        if (!empty($promocode_query) && in_array($promocode_query, $promocodes)) {
            return true;
        }
    }

    public function promocode_is_not($promocodes)
    {

        //[if promocode_is=EWEKTA40,EWEKTA55]This is the promocode[else]This is not the promocode[/else][/if]
        $promocode_query = get_query_var('promocode');
        $promocodes = str_replace(' ', '', $promocodes);
        $promocodes = explode(",",$promocodes);
        if (empty($promocode_query) || !in_array($promocode_query, $promocodes)) {
            return true;
        }
    }

    public function lytics_user_has_visted_before()
    {

        global $retina_lytics;

        $data = $retina_lytics->lytics_entity_call('full_urls');

        if (isset($data->full_urls)) {
            $actual_link = $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI'];

            if (in_array($actual_link, $data->full_urls)) {
                return true;
            }           
        }   
    }

	
    public function register_shortcodes () 
	{
        //displays the current date in human readable format
        add_shortcode('timestamp', array($this, 'timestamp_shortcode') );

        //displays the current hostname name for dynamic urls
        add_shortcode( 'hostname', array($this, 'hostname_shortcode') );

        //displays a phone number for calling sales
        add_shortcode( 'phonenumber', array($this, 'phonenumber_shortcode') );

        //customer service email
        add_shortcode( 'customerserviceemail', array($this, 'customerserviceemail_shortcode') );

        //adds the exit pop html
        add_shortcode( 'exitpop', array($this, 'exitpop_shortcode') );

        //adds the twostep html
        add_shortcode( 'twostep', array($this, 'twostep_shortcode') );

        //adds the wistia video to the page
        add_shortcode( 'wistiavideo', array($this, 'wistiavideo_shortcode') );

        //add lazyload image
        add_shortcode( 'lazyloadimage', array($this, 'lazyloadimage_shortcode') );

        //get orderform link in its entirety
        add_shortcode( 'orderformlink', array($this, 'orderformlink_shortcode') );

        //buy link
        add_shortcode( 'buylink', array($this, 'buylink_shortcode') );

        //footer text
        add_shortcode( 'footertext', array($this, 'footertext_shortcode') );

        //get the transcript link
        add_shortcode( 'transcriptlink', array($this, 'transcriptlink_shortcode') );

        //showing advertising message
        add_shortcode( 'advertmsg', array($this, 'advertmsg_shortcode') );  

        //adding number 10 style testimonial
        add_shortcode( 'testimonial', array($this, 'testimonial_shortcode') );

        //LYTICS SPECIFIC SHORTCODES
        //first name
        add_shortcode( 'firstname', array($this, 'firstname_shortcode') );

        //city
        add_shortcode( 'city', array($this, 'city_shortcode') );
	}


    public function timestamp_shortcode($args)
    { 
        return date("F j, Y");
    }

    public function hostname_shortcode($atts) 
    {
       return $_SERVER['HTTP_HOST'];
    }


    public function phonenumber_shortcode($atts) 
    {
       global $post;
       $publication_id = get_field('publication', $post->ID);
       return get_field('pubphone', $publication_id );
    }

    public function customerserviceemail_shortcode($atts)
    {
       global $post;
       $productid = get_field('publication', $post->ID);
       $frachiseid = get_field('pubfranchise', $productid);
       return get_field('customer_support_email_address', $frachiseid);
    }

    public function footertext_shortcode($atts)
    {
       global $post;
       $productid = get_field('publication', $post->ID);
       $frachiseid = get_field('pubfranchise', $productid);
       return get_field('footertext', $frachiseid);
    }
       
       

    public function exitpop_shortcode($atts) 
    {

        global $post, $retina_promo;

         $html ='<div class="reveal" id="exitPop" data-reveal>';
            $html .= '<p><span style="font:24px bold;font-family:impact;">Wait!</span> By clicking out of this page you\'ll forfeit the chance to learn about this exciting and exclusive opportunity.</p>';

            $html .= '<p class="exp">Click <span style="text-decoration:underline">Watch Video</span> to continue watching.</p>';

            $html .= '<p class="exp">Click <span style="text-decoration:underline">Read Transcript</span> to read about this opportunity.</p>';

            $html .= '<p class="exp">Or call <span id="pubPhone">'.do_shortcode('[phonenumber]').'</span> to speak more about this opportunity with one of our professional customer care representatives.</p>';

            $html .= '<a class="watch-video button large green">'; 
                $html .= '<img class="button-icon" src="/wp-content/themes/retina/images/playbutton.svg" />';
                $html .= 'Watch Video';
            $html .= '</a>';

            $html .= '<a href="'.do_shortcode('[transcriptlink]').'" class="read-trans button large orange">'; 
                $html .= '<img class="button-icon" src="/wp-content/themes/retina/images/glasses.svg" />';
                $html .= 'Read Transcript';
            $html .= '</a>';


           $html .='<button class="close-button" data-close aria-label="Close modal" type="button">';
             $html .='<span aria-hidden="true">&times;</span>';
           $html .='</button>';
         $html .='</div>';
       
        $show_exit_pop = $retina_promo->get_exitpop_settings($post->ID);

        $promocode = get_query_var('promocode');

        if ($show_exit_pop !== false) {

            if (!in_array($promocode, $show_exit_pop)) {

                return $html;
            }
        }
    }

    public function twostep_shortcode($atts)
    {

        global $post;

        $email = (isset($_GET['email']) ? $_GET['email'] : '');
        $twostep_img_url = get_field('two_step_image', $post->ID);

        $html = '<div class="reveal" id="twoStep" data-animation-in="scale-in-up" aria-labelledby="twoStepHeading" data-reveal>';
            $html .= '<h3 id="twoStepHeading">Reserve Your Order by<br>';
            $html .= 'Entering Your Email Below</h3>';
            $html .= '<input class="large" id="emailEnter" placeholder="Enter Email Address" value="'.$email.'" name="user_email" type="email">';
            $html .= '<a class="button large golden" id="emailEnterBtn" href="'.do_shortcode('[orderformlink]').'">Click to Proceed</a>';

            if ($twostep_img_url != '')
                $html .= '<p><img class="image_center" src="'.$twostep_img_url.'" width="99%"></p>';

            $html .= '<button class="close-button" data-close aria-label="Close Popup" type="button">';
            $html .= '<span aria-hidden="true">&times;</span>';
            $html .= '</button>';
        $html .= '</div>';

        return $html;
    }

    public function wistiavideo_shortcode ($atts)
    {

        extract(shortcode_atts(array(
          'autoplay' => "true",
          'playbar' => "false",
          'fullscreen' => "false",
          'playbutton' => "true",
          'videoid' => "diwezfgilg",
          'crosstime' => "false",
          'mobiletranscript' => "true",
          "title" => "Subscribe Today",
          "disclaimer" => "false"
        ), $atts));


        if ($disclaimer != 'false') {
            $disclaimer = "<p class='disclaimer unhide' style='text-align: center;'>".$disclaimer."</p>";
        }
        else {
            $disclaimer = '';
        }

        $html = '<!--wistia-->';
        $html .= '<script charset="ISO-8859-1" src="https://fast.wistia.com/assets/external/E-v1.js"></script>';
        $html .= '<div class="wistia_responsive_wrapper" style="max-height:360px;max-width:640px; margin:0 auto; margin-bottom: 2em; -webkit-box-shadow: 0px 0px 26px 0px rgba(50, 50, 50, 1);-moz-box-shadow: 0px 0px 26px 0px rgba(50, 50, 50, 1);box-shadow: 0px 0px 26px 0px rgba(50, 50, 50, 1);">';
        $html .= '<div class="wistia_embed wistia_async_'.$videoid.' videoFoam=true playbar='.$playbar.' fullscreenButton='.$fullscreen.'  playButton='.$playbutton.' autoPlay='.$autoplay.'" style="max-width:640px;max-height:360px;">&nbsp;</div>';
        $html .= '</div>';
        $html .= '<!--=== / ^^ Change the Wistia ID above. ===--><!-- Wistia, enter button, 2step --><script>';
            $html .= 'jQuery( document ).ready(function($) {';
                $html .= 'document.addEventListener("retinajs_ready", fireWistia, false);';
            
            $html .= 'function fireWistia() {';
                $html .= '$.triggerWistiaVideo("'.$videoid.'", "'.$crosstime.'", "'.$mobiletranscript.'");';
            $html .= '}';
        $html .= '});';
        $html .= '</script>';
        
        $html .= do_shortcode('[buylink title="'.$title.'" class="unhide subnow"]');
        $html .= $disclaimer;

        if ($mobiletranscript == "true") {
            $html .= '<p class="centered"><a href="'.do_shortcode('[transcriptlink]').'" class="read-trans unhide button large orange">'; 
                $html .= '<img class="button-icon" src="/wp-content/themes/retina/images/glasses.svg" />';
                $html .= 'Read Transcript';
            $html .= '</a></p>';
        }

        return $html;
    }

    public function lazyloadimage_shortcode ($atts)
    {
        extract(shortcode_atts(array(
          'imgsrc' => 'http://placehold.it/500x100',
          'class' => "",
        ), $atts));

        global $post, $retina_promo;

        $imgid = randomString();

        wp_enqueue_script( 'get-waypoints' );

        $imgsrc = wp_get_attachment_image_url($imgsrc, 'full');

        $html = '<script>';
            $html .= 'jQuery(window).load(function(){';
        
                $html .= 'var waypoints = $("p.'.$imgid.'").waypoint({';
                  $html .= 'handler: function(direction) {';
                    $html .= '$("img#'.$imgid.'").attr("src", $("img#'.$imgid.'").attr("src"));';
                    $html .= '$("img#'.$imgid.'").fadeIn("slow")';
                   
                  $html .= '}, offset: "75%"';
                $html .= '})';

            $html .= '})';

        $html .= '</script>';

        $html .= '<p style="margin:0 auto; text-align:center" class="'.$imgid.' '.$class.'"><img style="display:none;" id="'.$imgid.'" src="'.$imgsrc.'" /></p>';

        return $html;
    }


    public function buylink_shortcode ($atts)
    {
        extract(shortcode_atts(array(
          'title' => "Subscribe Today",
          'imgsrc' => 'false',
          'twostepoverride' => 'false',
          'class' => 'subnow',
          'disclaimer' => 'false'
        ), $atts));

        global $post, $retina_promo;

        $show_two_step = $retina_promo->get_twostep_settings($post->ID);

        $promocode = get_query_var('promocode');

        if ($disclaimer != 'false') {
            $disclaimer = "<p style='text-align: center;'>".$disclaimer."</p>";
        }
        else {
            $disclaimer = '';
        }

        if (isset($show_two_step) && $twostepoverride != "true") {

            if (!in_array($promocode, $show_two_step)) {

                if ($imgsrc != 'false') {
                    return '<img data-open="twoStep" class="twosteplink famshot '.$class.'" src="'.$imgsrc.'">';
                }
                else {
                    return '<a data-open="twoStep" class="twosteplink '.$class.'">'.$title.'</a>'.$disclaimer;
                }
            }
        }

        if ($imgsrc != 'false') {
            return '<a href="'.do_shortcode('[orderformlink]').'"><img alt="famshot '.$class.'" class="famshot" src="'.$imgsrc.'"></a>';
        }

        else {
            return '<a href="'.do_shortcode('[orderformlink]').'" class="'.$class.'">'.$title.'</a>'.$disclaimer; 
        }
        
        
    }


    public function orderformlink_shortcode($atts)
    {
        global $post;

        $no_code = get_field('no_code', $post->ID);
        $campaign_name = get_query_var('n');

        parse_str($this->page_query_vars, $get_array);

        if (array_key_exists('n', $get_array)) {
            $campaign_name = $get_array['n'];
        }
        else {
            $campaign_name = 'NO_CAMPAIGN_NEED_FIXING';
        }

        //get the promocode from the url
        $promocode = get_query_var('promocode');

        if ( $promocode ) {
            return 'https://pro.agorafinancial.com/o/' . $campaign_name . '/' . $promocode . '/' . $this->page_query_vars;
        }
        else {
            return 'https://pro.agorafinancial.com/o/' . $campaign_name . '/' . $no_code . '/' . $this->page_query_vars;
        }

    }


    public function transcriptlink_shortcode($atts)
    {
        global $post;

        //get the transcript promo
        $transcript_promo = get_field('transcript_promo', $post->ID);

        //get the transcript promo slug
        $transcript_link = get_post_field( 'post_name', $transcript_promo );

        //get current dynamic host name
        $hostname = do_shortcode('[hostname]');

        //get the promocode from the url
        $promocode = get_query_var('promocode');

        if ( $promocode ) {
            return 'https://' . $hostname . '/' . $transcript_link . $this->page_query_vars;
        }
        else {
            return 'https://' . $hostname . '/' . $transcript_link . $this->page_query_vars;
        }
    }

    public function advertmsg_shortcode ($atts)
    {
        global $post, $retina_promo;

        $advertmsg = get_post_field( 'advertisement_message', $post->ID );
        
        if ($advertmsg == '') {
            $advertmsg = 'Advertorial';
        }

        $show_advertmsg = $retina_promo->get_advertmsg_settings($post->ID);

        $promocode = get_query_var('promocode');

        if ($show_advertmsg) {
            if (in_array($promocode, $show_advertmsg)) {
                return '<h3 class="advertmsg">'.$advertmsg.'</h3>';
            }
        }
        
    }

    public function testimonial_shortcode ($atts, $content)
    {
        return '<p class="testimonial">' . $content . '</p>';
    }


    public function firstname_shortcode($atts)
    {

        global $retina_lytics;

        extract(shortcode_atts(array(
          'default' => "Reader"
        ), $atts));


        $data = $retina_lytics->lytics_entity_call('first_name');

        if (isset($data->first_name)) 
            return ucfirst(strtolower($data->first_name));
        else 
            return $default;

    }

    public function city_shortcode($atts)
    {

        global $retina_lytics;

        extract(shortcode_atts(array(
          'default' => "The United States"
        ), $atts));


        $data = $retina_lytics->lytics_entity_call('city');

        if (isset($data->city)) 
            return $data->city;
        else 
            return $default;

    }

}

$retina_shortcodes = new Retina_Shortcodes;

?>