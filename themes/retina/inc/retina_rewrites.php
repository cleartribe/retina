<?php 

class Retina_Rewrites{

	public function __construct() {
		$this->hooks();  
	}

	public function hooks()
	{
		//add new query params
        add_filter('query_vars', array($this, 'setQueryVars'));

        //remove s query param modified from:
        //http://zurb.com/forrst/posts/How_to_replace_the_default_search_query_paramete-Nhm
        add_filter('query_string', array($this, 'remove_search_parameter') );

	}


    public function setQueryVars($vars)
    {

        $vars[] = 'promocode';
        $vars[] = 'postname';
        return $vars;
    }

    public function rewriteRules($rules)
    {
        global $wp_rewrite;

        $new_rules = array(

            '([\s\S]+)/([\s\S]+)/?$' => 'index.php?postname=' . $wp_rewrite->preg_index(1) . '&promocode=' . $wp_rewrite->preg_index(2),
        );

        $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;

        return $wp_rewrite->rules;
    }




    // populate s parameter with value of search
    public function remove_search_parameter($query_string) {

        $query_string_array = array();

        // convert the query string to an array
        parse_str($query_string, $query_string_array); 
        
        // if "search" is in the query string
        if(isset($query_string_array['s'])){
            unset($query_string_array['s']); // delete "search" from query string
        }
            
        return http_build_query($query_string_array, '', '&'); // Return our modified query variables
    }
 


}

$retina_rewrites = new Retina_Rewrites;

?>