<?php 

class Retina_Custom_Post_Types {

    public function __construct() {
        $this->hooks();
    }

    public function hooks() {
        add_action('init', array($this, 'publication') );
        add_action('init', array($this, 'franchise') );
    }


    public function publication()
    {
        $labels = array(
            'name' => _x('Publications', 'publication'),
            'singular_name' => _x('Publication', 'publication'),
            'add_new' => _x('Add New', 'publication'),
            'add_new_item' => _x('Add New Publication', 'publication'),
            'edit_item' => _x('Edit Publication', 'publication'),
            'new_item' => _x('New Publication', 'publication'),
            'view_item' => _x('View Publication', 'publication'),
            'search_items' => _x('Search Publications', 'publication'),
            'not_found' => _x('No publications found', 'publication'),
            'not_found_in_trash' => _x('No publications found in Trash', 'publication'),
            'parent_item_colon' => _x('Parent Publication:', 'publication'),
            'menu_name' => _x('Publications', 'publication'),
        );

        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'description' => 'Publications',
            'supports' => array('title'),
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 20,
            'show_in_nav_menus' => true,
            'publicly_queryable' => false,
            'exclude_from_search' => true,
            'has_archive' => false,
            'query_var' => true,
            'can_export' => true,
            'rewrite' => array(
                'slug' => 'publications',
                'with_front' => false,
                ),
            'capability_type' => 'post',
            'menu_icon' => 'dashicons-media-default'
        );

        register_post_type('publication', $args);

    }

    public function franchise()
    {
        $labels = array(
            'name' => _x('Franchises', 'franchise'),
            'singular_name' => _x('Franchise', 'franchise'),
            'add_new' => _x('Add New', 'franchise'),
            'add_new_item' => _x('Add New Franchise', 'franchise'),
            'edit_item' => _x('Edit Franchise', 'franchise'),
            'new_item' => _x('New Franchise', 'franchise'),
            'view_item' => _x('View Franchise', 'franchise'),
            'search_items' => _x('Search Franchises', 'franchise'),
            'not_found' => _x('No publications found', 'franchise'),
            'not_found_in_trash' => _x('No publications found in Trash', 'franchise'),
            'parent_item_colon' => _x('Parent Franchise:', 'franchise'),
            'menu_name' => _x('Franchises', 'franchise'),
        );

        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'description' => 'Franchises',
            'supports' => array('title'),
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 20,
            'show_in_nav_menus' => true,
            'publicly_queryable' => false,
            'exclude_from_search' => true,
            'has_archive' => false,
            'query_var' => true,
            'can_export' => true,
            'rewrite' => array(
                'slug' => 'franchises',
                'with_front' => false,
                ),
            'capability_type' => 'post',
            'menu_icon' => 'dashicons-media-default'
        );

        register_post_type('franchise', $args);

    }

}

$retina_cpt = new Retina_Custom_Post_Types;

?>