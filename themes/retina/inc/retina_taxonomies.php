<?php 

class Retina_Taxonomies {

    public function __construct() {
        $this->hooks();
    }

    public function hooks() {
        add_action( 'init', array($this, 'type') );
    }


    public function type () {

        $labels = array(
            'name'              => _x( 'Type', 'taxonomy general name' ),
            'singular_name'     => _x( 'Type', 'taxonomy singular name' ),
            'search_items'      => __( 'Search Types' ),
            'all_items'         => __( 'All Types' ),
            'parent_item'       => __( 'Parent Type' ),
            'parent_item_colon' => __( 'Parent Type:' ),
            'edit_item'         => __( 'Edit Type' ),
            'update_item'       => __( 'Update Type' ),
            'add_new_item'      => __( 'Add New Type' ),
            'new_item_name'     => __( 'New Type Name' ),
            'menu_name'         => __( 'Types' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'type' ),
        );

        register_taxonomy( 'types', array( 'post' ), $args );
    }

}

$retina_taxonomies = new Retina_Taxonomies;

?>