<?php 
class Retina_VC {

    public function __construct() {
        $this->hooks();
    }

    public function hooks()
    {
        add_action( 'vc_before_init', array($this, 'register_vc_params'));

    }


    public function register_vc_params()
    {
    
        vc_map( array(
           "name" => __("Lazy Load Image"),
           "base" => "lazyloadimage",
           "category" => __('Content'),
           "params" => array(
                array(
                    "type" => "attach_image",
                    "heading" => __( "Link Image" ),
                    "param_name" => "imgsrc",
                    "value" => __( "http://placehold.it/1000x300" ),
                    "description" => __( "Choose an image to lazy load?" )
                ),  
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Class Name" ),
                    "param_name" => "class",
                    "value" => __( "" ),
                    "description" => __( "Optional Class to be applied to image wrapper" )
                ),           
            )
        ) );

    }



} 

$retina_vc = new Retina_VC;