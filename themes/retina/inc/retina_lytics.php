<?php 

class Retina_Lytics{

	private $token;
	private $callprops;
	private $callargs;

	public function __construct() {
		$this->token = 'aW1Cmz6lLAmZM2o47cC9ngxx';
		$this->callprops = $this->get_lytics_call_props();
		$this->callargs = array ( 'method' => 'GET', 'timeout' => 120, 'Content-Type' => 'application/json' );

		$this->hooks();
	}


	public function hooks()
	{
		//ajax methods
        add_action('wp_ajax_nopriv_lytics_entity_call', array($this, 'ajax_lytics_entity_call'));
        add_action('wp_ajax_lytics_entity_call', array($this, 'ajax_lytics_entity_call'));  
	}

	/*
	This gives you details on all content segments
	*/
	public function get_topics_list() {
		//getting all session data
		$response = wp_remote_request ( 'https://api.lytics.io/api/content/doc?access_token='.$this->token, $this->callargs );

		$responsebody = json_decode($response['body']);

		return $responsebody->data;	
	}

	/*
	This gets all content segments and the scheme info
	*/
	public function get_segments_list() {
		//getting all session data
		$response = wp_remote_request ( 'https://api.lytics.io/api/schema/content/fieldinfo?access_token='.$this->token, $this->callargs );

		$responsebody = json_decode($response['body']);

		return $responsebody->data;	
	}

	
	/*
		You can use this call to get articles or promos that lytics says a user is interested in
	*/
	public function lytics_content_recommend_call()
	{

		if (!$this->callprops)
			return false;

		//getting all session data
		$response = wp_remote_request ( 'https://api.lytics.io/api/content/recommend/user/'.$this->callprops['method'].'/'.$this->callprops['value'].'?access_token='.$this->token.'&contentsegment=&wait=false&from='.date('Y-m-d', strtotime('-10 days')).'&to='.date('Y-m-d'), $this->callargs );

		$responsebody = json_decode($response['body']);


		return $responsebody->data;	
	}

	/*
		This is a user level call to the lytics api for getting various user properties. 
		If you want to get an idea of the properties you can pass into this just make the call without any fields and it will give you the kitchen sink. 
	*/
	public function lytics_entity_call($fields = '')
	{
		if (!$this->callprops)
			return false;

		//getting all session data
		$response = wp_remote_request ( 'https://api.lytics.io/api/entity/user/'.$this->callprops['method'].'/'.$this->callprops['value'].'?access_token='.$this->token.'&fields='.$fields.'&segments=false&meta=false', $this->callargs );

		$responsebody = json_decode($response['body']);

		return $responsebody->data;	
	}


	/*
		This is a wrapper for the lytics_entity_call to support calling it from ajax
	*/
	public function ajax_lytics_entity_call()
	{


		$fields = '';

		$this->callprops = $this->get_lytics_call_props();

		//get the fields from post
		if (isset($_POST['fields']) && !empty($_POST['fields']))
			$fields = $_POST['fields'];
		
		//get email address if applicable
		if (!empty($_POST['email'])) {
			$this->callprops['method'] = 'email';
			$this->callprops['value'] = $_POST['email'];
		}
		//get uid for testing if applicable
		else if (!empty($_POST['uid'])) {
			$this->callprops['method'] = '_uid';
			$this->callprops['value'] = $_POST['uid'];
		}

		//make the entity call and return data
		$results = $this->lytics_entity_call($fields);

		echo json_encode($results);
		exit;
	}

	/*
	Use this to return the lytics tag to track a users email if present in the query string
	*/
	public function get_lytics_page_tracking_values()
	{
		if (isset($_GET['email'])) {
			return  'window.jstag.send({ email: "'.$_GET['email'].'" });';
		}
		
	}

	/*
	use this internally to figure out if there is either an email or uid in the query string OR a uid (seerid) set as a cookie. this is used in other calls to the lytics api to get user information
	*/
	private function get_lytics_call_props()
	{
		$callprops = array();

		//assign method for getting user
		if (isset($_GET['email'])) {
			$callprops['method'] = 'email';
			$callprops['value'] = $_GET['email'];
			return $callprops;
		}
		else if (isset($_GET['uid'])) {
			$callprops['method'] = '_uid';
			$callprops['value'] = $_GET['uid'];
			return $callprops;
		}
		else if (isset($_COOKIE['seerid'])) {
			$callprops['method'] = '_uid';
			$callprops['value'] = $_COOKIE['seerid'];
			return $callprops;
		}
		else {
			return false;
		}
	}

}

$retina_lytics = new Retina_Lytics;

?>