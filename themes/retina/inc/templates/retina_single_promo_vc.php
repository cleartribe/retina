<?php 

class Retina_Single_Promo_VC extends Retina_Single_Promo {

    public function build_template()
    {
        global $retina_theme;

        $this->main_props['controlcss'] = true;

        $retina_theme->header_html($this->header_props);
        $this->main_html($this->main_props);
        $retina_theme->footer_html();

    }

    public function main_html($props)
    {

        global $wp_query; ?>
     
        <div class="contentwrapper">

            <?php if ($props['show_advert_msg']) { echo do_shortcode('[advertmsg]'); } ?>
        
            <div class="row">
                <div class="small-12 medium-10 medium-centered columns">

                    <?php 
                    if ($wp_query->have_posts() ) {
                        while ( $wp_query->have_posts() ) {

                            the_post(); 
                            
                            the_content();

                        }
                    } ?>

                </div>
            </div> 

        </div>

    <?php 
       
    }

}