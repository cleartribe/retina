<?php 

class Retina_Single_Promo {

    private $base_stylesheet;
    private $show_advert_msg;
    private $show_disclaimer_msg;
    private $show_exit_pop;
    private $show_two_step;
    private $custom_scripts;


    public function __construct() 
    {
        $this->set_data();
        $this->hooks();
        $this->build_template();
    }


    private function set_data ()
    {
        global $post, $retina_promo;

        //getting product that we are selling and data associated with it from franchise and publication CPTs.
        $productid = get_field('publication', $post->ID);
        $franchiseid = get_field('pubfranchise', $productid);
        $this->pubcode = get_field('pubcode', $productid);
        $this->favicon = get_field('favicon', $franchiseid);
        $this->custom_scripts = get_field('custom_scripts', $franchiseid);

        //getting promo settings
        $this->show_advert_msg = $retina_promo->get_advertmsg_settings($post->ID);
        $this->show_disclaimer_msg = $retina_promo->get_disclaimermsg_settings($post->ID);
        $this->show_exit_pop = $retina_promo->get_exitpop_settings($post->ID);
        $this->show_two_step = $retina_promo->get_twostep_settings($post->ID);

        //passing properties to sections within template
        $this->header_props = array (
            'favicon' => $this->favicon, 
            'custom_scripts' => $this->custom_scripts,
            'maincss' => true,
            'controlcss' =>  false
        );

        $this->main_props = array (
            'show_advert_msg' => $this->show_advert_msg,
            'show_disclaimer_msg' => $this->show_disclaimer_msg
        );

    }

    public function hooks ()
    {
        //adding extras to footer
        add_action( 'wp_footer', array($this, 'add_footer_page_options') );
    }

    public function add_footer_page_options ()
    {
        if (isset($this->show_two_step)) {
            echo do_shortcode('[twostep]');
        }

        if (isset($this->show_exit_pop)) {
            echo do_shortcode('[exitpop]');
        }
    }


    public function build_template()
    {
        global $retina_theme;
        
        $retina_theme->header_html($this->header_props);
        $this->main_html($this->main_props);
        $retina_theme->footer_html();

    }

    public function main_html($props)
    {

        global $wp_query;
     
        if ($props['show_advert_msg']) { echo do_shortcode('[advertmsg]'); } 
            
        if ($wp_query->have_posts() ) {
            while ( $wp_query->have_posts() ) {

                the_post(); 
                
                the_content();

            }
        } 
                 
    }

}


?>