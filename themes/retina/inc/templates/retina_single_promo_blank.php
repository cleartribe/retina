<?php 

class Retina_Single_Promo_Blank extends Retina_Single_Promo {


    public function build_template()
    {
        global $retina_theme;

        $this->header_props['maincss'] = false;

        $retina_theme->header_html($this->header_props);
        $this->main_html($this->main_props);
        $retina_theme->footer_html();

    }

    public function main_html($props)
    {

        global $wp_query;
     
        if ($props['show_advert_msg']) { echo do_shortcode('[advertmsg]'); } ?>
            
        <?php 
        if ($wp_query->have_posts() ) {
            while ( $wp_query->have_posts() ) {

                the_post(); 
                
                the_content();

            }
        } 
       
    }

}


?>