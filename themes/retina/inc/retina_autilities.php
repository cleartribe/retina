<?php 

//helper functions
if (!function_exists('isSSL')) {
    
    /**
     * @param string $html
     * @param string $html
     */
    function isSSL()
    {
        return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443)
            ? true
            : false;
    } 
}

//return http or https
if (!function_exists('getProtocol'))
{
    function getProtocol() {
        return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

    }
}



if (!function_exists('randomString'))
{
    function randomString($type = 'alnum', $len = 8)
    {
        // default
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        switch($type)
        {
            case 'basic'    : return mt_rand();
            case 'alnum'    : $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; break;
            case 'numeric'  : $pool = '0123456789'; break;
            case 'nozero'   : $pool = '123456789'; break;
            case 'alpha'    : $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; break;
            // don't include lower-case, don't include similar chars like 0 & O or I, L, and 1
            case 'non-confuse'    : $pool = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789'; break;
            case 'unique'   : return md5(uniqid(mt_rand()));
            case 'md5'      : return md5(uniqid(mt_rand()));
        }

        $str = '';
        for ($i=0; $i < $len; $i++)
        {
            $str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
        }

        return $str;
    }
}

?>