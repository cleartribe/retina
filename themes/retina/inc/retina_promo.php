<?php 

class Retina_Promo{

	public function __construct() {
   
	}

    public function get_exitpop_settings ($postID)
    {
        $show_exit_pop = get_field('show_exit_pop', $postID);

        if ($show_exit_pop) {

            $ep_promocodes = array();

            if ( have_rows('hide_exit_pop') ) {

                // loop through the rows of data
                while ( have_rows('hide_exit_pop') ) : the_row();

                    // display a sub field value
                    array_push($ep_promocodes, get_sub_field('ep_promocode'));

                endwhile;
            }

            return $ep_promocodes;
        }

        else {

            return false;
        
        }
    }

    public function get_twostep_settings ($postID)
    {
        $show_two_step = get_field('show_two_step', $postID);

        if ($show_two_step) {

            $ts_promocodes = array();

            if ( have_rows('hide_two_step') ) {

                // loop through the rows of data
                while ( have_rows('hide_two_step') ) : the_row();

                    // display a sub field value
                    array_push($ts_promocodes, get_sub_field('ts_promocode'));

                endwhile;
            }

            return $ts_promocodes;
        }

        else {
            return null;
        }
    }

    public function get_advertmsg_settings ($postID)
    {
        $show_advert_msg = get_field('show_advertisement_message', $postID);

        if ($show_advert_msg) {

            $av_promocodes = array();

            if ( have_rows('advertisement_promocodes') ) {

                // loop through the rows of data
                while ( have_rows('advertisement_promocodes') ) : the_row();

                    // display a sub field value
                    array_push($av_promocodes, get_sub_field('av_promocode'));

                endwhile;
            }

            return $av_promocodes;
        }

        else {

            return null;
        
        }
    }


    public function get_disclaimermsg_settings ($postID)
    {
        $show_disclaimer_msg = get_field('show_disclaimer', $postID);

        if ($show_disclaimer_msg) {

            $dc_promocodes = array();

            if ( have_rows('disclaimer_promocodes') ) {

                // loop through the rows of data
                while ( have_rows('disclaimer_promocodes') ) : the_row();

                    // display a sub field value
                    array_push($dc_promocodes, get_sub_field('dc_promocode'));

                endwhile;
            }

            return $dc_promocodes;
        }

        else {
            return false;
        }
    }

}

$retina_promo = new Retina_Promo;

?>