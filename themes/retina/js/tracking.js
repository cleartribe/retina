<script>

/*
//insert afga_page_props in your page body if you want to track additional page metrics

var afga_page_props = {
    is_logged_in : 'false',
    tags : 'tag:specific, sent:sentimenttag',
    authors : 'Jim Rickards',
    categories : 'Promo Page, Order Form, Etc',
    post_type : 'page'
}



//track pubs specific to promo
var promopub = 'CWA';

//to insert events use this convention
afga.send('event', {
    'eventCategory' : 'category',
    'eventAction' : 'action',
    'eventLabel' : 'label'
});

*/
var profiles_server = 'https://profiles.agorafinancial.com/';

function ProfilesTracker () {

    //cookie functions
    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    function setCookie (name,value,days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
    }

    function deleteCookie(name) {
        setCookie(name,"",-1);
    }
  
  
    //get url parameter from url
    function getUrlParam(e) {e=e.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");var n=new RegExp("[\\?&]"+e+"=([^&#]*)"),r="";if(typeof t==="undefined"){r=n.exec(location.search)}else{r=n.exec(t)}return r==null?null:decodeURIComponent(r[1].replace(/\+/g," "))}

    //object check
    function isEmptyObject(obj) {
        for(var prop in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                return false;
            }
        }
        return true;
    }

    //initialize lytics tracking
    window.jstag=function(e){var t={_q:[],_c:{},ts:(new Date).getTime()},e=!1,i=(window,document),n="/static/io",s=".min.js",r=Array.prototype.slice,a="//c.lytics.io",c="//c.lytics.io",o="io";return t.init=function(e){return c=e.url||c,s=e.min===!1?".js":s,o=e.tag||o,t._c=e,this},t.load=function(){var t,r=i.getElementsByTagName("script")[0];return e=!0,i.getElementById(n)?this:(t=i.createElement("script"),n=a+"/static/"+o+s,t.id=n,t.src=n,r.parentNode.insertBefore(t,r),this)},t.bind=function(t){e||this.load(),this._q.push([t,r.call(arguments,1)])},t.ready=function(){e||this.load(),this._q.push(["ready",r.call(arguments)])},t.send=function(){return e||this.load(),this._q.push(["ready","send",r.call(arguments)]),this},t}(),window.jstag.init({ cid: "7633a10cce24ede709377546c8e3146d", url:'//c.lytics.io', min:false }),function(t){var e=document.createElement("script");e.type="text/javascript",e.async=!0,e.src="//c.lytics.io/api/tag/"+t+"/lio.js";var i=document.getElementsByTagName("script")[0];i.parentNode.insertBefore(e,i)}("7633a10cce24ede709377546c8e3146d");


    //initialize google analytics external script
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  

    /*  load profiles.compressed.js
    asynchronously load profiles script incase server is slow
    */
    (function() {
        var ce = document.createElement('script'),
        s = document.getElementsByTagName('script')[0],
        version = (typeof profilesVersion != 'undefined') ? profilesVersion : '1.0.0';

        ce.type = 'text/javascript';
        ce.async = true;
        version = '?v=' + version;
        ce.src = 'https://profiles.agorafinancial.com/js/profiles.compressed.js' + version;

        if (ce.addEventListener) {
            ce.addEventListener("load", function() { fire_profiles(); }, false);          
        } else if (ce.attachEvent) {
            ce.attachEvent("onload", function() { fire_profiles(); });          
        }

        function fire_profiles() {
            if (typeof profiles_script_callback != 'undefined' ){
                profiles_script_callback();           
            }
        }

        s.parentNode.insertBefore(ce, s); 

    })();

    
    function profilesIsReady(callback) {

        var sto = {},
        polltries = 0,
        profilesReady = false;

        function pollProfiles(callback) {

            sto = window.setTimeout(function() {
                if (typeof Profiles == 'object' || polltries === 300) {
                    callback();
                    // shut down settimeout
                    window.clearTimeout(sto);
                    sto = null;
                    return;
                }

                polltries++;
                pollProfiles(callback);

            }, 10);
        }

        pollProfiles(callback);
    }

    // must load profile data in callback
    function profiles_script_callback() {
      
       profilesIsReady(function () {

          if (typeof afga_page_props === 'undefined') {
              afga_page_props = false;
          }
          
          //get IDs assigned through profiles iframe
          //CAUSES ISSUES RIGHT NOW WITH JQUERY DISABLING
          //Profiles.getID();
         
          //initiate afga to send to warehouses
          afga.init(afga_page_props);
         
          //initiate lytics page load variable send
          lyticsPageLoad();

          //initiate event trackers
          scrollReach();
          wistiaReach();
          exitPop();
          twoStep();
          orderForm();
          orderCompletion();
         
          
        });
    }


    function exitPop () {

        var poplimit = false;

        //on exit - limiting to one exit. 
        $('body').on('lytics_event', '#popmessage', function () {

            if (poplimit)
                return;

            poplimit = true;

            afga.send('event', {
               'eventCategory' : 'Exit Pop',
               'eventAction' : 'Open',
               'eventLabel' : 'Exit Pop | Open | '+pagename
            });
        })


        //on close
        $('#exitButton').on('click', function () {
            afga.send('event', {
                'eventCategory' : 'Exit Pop',
                'eventAction' : 'Close - X',
                'eventLabel' : 'Exit Pop | Close - X | '+pagename
            });
        })

        //on close from watch video
        $('#watch_video').on('click', function () {
            afga.send('event', {
                'eventCategory' : 'Exit Pop',
                'eventAction' : 'Close - Watch Video',
                'eventLabel' : 'Exit Pop | Close - Watch Video | '+pagename
            });
        })
        
        //read transcript
        $('#read_transcript').on('click', function () {
            afga.send('event', {
                'eventCategory' : 'Exit Pop',
                'eventAction' : 'Transcript',
                'eventLabel' : 'Exit Pop | Transcript | '+pagename
            });
        })

        //read transcript for mobile and external ad networks
        $('.read_transcript_2').on('click', function () {
            afga.send('event', {
                'eventCategory' : 'Mobile - External Ad Network',
                'eventAction' : 'Transcript',
                'eventLabel' : 'Mobile - External Ad Network | Transcript | '+pagename
            });
        })


    }

    function twoStep()
    {

        var stepopenlimit = 0,
            fieldfocuslimit = 0;

        $(".pop").on('click', function () {
            if (stepopenlimit == 0) {
                afga.send('event', {
                    'eventCategory' : '2 Step',
                    'eventAction' : 'Open',
                    'eventLabel' : '2 Step | Open | '+pagename
                });
                stepopenlimit = 1;
            }
        })

        $('.x').on('click', function () {
            afga.send('event', {
                'eventCategory' : '2 Step',
                'eventAction' : 'Close - X',
                'eventLabel' : '2 Step | Close - X | '+pagename
            });
        })

        $("#emailEnter").on("focus", function () {
            if (fieldfocuslimit == 0) {
                afga.send('event', {
                    'eventCategory' : '2 Step',
                    'eventAction' : 'Email Address - Focused',
                    'eventLabel' : '2 Step | Email Address - Focused | '+pagename
                });
                fieldfocuslimit = 1;
            }
        })

        $(".clickPop_2").on('click', function () {
            afga.send('event', {
                'eventCategory' : '2 Step',
                'eventAction' : 'Email Address - Complete',
                'eventLabel' : '2 Step | Email Address - Complete | '+pagename
            });
        })

        $("#emailEnter").on('click', function () {
            afga.send('event', {
                'eventCategory' : '2 Step',
                'eventAction' : 'Email Address - Complete',
                'eventLabel' : '2 Step | Email Address - Complete | '+pagename
            });
        })

        $("#emailEnter").keyup(function(event){
            if(event.keyCode == 13){
                afga.send('event', {
                    'eventCategory' : '2 Step',
                    'eventAction' : 'Email Address - Complete',
                    'eventLabel' : '2 Step | Address - Complete | '+pagename
                });
            }
        });

 
    }


    function wistiaReach()
    {

        if (typeof video === 'undefined')
            return;

        if (video._containerId.indexOf('wistia') === -1) 
            return;

        var videoDuration = video.duration(),
        vidQuarter = videoDuration/4;

        video.bind("crosstime", vidQuarter, function() {
            afga.send('event', {
                'eventCategory' : 'Wistia Video',
                'eventAction' : '25%',
                'eventLabel' : 'Wistia Video | 25% | '+pagename
            });
        });

        video.bind("crosstime", vidQuarter*2, function() {
            afga.send('event', {
                'eventCategory' : 'Wistia Video',
                'eventAction' : '50%',
                'eventLabel' : 'Wistia Video | 50% | '+pagename
            });
        });

        video.bind("crosstime", vidQuarter*3, function() {
            afga.send('event', {
                'eventCategory' : 'Wistia Video',
                'eventAction' : '75%',
                'eventLabel' : 'Wistia Video | 75% | '+pagename
            });
        });

        /*video.on('crosstime', suscribePopTime, function () {
            afga.send('event', {
                'eventCategory' : 'Wistia Video',
                'eventAction' : 'Subscribe Pop',
                'eventLabel' : 'Wistia Video | Subscribe Pop | '+pagename
            });
        })*/

        video.bind("end", function() {
            afga.send('event', {
                'eventCategory' : 'Wistia Video',
                'eventAction' : '100%',
                'eventLabel' : 'Wistia Video | 100% | '+pagename
            });
        });

    }


    function scrollReach()
    {

        var docHeight = $(document).height(),
        docQuarter = docHeight/4,
        scrolled = 0;

        $(window).scroll(function() {

            var curScroll = $(window).scrollTop() + $(window).height();

            if (curScroll >= docQuarter && scrolled == 0) {
                scrolled = 1;
                afga.send('event', {
                   'eventCategory' : 'Scroll Reach',
                   'eventAction' : '25%',
                   'eventLabel' : 'Scroll Reach | 25% | '+pagename
                });
            }

            else if (curScroll >= docQuarter*2 && scrolled == 1) {
                scrolled = 2;
                afga.send('event', {
                   'eventCategory' : 'Scroll Reach',
                   'eventAction' : '50%',
                   'eventLabel' : 'Scroll Reach | 50% | '+pagename
               });
            }

            else if (curScroll >= docQuarter*3 && scrolled == 2) {
                scrolled = 3;
                afga.send('event', {
                   'eventCategory' : 'Scroll Reach',
                   'eventAction' : '75%',
                   'eventLabel' : 'Scroll Reach | 75% | '+pagename
               });
            }

            else if (curScroll >= docQuarter*4 && scrolled == 3) {
                scrolled = 4;
                afga.send('event', {
                   'eventCategory' : 'Scroll Reach',
                   'eventAction' : '100%',
                   'eventLabel' : 'Scroll Reach | 100% | '+pagename
                });
            }
        })   
    }

    function orderForm () {


        var a = b = c = d = e = f = g = h = i = j = k = l = m = n = o = 0;


        $("input[name^='formComponentsMap[\\'order\\'].firstName']").focus(function () {
            
            if (a == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'First Name',
                   'eventLabel' : 'Order Form | First Name | '+pagename
                });
                a = 1;
            }
        })

        $("input[name^='formComponentsMap[\\'order\\'].lastName']").focus(function () {
            if (b == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'Last Name',
                   'eventLabel' : 'Order Form | Last Name | '+pagename
                });
                b = 1;
            }
        })

        $("input[name^='formComponentsMap[\\'order\\'].emailAddress']").focus(function () {
            if (c == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'Email Address',
                   'eventLabel' : 'Order Form | Email Address | '+pagename
                });
                c = 1;
            }
        })

        $("input[name^='formComponentsMap[\\'order\\'].confirmEmailAddress']").focus(function () {
            if (d == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'Confirm Email Address',
                   'eventLabel' : 'Order Form | Confirm Email Address | '+pagename
                });
                d = 1;
            }
        })

        $("input[name^='formComponentsMap[\\'order\\'].addressLine1']").focus(function () {
            if (e == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'Address Line 1',
                   'eventLabel' : 'Order Form | Address Line 1 | '+pagename
                });
                e = 1;
            }
        })

        $("input[name^='formComponentsMap[\\'order\\'].addressLine2']").focus(function () {
            if (f == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'Address Line 2',
                   'eventLabel' : 'Order Form | Address Line 2 | '+pagename
                });
                f = 1;
            }
        })

        $("input[name^='formComponentsMap[\\'order\\'].city']").focus(function () {
            if (g == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'City',
                   'eventLabel' : 'Order Form | City | '+pagename
                });
                g = 1;
            }
        })
        
        $("select[name^='formComponentsMap[\\'order\\'].stateId']").change(function () {
            if (h == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'State',
                   'eventLabel' : 'Order Form | State | '+pagename
                });
                h = 1;
            }
        })

        $("input[name^='formComponentsMap[\\'order\\'].postalCode']").focus(function () {
            if (i == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'Postal Code',
                   'eventLabel' : 'Order Form | Postal Code | '+pagename
                });
                i = 1;
            }
        })

        $("select[name^='formComponentsMap[\\'order\\'].countryId']").change(function () {
            if (j == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'Country',
                   'eventLabel' : 'Order Form | Country | '+pagename
                });
                j = 1;
            }
        })

        $("input[name^='formComponentsMap[\\'order\\'].phoneNumber']").focus(function () {
            if (k == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'Phone Number',
                   'eventLabel' : 'Order Form | Phone Number | '+pagename
                });
                k = 1;
            }
        })

        $("input[name^='formComponentsMap[\\'paymentOption\\'].maskedCreditCardNumber']").focus(function () {
            if (l == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'CC Number',
                   'eventLabel' : 'Order Form | CC Number | '+pagename
                });
                l = 1;
            }
        })  

        $("select[name^='formComponentsMap[\\'paymentOption\\'].creditCardExpirationMonth']").change(function () {
            if (m == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'CC Expiration Month',
                   'eventLabel' : 'Order Form | CC Expiration Month | '+pagename
                });
                m = 1;
            }
        })

        $("select[name^='formComponentsMap[\\'paymentOption\\'].creditCardExpirationYear']").change(function () {
            if (n == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'CC Expiration Year',
                   'eventLabel' : 'Order Form | CC Expiration Year | '+pagename
                });
                n = 1;
            }
        })

        $("input[name^='formComponentsMap[\\'paymentOption\\'].shouldSaveCreditCard']").focus(function () {
            if (o == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'Should Save CC',
                   'eventLabel' : 'Order Form | Should Save CC | '+pagename
                });
                o = 1;
            }
        })  

        $("input[name^='formComponentsMap[\\'paymentOption\\'].creditCardDescription']").focus(function () {
            if (p == 0) {
                afga.send('event', {
                   'eventCategory' : 'Order Form',
                   'eventAction' : 'CC Description',
                   'eventLabel' : 'Order Form | CC Description | '+pagename
                });
                p = 1;
            }
        })

        $("input[name^='btnPaymentOptionSubmit']").on('click', function () {
            
            afga.send('event', {
               'eventCategory' : 'Order Form',
               'eventAction' : 'Submit',
               'eventLabel' : 'Order Form | Submit | '+pagename
            });
         
        })
    }
  
  
    function orderCompletion() {
      
   
        var iframepubcode = getpubcode();
      
        //get the pub code from url
        function getpubcode() {

          var pub = window.location.href
          regex = new RegExp('/');

          if (pub.search(regex) === -1 || pub == '') { 
            return false 
          }

          pub = pub.split('/');
          pub = pub[3].split('_');

          return pub[0].toUpperCase();

        }
      
      
      if ( getUrlParam('pageNumber') == '5' || getUrlParam('pageNumber') == '6') {
          
              //only fire when tracker is available
              var sto = {},
            polltries = 0;
              
              pollPrimaryTracker(function () {
                     
                  //firing off completion event
                  afga.send('event', {
                    'eventCategory' : 'Order Complete',
                    'eventAction' : 'Success',
                    'eventLabel' : 'Order Complete | Success | '+pagename
                  });
                  
                  //load iframe and send post message
                  var iframe = document.createElement("iframe");
                  iframe.src = "https://research.dailyreckoning.com/wp-content/themes/drads/iframe.php";
                  iframe.id = 'push-frame';
                  iframe.style.display = 'none';
                  iframe.onload = function() {
                      var iframeWin = document.getElementById("push-frame").contentWindow; 
                      iframeWin.postMessage(get_pub_code(), "https://research.dailyreckoning.com");
                  };
                  document.body.appendChild(iframe);
                
              });
              
              //looking for the GA primary tracker
              function pollPrimaryTracker(callback) {
                sto = window.setTimeout(function() {
                  if (typeof ga.getByName('primaryTracker') == 'object' || polltries === 1000) {
                    callback();
                    // shut down settimeout
                    window.clearTimeout(sto);
                    sto = null;
                    return;
                  }

                  polltries++;
                  pollPrimaryTracker(callback);

                }, 10);
              }
            
        } 
    }
}

//load domready
!function(a,ctx,b){typeof module!="undefined"?module.exports=b():typeof define=="function"&&typeof define.amd=="object"?define(b):ctx[a]=b()}("domready",this,function(a){function m(a){l=1;while(a=b.shift())a()}var b=[],c,d=!1,e=document,f=e.documentElement,g=f.doScroll,h="DOMContentLoaded",i="addEventListener",j="onreadystatechange",k="readyState",l=/^loade|c/.test(e[k]);return e[i]&&e[i](h,c=function(){e.removeEventListener(h,c,d),m()},d),g&&e.attachEvent(j,c=function(){/^c/.test(e[k])&&(e.detachEvent(j,c),m())}),a=g?function(c){self!=top?l?c():b.push(c):function(){try{f.doScroll("left")}catch(b){return setTimeout(function(){a(c)},50)}c()}()}:function(a){l?a():b.push(a)}});

domready(function() { 
  ProfilesTracker();
});
 

</script>


