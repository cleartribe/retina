jQuery( document ).ready(function($) {

//GLOBAL PROPS
    var twosetepallset = false,
        iframeloaded = false;
    
    window.pagename = window.location.origin + window.location.pathname;


//HELPER FUNCTIONS
//--------------------------------------------- 
    
    //email validation
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    //get query parameters
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    //run the callback on load or check reference to already loaded and call the callback
    function whenIframeLoaded(id, callback) {
        // Get a handle to the iframe element
       // var iframe = document.getElementById(id);
        $('#'+id).on('load', function() {
            iframeloaded = true;
            callback();
        });
        
        if(iframeloaded = true)
            callback();
    }

//EXIT POP 
//---------------------------------------------    

    if ($('#exitPop').length !== 0) {
        triggerExitPop();
    }   

    if ($('#twoStep').length !== 0) {
        triggerTwoStep();
    }

    //trigger exit pop
    function triggerExitPop() {

        //initialize foundation on the following ids
        $('#exitPop').foundation();

        var leftpage = false,
            delaytime = 60000;

        if (getParameterByName('exitpop') != null) {
            delaytime = getParameterByName('exitpop');
            delaytime = Number(delaytime);
            delaytime = delaytime*1000;
        }

        setTimeout(initExitEvent, delaytime);

        function initExitEvent()
        {
            
            addEvent(document, "mouseout", function(e) {
                e = e ? e : window.event;
                var from = e.relatedTarget || e.toElement;
                if (!from || from.nodeName == "HTML") {
                    if (!leftpage) {
                        leftpage = true;
                        $('#exitPop').foundation('open');

                        //tracking
                        afga.send('event', {
                           'eventCategory' : 'Exit Pop',
                           'eventAction' : 'Open',
                           'eventLabel' : 'Exit Pop | Open | '+window.pagename
                        });
                    }
                }
            });
         
        }

        function addEvent(obj, evt, fn) {
            if (obj.addEventListener) {
                obj.addEventListener(evt, fn, false);
            }
            else if (obj.attachEvent) {
                obj.attachEvent("on" + evt, fn);
            }
        }

        //exit pop close
        $('.watch-video').on('click touchstart', function () {
            $('#exitPop').foundation('close');
            
            //tracking
            afga.send('event', {
                'eventCategory' : 'Exit Pop',
                'eventAction' : 'Close - Watch Video',
                'eventLabel' : 'Exit Pop | Close - Watch Video | '+window.pagename
            });
        })

        $('#exitPop .close-button').on('click touchstart', function () {
            afga.send('event', {
                'eventCategory' : 'Exit Pop',
                'eventAction' : 'Close - X',
                'eventLabel' : 'Exit Pop | Close - X | '+window.pagename
            });
        })

        //read transcript button
        $('.read-trans').on('click touchstart', function (e) {
            
            //tracking
            if (isMobile) {
                afga.send('event', {
                    'eventCategory' : 'Mobile',
                    'eventAction' : 'Transcript',
                    'eventLabel' : 'Mobile | Transcript | '+window.pagename
                });
            }
            else {
                 afga.send('event', {
                    'eventCategory' : 'Exit Pop',
                    'eventAction' : 'Transcript',
                    'eventLabel' : 'Exit Pop | Transcript | '+window.pagename
                });
            }
        });

    }


//TWO STEP
//---------------------------------------------   


    $(document).on('opened.fndtn.reveal', '[data-reveal]', function () {
      var modal = $(this).attr('id');
    });

    
    function triggerTwoStep() {

        var fieldfocuslimit = 0;

        //initialize foundation on the following ids
        $('#twoStep').foundation();

        //when 2 step opens
        $('.twosteplink').on('click touchstart', function () {
            afga.send('event', {
                'eventCategory' : '2 Step',
                'eventAction' : 'Open',
                'eventLabel' : '2 Step | Open | '+window.pagename
            });
        })

        //two step email focus
        $("#emailEnter").on("focus", function () {
            if (fieldfocuslimit == 0) {
                afga.send('event', {
                    'eventCategory' : '2 Step',
                    'eventAction' : 'Email Address - Focused',
                    'eventLabel' : '2 Step | Email Address - Focused | '+window.pagename
                });
                fieldfocuslimit = 1;
            }
        })

        //two step email enter
        $("#emailEnter").keyup(function(event) {
            if (event.keyCode == 13) {
                //just trigger a button click
                $("#emailEnterBtn").click();
            }
        });


        //submit email button click
        $("#emailEnterBtn").on('click', function(e) {
            //if this is the first click and the email entered is valid
            //stop the normal button click and pass the cookie through to the iframe for storage

            afga.send('event', {
                'eventCategory' : '2 Step',
                'eventAction' : 'Email Address - Entered',
                'eventLabel' : '2 Step | Email Address - Entered | '+window.pagename
            });

            if (!twosetepallset && validateEmail($('#emailEnter').val())) {
                e.preventDefault();
                var emailVal = $('#emailEnter').val()
                //pass the info from the form to the iframe
                $.set_iframe_cookie('afemail', emailVal, function (e) {

                    //track success
                    afga.send('event', {
                        'eventCategory' : '2 Step',
                        'eventAction' : 'Email Address - Complete',
                        'eventLabel' : '2 Step | Email Address - Complete | '+window.pagename
                    });
                    
                    //allow for second attempt
                    twosetepallset = true;

                    //send the info over to lytics
                    window.jstag.send({ email: e.value });
                    
                    //send them to the orderform
                    window.location.href = $("#emailEnterBtn").attr('href');
                });
            }
        });


        //when 2 step is closed
        $('#twoStep .close-button').on('click touchstart', function () {
            afga.send('event', {
                'eventCategory' : '2 Step',
                'eventAction' : 'Close - X',
                'eventLabel' : '2 Step | Close - X | '+window.pagename
            });
        })


    }

//WISTIA VIDEO
//---------------------------------------------

    $.triggerWistiaVideo = function (videoid, crosstime, mobiletranscript) {

        if (mobiletranscript == "true") {
            if( isMobile == true ){
                  $(".read-trans.unhide").attr("style", "display:block !important");
            }
        }

        window._wq = window._wq || [];
        _wq.push({ id: videoid, onReady: function(video) {
            

            window['video_'+videoid] = Wistia.api(videoid);

            var wistia_ready = new Event("wistia_ready");
            document.dispatchEvent(wistia_ready);

            //hide the button pop if crosstime is set to false
            if (crosstime !== "false") {

                    window['video_'+videoid].bind("crosstime", crosstime, function() {         
                          $(".subnow.unhide").attr("style", "display:block !important");
                          $(".disclaimer.unhide").removeClass("unhide");
                          //$(".fda-footer").css("display", "block");
                          afga.send("event", {
                              "eventCategory" : "Wistia Video",
                              "eventAction" : "Subscribe Pop",
                              "eventLabel" : "Wistia Video | Subscribe Pop | "+window.pagename
                          });
                     });

            }

            var videoDuration = window['video_'+videoid].duration(), 
            vidQuarter = videoDuration/4; 

            window['video_'+videoid].bind("crosstime", vidQuarter, function() { 
                afga.send("event", { 
                    "eventCategory" : "Wistia Video", 
                    "eventAction" : "25%", 
                    "eventLabel" : "Wistia Video | 25% | "+window.pagename 
                }); 
            }); 

            window['video_'+videoid].bind("crosstime", vidQuarter*2, function() { 
                afga.send("event", { 
                    "eventCategory" : "Wistia Video", 
                    "eventAction" : "50%", 
                    "eventLabel" : "Wistia Video | 50% | "+window.pagename 
                }); 
            }); 

            window['video_'+videoid].bind("crosstime", vidQuarter*3, function() { 
                afga.send("event", { 
                    "eventCategory" : "Wistia Video", 
                    "eventAction" : "75%", 
                    "eventLabel" : "Wistia Video | 75% | "+window.pagename 
                }); 
            }); 


            window['video_'+videoid].bind("end", function() {
                afga.send("event", {
                    "eventCategory" : "Wistia Video",
                    "eventAction" : "100%",
                    "eventLabel" : "Wistia Video | 100% | "+window.pagename
                });
            });
        }});
    }




//AJAX
//---------------------------------------------
    /**
     * get lytics data from lytics ajax request
     * @param  {string} fields - this is a comma seperated list of fields you want to retreive from lytics
     * @param  {function} callback - pass through the results from the response
     * @return {null}

        example: 

        $.get_lytics_user_data ('circstatus_pubcode,sub_promotion', function (results) {
            console.log(results);
        })

     */


    $.get_lytics_user_data = function (fields, callback) {
        
        var options = {ajaxdata : {}};
        options.ajaxdata.action = 'lytics_entity_call';
        options.ajaxdata.fields = fields;
        options.ajaxdata.email = getParameterByName('email');
        options.ajaxdata.uid = getParameterByName('uid');

        $.make_ajax_request(options, function(results) {
            if (typeof callback == 'function') {
                callback(results);
            }
        });
    }   

    /**
     * make an ajax request and return results as json data.
     * @param  {object} options - this should contain the global "ajaxdata" as this 
     * contains some core values. you can append to ajax data to get more options into the receiver of the call
     * @param  {function} callback - pass through the results from the response
     * @return {null}
     */
    $.make_ajax_request = function (options, callback) {

        //ajaxdata, callback, holder
        var results = {};
        $.post(window.location.origin+'/wp-admin/admin-ajax.php', options.ajaxdata, function(data) {
            results = $.parseJSON(data); 
            results.success = true;    
        }).fail(function() {
            results.success = false;

        }).always(function() {
            if (typeof callback == 'function')
                callback(results);
        });
    }


//IFRAME METHODS / LISTENERS / RECIEVERS
//---------------------------------------------    


    //setting up global listeners for iframe messages
    var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
    var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
    var eventer = window[eventMethod];

    //this is the main event listener. there can only be 1 so its a bit weird how to handle callbacks
    //it handles them by adding a reference to a function in the "callbacks" object then when the 
    //listener receives the function reference it executes the function. 
    eventer(messageEvent,function(e) {
        if (e.data.callback !== undefined)
            callbacks[e.data.callback](e);
    }, false);


    /**
     * set a cookie via the child iframe
     * @param  {string} cookie_name - this is what you want the cookie to be called
     * @param  {string} cookie_value - this is the value of the cookie
       @param  {function} callback - this is the function to run after the cookie is set
     * @return {null}
     */
    $.set_iframe_cookie = function (cookie_name, cookie_value, callback) {
        whenIframeLoaded("retina-iframe", function() {
            callbacks.setIframeCallback = function (e) { callback(e) };
            var iframeWin = document.getElementById("retina-iframe").contentWindow,
            options = {method: 'setcookie', name: cookie_name, value: cookie_value, callback: 'setIframeCallback', url: retinaUrl};
            iframeWin.postMessage(options, '*');
        });
    }

    /**
     * get a cookie via the child iframe
     * @param  {string} cookie_name - this is the name of the cookie that you want to retrieve
       @param  {function} callback - this is the function to run after the cookie is retreived
     * @return {null}
     */
    $.get_iframe_cookie = function (cookie_name, callback) {
        whenIframeLoaded("retina-iframe", function() {
            callbacks.getIframeCallback = function (e) { callback(e) };
            var iframeWin = document.getElementById("retina-iframe").contentWindow,
            options = {method: 'getcookie', name: cookie_name, callback: 'getIframeCallback', url: retinaUrl};
            iframeWin.postMessage(options, '*');
        });
    }


    //dispatch event when all this is loaded
    var retinajs_ready = new Event("retinajs_ready");
    document.dispatchEvent(retinajs_ready);


});