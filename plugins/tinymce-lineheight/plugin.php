<?php
/*
Plugin Name: Tiny MCE LineHeight
Author: tjtate
Description: Adds lineheight to wordpress
Text Domain: lineheight
Version: 0.0.1
*/

function my_custom_plugins( $plugins ) {
     $plugins['lineheight'] = plugins_url( '/', __FILE__ ) . 'lineheight/plugin.min.js';
     return $plugins;
}
add_filter( 'mce_external_plugins', 'my_custom_plugins' );


function my_mce_buttons_3( $buttons ) {	
	$buttons[] = 'lineheight';

	return $buttons;
}
add_filter( 'mce_buttons_3', 'my_mce_buttons_3' );


function format_TinyMCE ($in)
{

    $in['plugins'] .= ',lineheight';
    $in['toolbar3'] .= ',lineheightselect';
    return $in;
}
add_filter( 'tiny_mce_before_init', 'format_TinyMCE');