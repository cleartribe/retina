<?php

/*
	tag data is sent in $_POST object as
	comma seperated strings 

	["tax_input"]=>
	  array(4) {
		["post_tag"]=>
		string(5) "obama"
		["psychology"]=>
		string(5) "greed,politics"
		["objective"]=>
		string(0) "get rich"
		["sector"]=>
		string(0) ""
	}

	native WP input for tags is
	<input type="text" id="new-tag-psychology" name="newtag[psychology]" class="newtag form-input-tip" size="16" autocomplete="off" value="">
*/

class Agora_profiles_taxonomies extends Agora_Profiles_Client {

	/*
		any custom post types registered within the AF franchise
	*/
	private $post_types = array(
		'post',
		'page',
		'topic',
		'promo-video',
		'promo-html',
		'alerts',
		'faq',
		'getting-started',
		'newsletter',
		'portfolios',
		'products',
		'publications',
		'reports',
		'topic',
		'webinars'
	);

	public function __construct() {
		
		$this->hooks();
	}

	public function hooks() {
		
		add_action( 'init', array( $this, 'hook_init' ) );

		// must init each post type
		foreach ( $this->post_types as $posttype ) {
			add_action( 'add_meta_boxes_' . $posttype, array( $this, 'hook_add_meta_box' ) );			
		}
	}

	public function hook_init() {
		$this->profiles_tags_init();
	}

	public function hook_add_meta_box() {
		$this->add_profiles_tags_meta_box();
	}

	/*
		create custom taxonomies
		@uses $custom_taxonomies
		@return null
	*/
	private function profiles_tags_init() {
		// create a new taxonomy
		for ( $i = 0; $i < count( $this->custom_taxonomies ); $i++ ) {
			register_taxonomy(
				$this->custom_taxonomies[$i],
				$this->post_types,
				array(
					'label' => __( $this->custom_taxonomies[$i] ),
					'rewrite' => FALSE,
					'show_ui' => FALSE,
				)
			);					
		}
	}

	private function add_profiles_tags_meta_box() {

		// must init each post type
		foreach ( $this->post_types as $posttype ) {
			add_meta_box(
				'af_profile_tags',
				'Profile Tags',
				array( $this, 'add_profiles_tags_meta_box_html' ),
				$posttype,
				'side'
			);	
		}
	}

	public function add_profiles_tags_meta_box_html( $post ) {

		$html = '<script>ProfilesAdmin.customTax(' . json_encode( $this->custom_taxonomies ) . ');</script>';

		echo $html;
	}
}
