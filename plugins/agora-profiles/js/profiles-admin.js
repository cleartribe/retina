// jquery-cookie https://raw.github.com/carhartl/jquery-cookie/master/jquery.cookie.js
(function(e){if(typeof define==="function"&&define.amd){define(["jquery"],e)}else{e(jQuery)}})(function(e){function n(e){return e}function r(e){return decodeURIComponent(e.replace(t," "))}function i(e){if(e.indexOf('"')===0){e=e.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,"\\")}try{return s.json?JSON.parse(e):e}catch(t){}}var t=/\+/g;var s=e.cookie=function(t,o,u){if(o!==undefined){u=e.extend({},s.defaults,u);if(typeof u.expires==="number"){var a=u.expires,f=u.expires=new Date;f.setDate(f.getDate()+a)}o=s.json?JSON.stringify(o):String(o);return document.cookie=[s.raw?t:encodeURIComponent(t),"=",s.raw?o:encodeURIComponent(o),u.expires?"; expires="+u.expires.toUTCString():"",u.path?"; path="+u.path:"",u.domain?"; domain="+u.domain:"",u.secure?"; secure":""].join("")}var l=s.raw?n:r;var c=document.cookie.split("; ");var h=t?undefined:{};for(var p=0,d=c.length;p<d;p++){var v=c[p].split("=");var m=l(v.shift());var g=l(v.join("="));if(t&&t===m){h=i(g);break}if(!t){h[m]=i(g)}}return h};s.defaults={};e.removeCookie=function(t,n){if(e.cookie(t)!==undefined){e.cookie(t,"",e.extend({},n,{expires:-1}));return true}return false}});

(function(window, document, jQuery) {

var $ = jQuery;

var ProfilesAdmin = function() {

var utilities = {

        profilesEndPoint: '',

        /*
            ajaxRequest({
                url: example.com/end-point,
                data: {
                    foo: bar,
                    foz: baz
                },
                callBack: getDataCallback,
                dataType: 'json'
            });
        */
        ajaxRequest: function(request) {
            var ax = {};
            ax.response = "ajax-response";
            ax.type = "POST";
            ax.url = request.url;
            ax.data = request.data;
            ax.global = false;
            ax.timeout = 30000;
            if ( typeof request.dataType !== 'undefined') {
                ax.dataType = request.dataType;
            }
            ax.success = function(r) {
                if (typeof request.callBack !== 'undefined') {
                    request.callBack(r);
                }
            };
            ax.error = function(r) {};  
            // AJAX Request
            jQuery.ajax(ax);
        },
    },

    taxonomy = {

        tax_types: [],
        af_metabox: {},
        afaddtags: {},
        afclsmeta: {},
        dfltwptagbox: {},
        tag_store: {},
        post_tags_box: {},
        afcstmtags: {},
        profile_tags: {},
        aftdata: {},
        metabox_expand_w: 500,
        metabox_expand_h: 500,
        post_taxonomy: {},

        init: function(tax) {
            this.tax_types = tax;
            this.profiles_tag_metabox();
            this.get_custom_tags();
        },

        profiles_tag_metabox: function() {
            this.af_metabox = $('#af_profile_tags');
            // clearout default WP html
            this.af_metabox.html(this.profiles_tag_metabox_html());
            this.bind_profiles_metabox_objects();
            this.move_default_tag_metabox();
        },

        profiles_tag_metabox_html: function() {
            var html = '';

            html += '<div>';
            html +=     '<h3 class="hndle"><span>Profile Tags</span><button id="afclsmeta" class="button button-secondary button-large">X</button></h3>';
            html +=     '<button id="afaddtags" class="button button-secondary button-large">Add Tags</button>';
            html +=     '';
            html += '<div>';
            html += '<div id="dfltwptagbox">';
            html += '<div>';
            html += '<div id="afcstmtags">';
            html += '<div>';

            return html;
        },

        bind_profiles_metabox_objects: function() {

            this.afaddtags = $('#afaddtags');
            this.afaddtags.click(function(e) {
                e.preventDefault();
                taxonomy.expand_profiles_tag_metabox(true);
            });

            this.afclsmeta = $('#afclsmeta');
            this.afclsmeta.click(function(e) {
                e.preventDefault();
                taxonomy.expand_profiles_tag_metabox(false);
            });

            // set dims for metabox
            this.af_metabox_width = this.af_metabox.outerWidth();
            this.af_metabox_height = this.af_metabox.outerHeight();

            this.af_metabox.css('height', this.af_metabox_height);

            // set wrapper for custom tags
            this.afcstmtags = $('#afcstmtags');
        },

        move_default_tag_metabox: function() {
            this.dfltwptagbox = $('#dfltwptagbox');
            this.post_tags_box = $('#tagsdiv-post_tag');
            this.post_tags_box.appendTo(this.dfltwptagbox);
        },

        expand_profiles_tag_metabox: function(expand) {
            var n_left = (this.metabox_expand_w - this.af_metabox_width) * -1;

            if (expand) {
                this.afclsmeta.css('display', 'inline');
                this.af_metabox.css({
                    'box-shadow': '0 0 10px 1px #CCC'
                });
                this.af_metabox.animate({
                    width: this.metabox_expand_w,
                    height: this.metabox_expand_h,
                    left: n_left
                }, 200, function() {
                    $(this).css('height', 'auto');
                });

            } else {

                this.afclsmeta.css('display', 'none');
                this.af_metabox.css({
                    'box-shadow': 'none'
                });
                this.af_metabox.animate({
                    width: this.af_metabox_width,
                    height: this.af_metabox_height,
                    left: 0
                }, 200);
            }
        },

        get_custom_tags: function() {
            utilities.ajaxRequest({
                url: utilities.profilesEndPoint,
                data: {
                    route: 'taxonomies/getcustomterms',
                    data: ''
                },
                callBack: taxonomy.get_custom_tags_callback,
                dataType: 'json'
            });
        },

        get_custom_tags_callback: function(r) {             
            taxonomy.populate_custom_tags(r);
        },

        populate_custom_tags: function(tags) {
            var html = '',
                n_tags = 0;
            // iterate over tag set returned from profiles
            for (t in tags) {

                // create tag buckets for tag type
                html += '<div class="aftagbkt">';
                html += '<h4 class="afthl">' + this.proper_word(t) + '</h4>';

                n_tags = tags[t].length;
                for (var i = 0; i < n_tags; i++) {
                    html += '<label>';
                    html +=     '<input class="afprfltag" name="aftag_' + t + '" type="checkbox" value="' + tags[t][i].toLowerCase() + '" /> ';
                    html +=     tags[t][i];
                    html += '</label>';
                }
                html += '</div>';
            }

            this.af_metabox.append(html);
            this.bind_tag_inputs();
            this.set_prepopulated_tags();
            this.bind_custom_tag_box();
        },

        bind_tag_inputs: function() {
            this.profile_tags = $('input.afprfltag');
            // bind hidden inputs used to store comma seperated list of tags

            this.profile_tags.click(function() {
                var self = $(this),
                    val = self.val(),
                    tag_type = self.attr('name');
                if (self.attr('checked') == 'checked') {
                    taxonomy.apply_tag_data('save', val, tag_type);
                } else {
                    taxonomy.apply_tag_data('delete', val, tag_type);
                }
            })
        },

        apply_tag_data: function(action, tag_name, tag_type) {
            var tags = '';

            tag_type = tag_type.split('_');
            tag_type = tag_type[1],

            // get hidden input tag store
            this.tag_store = $('input[name="tax_input[' + tag_type + ']"]');
            tags = this.tag_store.val();

            if (action === 'save') {
                tags += ',' + tag_name + ',';

            } else if(action === 'delete') {
                // if tag is set in array, remove it
                tags = tags.split(',');
                for (var i = 0; i < tags.length; i++) {
                    if (tags[i].toLowerCase() === tag_name.toLowerCase()) {
                        tags.splice(i, 1);
                    }
                }
                tags = tags.join(',');
            }

            // trim commas
            tags = tags.replace(/(^\,|\,$)/g, '');
            this.tag_store.val(tags);
        },

        set_prepopulated_tags: function() {
            if (typeof af_taxonomy_types == 'undefined') {return false;}

            // get prepopulated hidden input tag store
            var tag_store = [],
                tag_type = af_taxonomy_types,
                tag_input = {},
                tag_value = [],
                tag_element = {},
                tags = [];

            // create an array of tag object values
            for (var i = 0; i < tag_type.length; i++) {

                tag_input = $('input[name="tax_input[' + tag_type[i] + ']"]');
                tag_value = tag_input.val();
                tag_value = tag_value.split(',');

                tag_store[i] = {
                    tag_input: tag_input,
                    tag_type: tag_type[i].toLowerCase(),
                    tag_value: tag_value
                };
            }

            // set tag values
            for (var i = 0; i < tag_store.length; i++) {
                tags = tag_store[i].tag_value;
                
                for (var j = 0; j < tags.length; j++) {
                    tag_element = $('input[name="aftag_' + tag_store[i].tag_type + '"][value="' + tags[j].toLowerCase() + '"]');
                    if (tag_element.length === 1){
                        tag_element.attr('checked', 'checked');
                    }
                }
            }
        },

        bind_custom_tag_box: function() {
            var tag_bucket = $('div.aftagbkt'),
                bucket_w = this.metabox_expand_w / 3;
            tag_bucket.css({
                width: bucket_w - 25,
                height: 300
            });
        },

        proper_word: function(t) {
            t = t.split('');
            t[0] = t[0].toUpperCase();
            return t.join('');
        }
    },

    profiles = {

        init: function() {
            this.bind_get_publications();
            this.bind_get_campaigns();
        },

        bind_get_publications: function () {
            var afpgetpubs = $('#afpgetpubs');

            afpgetpubs.click(function() {
                utilities.ajaxRequest({
                    url: utilities.pubFetchEndPoint,
                    data: {
                        data: ''
                    },
                    callBack: profiles.get_publications_callback,
                    dataType: 'json'
                });
            });
        },

        get_publications_callback: function(r) {
            alert('Publications returned = ' + r[0]);
        },

        bind_get_campaigns: function () {
            var afpgetcmps = $('#afpgetcmps');

            afpgetcmps.click(function() {
                utilities.ajaxRequest({
                    url: utilities.campaignFetchEndPoint,
                    data: {
                        data: ''
                    },
                    callBack: profiles.get_campaigns_callback,
                    dataType: 'json'
                });
            });
        },

        get_campaigns_callback: function(r) {
            alert('campaigns returned = ' + r[0]);
        }
    };

function showModal(modal_id, action)
{
    var background = $('.reveal-modal-bg'),
        modal = $('.reveal-modal'),
        close = $('.close-reveal-modal');

    modal_id = $(modal_id);

    if (action == 'show') {
        modal_id.css({
            display: 'block',
            visibility: 'visible',
            top: 50
        });
        background.css({
            display: 'block'
        });

    } else if (action == 'hide') {
        modal.css({
            display: 'none',
            visibility: 'hidden'
        });
        background.css({
            display: 'none'
        });
    }

    background.add(close).unbind('click');
    background.add(close).on('click', function() {
        showModal(modal_id, 'hide');
    })
}

/**
 * used in EmailBuilderClass and PremiumSetsClass
 * @type {String}
 */
var cookieEmailPub = 'afpEmailPub';

function setProfilesCookie(name, value) {
    $.cookie(name, value, {
        expires: 365
    });
}

function getSiteDomain()
{
    if (typeof siteUrl == 'undefined') {
        return 'agorafinancial.dev';
    }

    siteUrl = siteUrl.replace(/(http|https):\/\//, '');
    siteUrl = siteUrl.replace(/\/$/, '');

    return siteUrl;
}

var emailBuilder = EmailBuilderClass();

function EmailBuilderClass() {

    var orgEmail = {
            'AF': 'customerservice@agorafinancial.com',
            'DR': 'customerservice@agorafinancial.com',
            'LFB': 'customerservice@lfb.org'
        },
        orgName = {
            'AF': 'Agora Financial',
            'DR': 'The Daily Reckoning',
            'LFB': 'Laissez Faire Books',
        },
        emailTypeSelector = {},
        emailPubSelector = {},
        emailAdminEditor = {},
        emailEditorInterface = {},
        revealModalBg = {},
        emailName = {},
        emailFromEmail = {},
        emailFromName = {},
        emailSubject = {},
        emailContent = {},
        emailConfirm = {},
        emailSave = {},
        emailEdit = {},
        emailHelp = {},
        emailDocumentation = {},
        emailBacktotop = {},
        emailVariablesShow = {},
        emailVariablesDisplay = {},
        emailVariables = {},
        emailVarsTable = {},
        addVariable = {},
        variableNames = {},
        variableValues = {},
        cookieEmailType = 'afpEmailType',
        defaultSortColumn = 'created',
        defaultSortOrder = 'decending', // decending || ascending
        email = {
            name: '',
            type: '',
            pub: '',
            from_email: '',
            from_name: '',
            subject: '',
            content: ''
        },
        emailList = [];

    function init() {
        setElements();
        setEmailType();
        setEmailPublication();
        createEmailEditor();
        getEmails();
        bindEmailTableHeader();
        cloneVariableRows();
        getEmailVariables();
        bindAddVariable();
        showShortCodes();
    }

    function setElements() {
        emailTypeSelector = $('select[name="email_type"]');
        emailPubSelector = $('select[name="email_pub"]');
        emailAdminEditor = $('#email-admin-editor');
        emailEditorInterface = $('#email-editor-interface');
        emailConfirmation = $('#email-confirmation');
        revealModalBg = $('div.reveal-modal-bg');
        emailName = $('#email_name');
        emailFromEmail = $('#email_from_email');
        emailFromName = $('#email_from_name');
        emailSubject = $('#email_subject');
        emailContent = $('#email_content');
        emailConfirm = $('#email_confirm');
        emailSave = $('#email_save');
        emailEdit = $('button[name="edit_email"]');
        emailHelp = $('#email_help');
        emailDocumentation = $('#email-documentation');
        emailBacktotop = $('#emailBacktotop');
        emailVariablesShow = $('#email_variables');
        addVariable = $('#add_variable');
        emailVarsTable = $('#email-vars-table');
        emailVariablesDisplay = $('#email-variables-display');
    }

    /**
     * set variable and cookie values for email type
     */
    function setEmailType() {
        var cookie = $.cookie(cookieEmailType),
            value = (typeof cookie == 'undefined') ? emailTypeSelector.val() : cookie;

        // get the value for other methods
        email.type = value;

        // set the default value on load
        setProfilesCookie(cookieEmailType, value);

        // set the actual select value, which might be set by the cookie
        emailTypeSelector.val(value);
        
        // update values on change
        emailTypeSelector.change(function() {
            var self = $(this),
                value = self.val();

            email.type = value;
            emailTypeSelector.val(value);
            setProfilesCookie(cookieEmailType, value);
            getEmails();
            // show/hide shortcodes
            showShortCodes();
        })
    }

    function showShortCodes() {
        $('div.view_shortcodes').css('display', 'none');
        $('#shortcodes_' + email.type).css('display', 'block');
    }

    /**
     * set variable and cookie values for email pub
     */
    function setEmailPublication() {
        var cookie = $.cookie(cookieEmailPub),
            value = (typeof cookie == 'undefined') ? emailPubSelector.val() : cookie;

        // get the value for other methods
        email.pub = value;

        // set the default value on load
        setProfilesCookie(cookieEmailPub, value);

        // set the actual select value, which might be set by the cookie
        emailPubSelector.val(value);
        
        // update values on change
        emailPubSelector.change(function() {
            var self = $(this),
                value = self.val();

            email.pub = value;
            emailPubSelector.val(value);
            setProfilesCookie(cookieEmailPub, value);
            getEmails();
            getEmailVariables();
        });
    }

    function createEmailEditor() {
        bindEmailEditor();
    }

    function bindEmailEditor() {

        emailName.on('change', function(){
            var self = $(this),
                val = self.val();

            // check if email name contains spaces
            if (val.match(/\s/)) {
                $('#editor_error_name_format').css('display', 'block');
            } else {
                $('#editor_error_name_format').css('display', 'none');
                email.name = val;               
            }

            utilities.ajaxRequest({
                url: utilities.profilesEndPoint,
                data: {
                    route: 'email/existing-email',
                    data: {
                        name: val
                    }
                },
                callBack: existingEmailCallback,
                dataType: 'json'
            });
        });

        emailFromEmail.on('change', function(){
            var self = $(this),
                val = self.val();
            email.from_email = val;
        });

        emailFromName.on('change', function(){
            var self = $(this),
                val = self.val();
            email.from_name = val;
        });

        emailSubject.on('change', function(){
            var self = $(this),
                val = self.val();
            email.subject = val;
        });

        emailContent.on('change', function(){
            var self = $(this),
                val = self.val();
            email.content = val;
        });

        emailConfirm.on('click', function() {
            if (validEmail()) {
                confirmEmail();             
            }
        });

        emailEdit.on('click', function() {
            editAnEmail(email);
        });

        $('a.close-reveal-modal').add('div.reveal-modal-bg').on('click', function() {
            closeEditor(true);
        })

        $('#email_add_new').on('click', function() {
            email.id = undefined;
            populateEditorFields();
            openEditor();
            $('p.email_editor_error').css('display', 'none');
        });

        emailSave.on('click', function() {
            utilities.ajaxRequest({
                url: utilities.profilesEndPoint,
                data: {
                    route: 'email/save-email',
                    data:email
                },
                callBack: saveEmailCallback,
                dataType: 'json'
            });
        });
    }

    function saveEmailCallback(r) {
        closeEditor(false);
        getEmails();
    }

    function existingEmailCallback(r) {
        var msg = $('#editor_error_name_taken');

        if (typeof r[0] != 'boolean') {
            // clear the name to prevent saving
            email.name = '';
            alert('something with name checker went wrong. Contact the admin');
            // exit
            return;
        }

        if (r[0]) {
            // clear the name to prevent saving
            email.name = '';
            msg.css('display', 'block');
        } else {
            msg.css('display', 'none');
        }
    }

    function validEmail() {

        for (e in email) {
            if (email[e] === '') {
                $('#editor_error_' + e).css('display', 'block');
                return false;
            }
        }

        return true;
    }

    function editAnEmail(emailData) {
        email = emailData;
        openEditor();
    }

    /**
     * show a preview of all the parameters and the email content
     * @return {null}
     */
    function confirmEmail() {
        // create an iframe to wrap the content because it includes <html> tags
        var doc = document.getElementById('email_preview').contentWindow.document;
        doc.open();
        doc.write(email.content);
        doc.close();

        var instructions = $('p.preview');
        instructions.css('display', 'none');

        toggleEditorInterface('confirm');
        $('#email_preview_name').html(email.name);
        $('#email_preview_type').html(email.type);
        $('#email_preview_pub').html(email.pub);
        $('#email_preview_from_email').html(email.from_email);
        $('#email_preview_from_name').html(email.from_name);
        $('#email_preview_subject').html(email.subject);

        if (email.type != 'order_confirmation') {
            $('#profiles_code_wrap').css('display', 'block');
            $('#profiles_code_instructions').css('display', 'block');
            $('#profiles_code').text(getProfilesCode());

        } else if (email.type == 'order_confirmation') {
            $('#profiles_code_wrap').css('display', 'block');
            $('#profiles_code_marketing').css('display', 'block');
            $('#profiles_code').text(getOrderConfirmCode());
        }
    }

    /* OUTPUT    
    
    Profiles.email({
        "email":"mpizzo@agorafinancial.com",
        "email_name":"cancel_autorenew_0315",
        "email_type":"cs_team_alert",
        "advantage_id":"123456",
        "pub_code":"OST"
    });    
    */
    function getProfilesCode() {
        var js = '',
            short_codes = [];

        js += '<script>' + '\r'
        js +=   '\t' + 'Profiles.email({' + '\r';
        js +=       '\t\t' + '"email": "foobar@email.com",' + '\r';
        js +=       '\t\t' + '"email_name": "' + email.name + '",' + '\r';
        js +=       '\t\t' + '"email_type": "' + email.type + '",' + '\r';

        // get the shortcodes
        short_codes = parseShortCodes();

        if (short_codes.length > 0) {

            for (var i = 0; i < short_codes.length; i++) {
                js +=       '\t\t' + '"' + short_codes[i] + '": "foobar",' + '\r';
            }
        }

        js +=       '\t\t' + '"callback": function(r) {} // optional callback' + '\r';

        js +=   '\t' + '})' + '\r';
        js += '</script>';

        return js;
    }

    /* OUTPUT

    <script>
        AFemail({
            emailName: 'AWN_bigdrop_0315'
        }).send();
    </script>    
    */
    function getOrderConfirmCode() {
        var js = '',
            short_codes = [];

        js += '<script>' + '\r';
        js +=   '\t' + 'AFemail({' + '\r';
        js +=       '\t\t' + 'emailName: "' + email.name + '"' + '\r';
        js +=   '\t' + '}).send()' + '\r';
        js += '</script>' + '\r' + '\r';

        // show optional parameters
        js += '<!--'  + '\r';
        js += '\t' + '// Optional Parameters' + '\r';
        js += '\t' + '<script>' + '\r';
        js +=   '\t\t' + 'AFemail({' + '\r';
        js +=       '\t\t\t' + 'emailName: "fooBar",' + '\r';
        js +=       '\t\t\t' + 'redirectTo: "confirm",' + '\r';
        js +=       '\t\t\t' + 'choiceCode: "79AR_Cand", // Over-ride Opium description termcode' + '\r';
        js +=       '\t\t\t' + 'noUpsell: true,' + '\r';
        js +=       '\t\t\t' + 'setConfirmPage: false' + '\r';
        js +=   '\t\t' + '}).send()' + '\r';
        js += '\t' + '</script>' + '\r';
        js += '-->' + '\r';

        return js;
    }

    function parseShortCodes() {
        var content = email.content,
            short_codes = [];

        content.replace(/\[[a-zA-z0-9_]+(\s|\])/g, function(t) {
            short_codes.push(t.replace(/(\[|\]|\s)/g, ''));
        });

        return short_codes;
    }

    function toggleEditorInterface(toggle) {

        if (toggle == 'editor') {
            emailEditorInterface.css('display', 'block');
            emailConfirmation.css('display', 'none');
        } else if (toggle == 'confirm') {
            emailEditorInterface.css('display', 'none');
            emailConfirmation.css('display', 'block');
        }
    }

    function clearEmail() {
        email.type = '';
        email.pub = '';
        email.name = '';
        email.from_email = '';
        email.from_name = '';
        email.subject = '';
        email.content = '';
    }

    function closeEditor(cnfrm) {

        if (cnfrm && email.content !== '' && !window.confirm('Do you want to close the editor?')) {
            return;
        }

        clearEmail();
        emailAdminEditor.css({
            display: 'none',
            visibility: 'hidden'
        });
        revealModalBg.css({
            display: 'none'
        });

        $('html, body').animate({
            scrollTop: 0
        }, 100);
    }

    /**
     * use the pub code org to prepopulate fields
     * @return {null}
     */
    function populateEditorFields() {
        var org = '';

        // get the org for pre-populating email and name
        for (var i = 0; i < agora_publications.length; i++) {
            if (email.pub === agora_publications[i].pubcode) {
                org = agora_publications[i].org;
            }
        }

        if (email.from_email === '') {
            email.from_email = orgEmail[org];
        }

        if (email.from_name === '') {
            email.from_name = orgName[org];
        }

        emailName.val(email.name);
        emailFromEmail.val(email.from_email);
        emailFromName.val(email.from_name);
        emailSubject.val(email.subject);
        emailContent.val(email.content);

        // if email.id is set, it means this is an edit, so disable changing the name
        if (typeof email.id != 'undefined') {
            emailName.prop('disabled', true);
        } else{
            emailName.prop('disabled', false);
        }
    }

    function openEditor() {

        toggleEditorInterface('editor');

        emailAdminEditor.css({
            display: 'block',
            visibility: 'visible',
            top: 50
        });

        revealModalBg.css({
            display: 'block'
        });
    }

    function getEmails() {
        utilities.ajaxRequest({
            url: utilities.profilesEndPoint,
            data: {
                route: 'email/get-emails',
                data: {
                    pub: $.cookie(cookieEmailPub),
                    type: $.cookie(cookieEmailType)
                }
            },
            callBack: function(response) {
                emailList = response;
                showEmails(response, defaultSortColumn, defaultSortOrder);
            },
            dataType: 'json'
        });
    }

    /**
     * cache objects so rewrites of table does not break
     */
    var emailTableDisplay = null,
        emailTableRows = null;

    /**
     * create table to show active emails
     * @param  {array} emails
     * @param  {string} sortColumn
     * @param  {string} sortOrder
     * @return {null}
     */
    function showEmails(emails, sortColumn, sortOrder) {
        var html = '';

        // cache objects for later use
        emailTableDisplay = (emailTableDisplay === null) ? $('#email-displayer') : emailTableDisplay;
        emailTableRows = (emailTableRows === null) ? emailTableDisplay.children('div.row') : emailTableRows;

        emails = sortTable(emails, sortColumn, sortOrder);
        // re-cache sorted emails
        emailList = emails;

        for (var i = 0; i < emails.length; i++) {
            // clone the default row and use it to create a row with email data
            html += '<div class="row">';
                html += showEmailRow(i, emailTableRows.clone(), emails[i]);
            html += '</div>';
        }

        emailTableDisplay.html(html);
        bindTableRows();
    }

    /**
     * sort array of objects based on object key
     * this will not work with general arrays
     * @param  {array} rows  array of objects
     * @param  {string} sortColumn key of object, with objects in array
     * @return {array} rows array of objects
     */
    function sortTable(rows, sortColumn, sortOrder) {
        // rows must be an array, and array values must be Objects
        if (Object.prototype.toString.call(rows) !== '[object Array]' || Object.prototype.toString.call(rows[0]) !== '[object Object]') {
            return rows;
        }

        var keys = [],
            column = '',
            sorted = [];

        // iterate over array values
        for (var i = 0; i < rows.length; i++) {

            // iterate over each object value to get desired keys
            for (column in rows[i] ) {
                // if key matches desired column and the key is valed, save value
                if (column === sortColumn && rows[i].hasOwnProperty(sortColumn)) {
                    keys.push(rows[i][sortColumn]);
                }
            }
        }

        // now sort the values
        keys.sort(function(a,b) {
            if (sortOrder === 'ascending') {
                return a > b;
            } else if (sortOrder === 'decending') {
                return b > a;
            }
        });

        sorted = getSortedValues(keys, rows, sortColumn);

        return sorted;
    }

    /**
     * @param  {string} key  key value
     * @param  {array} rows array of objects
     * @param  {string} sortColumn
     * @return {object} key to delete and array values
     */
    function getSortedValues(keys, tableRows, sortColumn) {
        var sorted = [],
            /**
             * must deep clone the tableRows array with JSON to break the array's "copy by reference"
             * below we use rows.splice() which will delete all "out-of-scope" reference to the row data
             * http://stackoverflow.com/a/20547803/1334612
             */
            rows = JSON.parse(JSON.stringify(tableRows));

        // iterate over keys first to maintain order
        for (var i = 0; i < keys.length; i++) {

            // iterate over rows to compare row values to sorted keys
            for (var j = 0; j < rows.length; j++) {

                // iterate over each object value to get desired values to match to keys
                for (column in rows[j] ) {
                    // if key matches desired column and the column value matches key value
                    if (column === sortColumn && rows[j][column] === keys[i]) {
                        sorted.push(rows[j]);
                        // remove values after they're matched to prevent duplicate collisions
                        keys.splice(i, 1);
                        rows.splice(j, 1);
                        // roll back to get missing values
                        i--;
                        j--;
                    }
                }
            }
        }

        return sorted;
    }

    /**
     * populate each row of the table for a given email
     * @param  {integer} index to maintain order when sorting
     * @param  {object} row   jquery object with row elements
     * @param  {object} email email data
     * @return {string} html
     */
    function showEmailRow(index, row, email) {
        row.find('div.email_display_name').text(email.name);
        row.find('div.email_display_subject').text(email.subject);
        row.find('div.email_display_created').text(getDateFormat(email.created));
        row.find('div.email_display_buttons').html('<button class="tiny email_edit" data-index="'+index+'">Edit</button><button class="tiny secondary email_delete" data-index="'+index+'">Delete</button>');
        return row.html();
    }

    function getDateFormat(t) {
        var date = new Date(t*1000);
        t = date.toDateString();
        // remove the day
        t = t.split(' ');
        t.shift();
        t = t.join(' ');
        return t;
    }

    function bindEmailTableHeader() {
        var toggleOrder = $('button.toggle_order');

        toggleOrder.click(function() {
            var self = $(this),
                val = self.val(),
                column = self.attr('data-column'),
                order = self.attr('data-order');

            order = (order === 'decending') ? 'ascending' : 'decending';
            self.attr('data-order', order);
            showEmails(emailList, column, order);
            toggleOrder.removeClass('pointing_up');
            if (order === 'ascending') {
                self.addClass('pointing_up');               
            }
        });

        emailHelp.click(function() {
            $('html, body').animate({
                scrollTop: (emailDocumentation.offset().top - 100)
            }, 100);
        });

        emailBacktotop.click(function() {
            $('html, body').animate({
                scrollTop: 0
            }, 100);
        });
    }

    /**
     * bind events to email rows, like edit and delete
     * @return {null}
     */
    function bindTableRows() {
        var edit = $('button.email_edit'),
            emailDelete = $('button.email_delete');

        edit.click(function() {
            var self = $(this),
                index = self.attr('data-index'),
                /**
                 * must deep clone the tableRows array with JSON to break the array's "copy by reference"
                 * http://stackoverflow.com/a/20547803/1334612
                 */
                emailData = JSON.parse(JSON.stringify(emailList));

            email = emailData[index];
            populateEditorFields();
            openEditor();
        });

        emailDelete.click(function() {
            var self = $(this),
                index = self.attr('data-index');

            if (window.confirm('Do you want to delete this email?')) {
                utilities.ajaxRequest({
                    url: utilities.profilesEndPoint,
                    data: {
                        route: 'email/delete-email',
                        data: {
                            email_id: emailList[index].id
                        }
                    },
                    callBack: emailDeleteCallback,
                    dataType: 'json'
                });
            }
        });
    }

    function emailDeleteCallback(r) {
        getEmails();
    }

    function cloneVariableRows() {
        // create a clone for later use, then remove
        emailVariables = $('div.email-variable-row').clone();
        $('div.email-variable-row').remove();
    }

    function getEmailVariables()
    {
        utilities.ajaxRequest({
            url: utilities.profilesEndPoint,
            data: {
                route: 'email/get-email-variables',
                data: {
                    pub_code: emailPubSelector.val()
                }
            },
            dataType: 'json',
            callBack: getEmailVariablesCallback
        });
    }

    function getEmailVariablesCallback(r) {
        // clear table
        emailVarsTable.html('');
        // create the variable rows
        for (var i = 0; i < r.length; i++) {
            emailVarsTable.append(emailVariables.clone());
        }
        // create the jquery objects
        bindEmailVariableInputs();
        // now populate the rows with data
        for (var i = 0; i < r.length; i++) {
            variableNames.eq(i).val(r[i].name).prop('disabled', true);
            variableValues.eq(i).val(r[i].value).prop('disabled', false);
        }
    }

    function bindAddVariable() {

        emailVariablesShow.add('#close_variable').click(function() {
            if (emailVariablesDisplay.css('display') == 'none') {
                emailVariablesDisplay.css('display', 'block');
            } else {
                emailVariablesDisplay.css('display', 'none');
            }
        });

        addVariable.click(function() {
            emailVarsTable.append(emailVariables.clone());
            bindEmailVariableInputs();
        });
    }

    function bindEmailVariableInputs() {
        var deleteVar = $('button.delete_variable'),
            varRows = $('div.email-variable-row'),
            errorName = $('p.email_var_name_error'),
            errorFormat = $('p.email_var_format_error');

        variableNames = $('input.email_variable_name');
        variableValues = $('input.email_variable_value');

        variableNames.unbind('change');
        variableNames.change(function() {
            var self = $(this),
                val = self.val(),
                index = self.index('input.email_variable_name');

            // confirm the name does not have spaces
            if (val.match(/\s/) !== null) {
                errorFormat.eq(index).css('display', 'block');

            // if the name is format is ok, remove errors
            } else {
                errorFormat.eq(index).css('display', 'none');
                errorName.eq(index).css('display', 'none');

                // and confirm the name does not already exist in the database
                utilities.ajaxRequest({
                    url: utilities.profilesEndPoint,
                    data: {
                        route: 'email/validate-email-variable',
                        data: {
                            name: val,
                            pub_code: emailPubSelector.val()
                        }
                    },
                    dataType: 'json',
                    callBack: function(r) {
                        if (typeof r[0] != 'undefined' && r[0]) {
                            errorName.eq(index).css('display', 'block');
                        } else {
                            if (window.confirm('Do you want to create the variable name "' + val + '" ?')) {
                                // set empty value '.' (not a space) to create the record in the DB
                                saveVariableName(val, '.', variableNames.eq(index), variableValues.eq(index));
                            }
                        }
                    }
                });
            }
        });

        variableValues.unbind('change');
        variableValues.change(function() {
            var self = $(this),
                val = self.val(),
                index = self.index('input.email_variable_value'),
                name = variableNames.eq(index).val();

            saveVariableName(name, val);
        });

        deleteVar.unbind('click');
        deleteVar.click(function() {
            var self = $(this),
                index = self.index('button.delete_variable'),
                parent = varRows.eq(index),
                name = variableNames.eq(index).val();

            if (window.confirm('Are you sure you want to delete this variable?')) {
                utilities.ajaxRequest({
                    url: utilities.profilesEndPoint,
                    data: {
                        route: 'email/delete-email-variable',
                        data: {
                            name: name,
                            pub_code: emailPubSelector.val()
                        }
                    },
                    dataType: 'json'

                });
                parent.remove();
                bindEmailVariableInputs();                
            }
        });
    }

    /**
     * save the variable name to the DB and update the interface
     * @param  {string} name
     * @param  {string} value
     * @param  {object} inputName 
     * @param  {object} inputValue 
     * @return {null}     
     */
    function saveVariableName(name, value, inputName, inputValue) {
        var pub_code = emailPubSelector.val();

        utilities.ajaxRequest({
            url: utilities.profilesEndPoint,
            data: {
                route: 'email/save-email-variable',
                data: {
                    name: name,
                    value: value,
                    pub_code: pub_code
                }
            },
            dataType: 'json',
            callBack: function(r) {
                if (typeof inputName != 'undefined' && typeof inputValue != 'undefined') {
                    // disable the name input so it can't be edited
                    inputName.prop('disabled', true);
                    inputValue.prop('disabled', false);
                } else {
                    alert('The variable has been saved to ' + name + ' = ' + value + ' for pub code ' + pub_code);
                }
            }
        });
    }

    return {
        init: function() {
            init();
        }
    }
}

var premiumSets = PremiumSetsClass();

function PremiumSetsClass()
{
    var premium_pub = {},
        premium_wrap = {},
        premium_row_clone = {},
        add_premium = {},
        premium_input_row_clone = {},
        premium_input_row = {},
        premium_row = {},
        pub_code = '';

    function init()
    {
        setElements();
        getPubCode();
        bindHeader();
        clonePremiumRows();
        bindPremiumRowInputs();
        getAvailableReports(pub_code);
    }

    function setElements()
    {        
        premium_wrap = $('#premium_set_wrap');
        premium_pub = $('select[name="premium_pub"]');
    }

    function getPubCode()
    {
        var cookie = $.cookie(cookieEmailPub);        
        pub_code = (typeof cookie == 'undefined') ? premium_pub.val() : cookie;
    }

    function getAvailableReports(pub_code)
    {
        utilities.ajaxRequest({
            url: ajaxurl,
            data: {
                action: 'getPremiumReports',
                pub_code: pub_code
            },
            callBack: function(reports) {
                setReportsSelector(reports);
                premium_wrap.html('');
                getPremiums(pub_code);
            }
        });
    }

    /**
     * update the selector if no reports have been saved in profiles
     * @param {null} reports
     */
    function setReportsSelector(reports)
    {
        var options = $(reports).html(),
            selector = $('select[name="premium_report"]');

        // set the default value
        premium_input_row.find('select[name="premium_report"]').html(options);

        // set all of the currently show records
        selector.html(options);
    }

    function bindHeader()
    {
        var add_premium_set = $('#add_premium_set');

        // set the saved pub
        premium_pub.val(pub_code);

        premium_pub.on('change', function() {
            var self = $(this),
                val = self.val();

            setProfilesCookie(cookieEmailPub, val);
            getAvailableReports(val)
        });

        add_premium_set.on('click', function() {
            appendPremiumSet();
        });
    }

    function getPremiums(pub_code) {
        utilities.ajaxRequest({
            url: utilities.profilesEndPoint,
            data: {
                route: 'premium-sets/get-by-pub',
                data: {
                    pub_code: pub_code
                }
            },
            callBack: function(premiums) {
                showPubPremiums(premiums, true);
            },
            dataType: 'json'
        });
    }

    function showPubPremiums(premiums, refresh)
    {
        // first clear the wrapper
        if (refresh) {
            premium_wrap.html('');            
        }

        appendPremiumSet(premiums, refresh);
    }

    function clonePremiumRows()
    {
        // get a copy of the input row html first
        premium_input_row_clone = $('div.premium_input_row');
        premium_input_row = premium_input_row_clone.eq(0).clone();
        // remove the cloned version
        premium_input_row_clone.eq(0).remove();

        // now get a copy of the entire premium set row html
        premium_row_clone = $('div.premium_set_row');
        premium_row = premium_row_clone.eq(0).clone();
        // remove the cloned version
        premium_row_clone.eq(0).remove();
    }

    function appendPremiumSet(premiums, refresh)
    {
        // get a clone of the premium_set_row
        var premium = $(premium_row.clone().prop('outerHTML'));

        if (typeof premiums == 'undefined' || (typeof premiums != 'undefined' && premiums.length === 0)) {
            // add a clone of inputs back into the premium_set_row
            premium_input_row.clone().appendTo(premium.find('div.premiums_collection'));
            
            // append the premium_set_row back into the wrapper
            premium_wrap.append(premium);

            // and then rebind all inputs
            bindPremiumRowInputs();

        } else {
            populatePremiumSets(premiums, refresh);
        }
    }

    /**
     * get reports for each premium set from WP and 
     * then populate the interface
     * @param  {array} premiums
     * @return {null}
     */
    function populatePremiumSets(premiums, refresh)
    {
        var report_ids = [],
            counter = 0,
            ps = JSON.parse(JSON.stringify(premiums)); // remove deep-linking

        for (var i = 0; i < ps.length; i++) {
            for (var j = 0; j < ps[i].post_ids.length; j++) {
                report_ids[counter] = ps[i].post_ids[j];
                counter++;
            }
        }

        utilities.ajaxRequest({
            url: ajaxurl,
            data: {
                action: 'getReportsById',
                report_ids: JSON.stringify(report_ids)
            },
            callBack: function(reports) {
                premiums = mergeReportPremiums(reports, ps);
                showPremiumSets(premiums, refresh);
            }
        });
    }

    /**
     * @param  {array} premiums
     * @return {null}         
     */
    function showPremiumSets(premiums, refresh)
    {
        var premium = {},
            set = {},
            link = {},
            title = {},
            index = 0;

        if (refresh) {
            for (var i = 0; i < premiums.length; i++) {
                premium = $(premium_row.clone().prop('outerHTML'));

                // set choice code
                premium.find('input.premium_choice_code').val(premiums[i].choice_code);
                premium.find('textarea.premium_description').val(premiums[i].description);

                // append the premium_set_row back into the wrapper
                premium_wrap.append(premium);

                for (var j = 0; j < premiums[i].post_ids.length; j++) {
                    premium_input_row.clone().appendTo(premium.find('div.premiums_collection'));
                    set = $('div.premiums_collection');

                    link = set.eq(i).find('input.premium_report_link').eq(j);
                    link.val(premiums[i].post_ids[j].guid);
                    link.attr('data-dont-change', '1');

                    title = set.eq(i).find('select[name="premium_report"]').eq(j);
                    title.val(premiums[i].post_ids[j].post_title);
                }
            }

        // if not a refresh, it's a single copy of another premium
        } else {
            premium = $(premium_row.clone().prop('outerHTML'));

            // set choice code
            premium.find('input.premium_choice_code').val(premiums[0].choice_code);
            premium.find('textarea.premium_description').val(premiums[0].description);

            // append the premium_set_row back into the wrapper
            premium_wrap.append(premium);

            for (var j = 0; j < premiums[0].post_ids.length; j++) {
                premium_input_row.clone().appendTo(premium.find('div.premiums_collection'));
                set = $('div.premiums_collection');
                index = set.length - 1;

                link = set.eq(index).find('input.premium_report_link').eq(j);
                link.val(premiums[0].post_ids[j].guid);
                link.attr('data-dont-change', '1');

                title = set.eq(index).find('select[name="premium_report"]').eq(j);
                title.val(premiums[0].post_ids[j].post_title);
            }
        }


        bindPremiumRowInputs();
    }

    /**
     * merge the WP reports with premium report ids saved in profiles
     * @param  {array} reports 
     * @param  {array} premiums
     * @return {array}         
     */
    function mergeReportPremiums(reports, premiums)
    {
        for (var i = 0; i < premiums.length; i++) {
            for (var j = 0; j < premiums[i].post_ids.length; j++) {
                for (var k = 0; k < reports.length; k++) {
                    if (premiums[i].post_ids[j] == reports[k].post_id) {
                        premiums[i].post_ids[j] = reports[k];
                    }
                }
            }
        }

        return premiums;
    }

    function bindPremiumRowInputs()
    {
        savePremiumSet();
        deletePremiumSets();
        addPremiumItem();
        deletePremiumItem();
        selectPremiumReport();
        bindNameCheck();
        copyPremiumSet();
    }

    function savePremiumSet()
    {
        var save = $('button.save_premium_set');

        save.unbind('click');
        save.on('click', function() {
            var self = $(this),
                index = self.index('button.save_premium_set'),
                premium_set = self.parents('div.premium_set_row'),
                choice_code = premium_set.find('input.premium_choice_code'),
                choice_error_missing = premium_set.find('p.premium_choice_missing'),
                choice_error_syntax = premium_set.find('p.premium_choice_syntax'),
                description = premium_set.find('textarea.premium_description'),
                description_error = premium_set.find('p.premium_description_error'),
                reports = premium_set.find('select[name="premium_report"]'),
                post_ids = [],
                premium = {};

            // get the report IDs
            for (var i = 0; i < reports.length; i++) {
                post_ids[i] = 1 * reports.eq(i).find(':selected').data('post-id')
            }

            premium.domain = getSiteDomain();
            premium.pub_code = premium_pub.val();
            premium.choice_code = choice_code.val();
            premium.description = description.val();
            premium.post_ids = post_ids;

            if (premium.choice_code === '') {
                choice_error_missing.css('display', 'block');

            } else if (premium.choice_code.match(/\s/)) {
                choice_error_syntax.css('display', 'block');

            } else if (premium.description === '') {
                description_error.css('display', 'block');

            } else {
                premium.pub_code = premium_pub.val();
                utilities.ajaxRequest({
                    url: utilities.profilesEndPoint,
                    data: {
                        route: 'premium-sets/save',
                        data: premium
                    },
                    callBack: function(response) {
                        window.alert('Save = ' + response)
                    },
                    dataType: 'json'
                });
            }

            return true;
        });
    }

    function deletePremiumSets()
    {
        var delete_set = $('button.delete_premium_set');

        delete_set.unbind('click');
        delete_set.on('click', function() {
            var self = $(this),
                premium_set = self.parents('div.premium_set_row'),
                choice_code = premium_set.find('input.premium_choice_code'),
                premium = {};

            if (!window.confirm('Are you sure you want to delete this premium set?')) {
                return false;
            }

            premium_set.remove();
            bindPremiumRowInputs();

            premium.domain = getSiteDomain();
            premium.pub_code = premium_pub.val();
            premium.choice_code = choice_code.val();

            utilities.ajaxRequest({
                url: utilities.profilesEndPoint,
                data: {
                    route: 'premium-sets/delete',
                    data: premium
                },
                callBack: function(response) {},
                dataType: 'json'
            });
        });
    }

    function addPremiumItem()
    {
        var add_premium = $('button.add_premium');

        add_premium.unbind('click');
        add_premium.on('click', function() {
            var self = $(this),
                index = self.index('button.delete_premium_set'),
                parent = self.parents('div.premium_set_row'),
                collection = parent.find('div.premiums_collection');

            collection.append(premium_input_row.clone().eq(0));
            bindPremiumRowInputs();
        });
    }

    function deletePremiumItem()
    {
        var delete_premium = $('button.delete_premium');

        delete_premium.unbind('click');
        delete_premium.on('click', function() {
            var self = $(this),
                parent = self.parents('div.premium_input_row');

            parent.remove();
            bindPremiumRowInputs();
        });
    }

    function selectPremiumReport()
    {
        var selector = $('select[name="premium_report"]'),
            links = $('input.premium_report_link');

        // fill in link values
        for (var i = 0; i < links.length; i++) {
            if (links.eq(i).val() === '') {
                links.eq(i).val(selector.eq(i).find(':selected').data('report-guid'));
            }
        }

        selector.unbind('change');
        selector.on('change', function() {
            var self = $(this),
                index = self.index('select[name="premium_report"]'),
                link = self.find(':selected').data('report-guid');

            links.eq(index).val(link);
        });
    }

    function bindNameCheck()
    {
        var choice_code = $('input.premium_choice_code');

        choice_code.unbind('change');
        choice_code.on('change', function() {
            var self = $(this),
                val = self.val(),
                premium = {};

            premium.domain = getSiteDomain();
            premium.pub_code = premium_pub.val();
            premium.choice_code = val;

            utilities.ajaxRequest({
                url: utilities.profilesEndPoint,
                data: {
                    route: 'premium-sets/name-check',
                    data: premium
                },
                callBack: function(response) {
                    if (response[0]) {
                        self.siblings('p.premium_choice_existing').css('display', 'block');
                    }
                },
                dataType: 'json'
            });
        });
    }

    function copyPremiumSet()
    {
        var copy = $('button.copy_premium_set');

        copy.unbind('click');
        copy.on('click', function() {
            var self = $(this),
                parent = self.parents('div.premium_set_row'),
                premiums = parent.find('select[name="premium_report"]'),
                reports = [],
                copied_set = {};

            for (var i = 0; i < premiums.length; i++) {
                reports[i] = premiums.eq(i).find(':selected').data('post-id');
            }

            copied_set = {
                choice_code: '',
                pub_code: premium_pub.val(),
                description: parent.find('textarea.premium_description').val(),
                post_ids: reports
            };

            showPubPremiums([copied_set]);
        });
    }

    return {
        init: function() {
            init();
        }
    }
}

var tempOrderUi = tempOrderUiClass();

function tempOrderUiClass()
{
    var emailPubSelector = {},
        email = {},
        email_name = {},
        data_preview = {},
        premium_name = {},
        email_address = {},
        premium_sets = [],
        user_credentials = {
            username: '',
            password: ''
        };

    function construct()
    {
        emailPubSelector = $('select[name="email_pub"]');
        email_address = $('#temp-email');
        email_name = $('select[name="email_name"]');
        premium_name = $('select[name="premium_name"]');
        data_preview = $('.temp-data-preview');
        itemdescription = $('#itemdescription'),
        setEmailPublication();
        getEmails();
        getPremiumSets();
        bindPreviewData();
        bindCreateTempOrder();
        bindConfirmAccount();
    }

    function setEmailPublication()
    {
        var cookie = $.cookie(cookieEmailPub),
            value = (typeof cookie == 'undefined') ? emailPubSelector.val() : cookie;

        // get the value for other methods
        email.pub = value;

        // set the default value on load
        setProfilesCookie(cookieEmailPub, value);

        // set the actual select value, which might be set by the cookie
        emailPubSelector.val(value);
        
        // update values on change
        emailPubSelector.change(function() {
            var self = $(this),
                value = self.val();

            email.pub = value;
            emailPubSelector.val(value);
            setProfilesCookie(cookieEmailPub, value);
            getEmails();
            getPremiumSets();
        });
    }

    function getEmails()
    {
        utilities.ajaxRequest({
            url: utilities.profilesEndPoint,
            data: {
                route: 'email/get-emails',
                data: {
                    pub: $.cookie(cookieEmailPub),
                    type: 'order_confirmation'
                }
            },
            callBack: function(response) {
                if (response.length > 0) {
                    populateEmailSelector(response);
                } else {
                    email_name.html('');
                }
            },
            dataType: 'json'
        });
    }

    function populateEmailSelector(emails)
    {
        var html = '';

        for (var i = 0; i < emails.length; i++) {
            html += '<option value="' + emails[i].name + '">' + emails[i].name + '</option>';
        }

        email_name.html(html);
    }

    function getPremiumSets()
    {
        utilities.ajaxRequest({
            url: utilities.profilesEndPoint,
            data: {
                route: 'premium-sets/get-by-pub',
                data: {
                    pub_code: $.cookie(cookieEmailPub)
                }
            },
            callBack: function(response) {
                if (response.length > 0) {
                    getPremiumReports(response);
                    populatePremiumSelector(response);
                } else {
                    premium_name.html('');
                }
            },
            dataType: 'json'
        });
    }

    function populatePremiumSelector(premiums)
    {
        var html = '';

        for (var i = 0; i < premiums.length; i++) {
            html += '<option value="' + premiums[i].choice_code + '">' + premiums[i].choice_code + '</option>';
        }

        premium_name.html(html);
    }

    function getPremiumReports(premiums)
    {
        var report_ids = [],
            counter = 0,
            ps = JSON.parse(JSON.stringify(premiums)); // remove deep-linking

        for (var i = 0; i < ps.length; i++) {
            for (var j = 0; j < ps[i].post_ids.length; j++) {
                report_ids[counter] = ps[i].post_ids[j];
                counter++;
            }
        }

        utilities.ajaxRequest({
            url: ajaxurl,
            data: {
                action: 'getReportsById',
                report_ids: JSON.stringify(report_ids)
            },
            callBack: function(response) {
                if (response.length > 0) {
                    premium_sets = mergeReportPremiums(response, ps);
                } else {
                    premium_sets = [];
                }
            }
        });
    }

    /**
     * merge the WP reports with premium report ids saved in profiles
     * @param  {array} reports 
     * @param  {array} premiums
     * @return {array}         
     */
    function mergeReportPremiums(reports, premiums)
    {
        for (var i = 0; i < premiums.length; i++) {
            for (var j = 0; j < premiums[i].post_ids.length; j++) {
                for (var k = 0; k < reports.length; k++) {
                    if (premiums[i].post_ids[j] == reports[k].post_id) {
                        premiums[i].post_ids[j] = reports[k];
                    }
                }
            }
        }

        return premiums;
    }

    function bindPreviewData()
    {
        var view_email = $('#view-email'),
            view_premium = $('#view-premium');

        view_email.on('click', function() {
            utilities.ajaxRequest({
                url: utilities.profilesEndPoint,
                data: {
                    route: 'email/get-email-content',
                    data: {
                        name: email_name.val()
                    }
                },
                callBack: function(response) {
                    showModal('#temp-data-preview', 'show');

                    if (response.length === 1) {
                        populateEmailPreview(response[0].content);                        
                    }
                },
                dataType: 'json'
            });

        });

        view_premium.on('click', function() {
            showModal('#temp-data-preview', 'show');
            populatePremiumSetPreview();
        });
    }

    function populateEmailPreview(email_content)
    {
        var doc = document.getElementById('preview_email').contentWindow.document;
        doc.open();
        doc.write(email_content);
        doc.close();
        $('.temp-data-preview').css('display', 'none');
        $('#preview_email').css('display', 'block');
    }

    function populatePremiumSetPreview()
    {
        var choice_code = premium_name.val(),
            premium_set_preview = $('#premium_set_preview'),
            reports = '';

        for (var i = 0; i < premium_sets.length; i++) {
            if (choice_code == premium_sets[i].choice_code) {
                reports += '<h4>Premium Set: ' + premium_sets[i].choice_code  + '</h4>';
                reports += '<h4>Description: ' + premium_sets[i].description  + '</h4>';

                for (var j = 0; j < premium_sets[i].post_ids.length; j++) {
                    reports += '<a class="temp-report-preview" href="' + premium_sets[i].post_ids[j].guid + '" target="_blank">' + premium_sets[i].post_ids[j].post_title + '</a>';
                }
            }
        }

        $('.temp-data-preview').css('display', 'none');
        premium_set_preview.html(reports).css('display', 'block');
    }

    function bindCreateTempOrder()
    {
        var create = $('#create-temp-order');

        create.on('click', function() {
            var temp_email = email_address.val(),
                cost = $('#temp-order').val();

                error_message = $('#error-message');

            if (temp_email == '' || cost == '') {
                error_message.css('display', 'block');
                return false;
            } else {
                error_message.css('display', 'none');
                createTempOrder(
                    emailPubSelector.val(),
                    email_name.val(),
                    premium_name.val(),
                    temp_email,
                    cost,
                    itemdescription
                );
                showModal('#temp-data-preview', 'show');
            }
        });
    }

    function createTempOrder(pub, emailname, premium, customer_email, cost, itemdescription)
    {
        utilities.ajaxRequest({
            url: utilities.profilesEndPoint,
            data: {
                route: 'email/send-temp-account',
                data: {
                    pub_code: pub,
                    email_name: emailname,
                    premium: premium,
                    email: customer_email,
                    username: user_credentials.username,
                    password: user_credentials.password,
                    cost: cost,
                    itemdescription: itemdescription.val()
                }
            },
            callBack: function(response) {
                showTempOrderConfirmation(response);
            },
            dataType: 'json'
        });

        sendAnalyticsReport(pub, emailname);

        // clear the credentials so they're not used again
        user_credentials.username = '';
        user_credentials.password = '';
        email_address.val('');
        $('#temp-order').val('');
        itemdescription.val('');
        populateCredentials('clear');
    }

    function sendAnalyticsReport(pub, emailname)
    {
        ga('send', 'event', {
            'eventCategory': 'Create Temp Account',
            'eventAction': pub + ' | ' + emailname,
            'eventLabel': 'User Id: ' + current_wp_user_id
        });
    }

    function showTempOrderConfirmation(confirm)
    {
        var confirm_account = $('#confirm_temp_account'),
            confirm_temp_success = $('#confirm_temp_success'),
            confirm_temp_fail = $('#confirm_temp_fail'),
            confirm_user = $('#confirm_temp_user'),
            confirm_pass = $('#confirm_temp_pass');

        data_preview.css('display', 'none');
        confirm_account.css('display', 'block');

        if (typeof confirm.username != 'undefined') {
            confirm_user.text(confirm.username);
            confirm_pass.text(confirm.password);
            logoutCustomer(confirm.username);
            confirm_temp_success.css('display', 'block');
            confirm_temp_fail.css('display', 'none');

        } else {
            confirm_user.text('');
            confirm_pass.text('');
            confirm_temp_success.css('display', 'none');
            confirm_temp_fail.css('display', 'block');
        }
    }

    function logoutCustomer(username)
    {
        utilities.ajaxRequest({
            url: ajaxurl,
            data: {
                action: 'logoutCustomer',
                user_login: username
            },
            callBack: function(response) {}
        });
    }

    /**
     * get the user's credentials if any exist
     * @return {null}
     */
    function bindConfirmAccount()
    {
        email_address.unbind('change');
        email_address.on('change', function() {
            var self = $(this),
                val = self.val();

            // get existing credentials
            utilities.ajaxRequest({
                url: utilities.profilesEndPoint,
                data: {
                    route: 'user/record/get-accounts-by-email',
                    data: {
                        email: val
                    }
                },
                callBack: function(response) {
                    populateCredentials(response);
                },
                dataType: 'json'
            });
        });
    }

    function populateCredentials(creds)
    {
        var wrapper = $('#creds-wrapper'),
            html = '';

        if (creds == 'clear') {
            html += '';
            user_credentials.username = '';
            user_credentials.password = '';

        } else if (creds.length === 0) {
            html += '<p>No existing credentials could be found</p>';
            html += '<p><strong>New credentials</strong> will be automatically generated</p>';
            user_credentials.username = '';
            user_credentials.password = '';

        } else {

            for (var i = 0; i < creds.length; i++) {
                html += '<div class="user-creds-wrap">';
                    html += '<input id="usercred' + i + '" type="radio" name="user-creds" data-username="' + creds[i].id.userName + '" data-password="' + creds[i].password + '" />';
                    html += '<label for="usercred' + i + '">';
                    html += creds[i].customerNumber;
                    html += ' | Login: ' + creds[i].id.userName;
                    html += ' | Password: ' + creds[i].password;
                    html += '</label>';
                html += '</div>';
            }
        }

        wrapper.html(html);
        bindUserCreds();
    }

    function bindUserCreds()
    {
        var creds = $('input[name="user-creds"]');

        creds.on('click', function() {
            var self = $(this);
            user_credentials.username = self.data('username');
            user_credentials.password = self.data('password');
        })
    }

    return {
        construct: function() {
            $(document).ready(function() {
                construct();
            });
        }
    }
}


//NEW
var genericEmailUi = genericEmailUiClass();

function genericEmailUiClass()
{
    var emailPubSelector = {},
        email = {},
        email_name = {},
        data_preview = {},
        email_address = {};

    function construct()
    {
        emailPubSelector = $('select[name="email_pub"]');
        email_address = $('#temp-email');
        email_name = $('select[name="email_name"]');
        data_preview = $('.temp-data-preview');
        itemdescription = $('#itemdescription');
        setEmailPublication();
        getEmails();
        bindPreviewData();
        createEmail();
    }

    function setEmailPublication()
    {
        var cookie = $.cookie(cookieEmailPub),
            value = (typeof cookie == 'undefined') ? emailPubSelector.val() : cookie;

        // get the value for other methods
        email.pub = value;

        // set the default value on load
        setProfilesCookie(cookieEmailPub, value);

        // set the actual select value, which might be set by the cookie
        emailPubSelector.val(value);
        
        // update values on change
        emailPubSelector.change(function() {
            var self = $(this),
                value = self.val();

            email.pub = value;
            emailPubSelector.val(value);
            setProfilesCookie(cookieEmailPub, value);
            getEmails();
        });
    }

    function getEmails()
    {
        utilities.ajaxRequest({
            url: utilities.profilesEndPoint,
            data: {
                route: 'email/get-emails',
                data: {
                    pub: $.cookie(cookieEmailPub),
                    type: 'generic_emails'
                }
            },
            callBack: function(response) {
                if (response.length > 0) {
                    populateEmailSelector(response);
                } else {
                    email_name.html('');
                }
            },
            dataType: 'json'
        });
    }

    function populateEmailSelector(emails)
    {
        var html = '';

        for (var i = 0; i < emails.length; i++) {
            html += '<option value="' + emails[i].name + '">' + emails[i].name + '</option>';
        }

        email_name.html(html);
    }



    function bindPreviewData()
    {
        var view_email = $('#view-email')

        view_email.on('click', function() {
            utilities.ajaxRequest({
                url: utilities.profilesEndPoint,
                data: {
                    route: 'email/get-email-content',
                    data: {
                        name: email_name.val()
                    }
                },
                callBack: function(response) {
                    showModal('#temp-data-preview', 'show');

                    if (response.length === 1) {
                        populateEmailPreview(response[0].content);                        
                    }
                },
                dataType: 'json'
            });

        });

    }

    function populateEmailPreview(email_content)
    {
        var doc = document.getElementById('preview_email').contentWindow.document;
        doc.open();
        doc.write(email_content);
        doc.close();
        $('.temp-data-preview').css('display', 'none');
        $('#preview_email').css('display', 'block');
    } 


    function createEmail()
    {
        var create = $('#send-email');

        create.on('click', function() {

            var temp_email = email_address.val();

            error_message = $('#error-message');

            utilities.ajaxRequest({
                url: utilities.profilesEndPoint,
                data: {
                    route: 'email/send-email',
                    data: {
                        email : temp_email,
                        email_name : email_name.val(),
                        email_type : 'generic_emails'
                    }
                },
                callBack: function(response) {
                    showConfirmation(response);
                },
                dataType: 'json'
            });
            
        });
    }

    function showConfirmation(confirm)
    {
        if (confirm) {
            alert ('The email was successfully sent to the queue. it will be queued to send to the recipient shortly.');
        }
        else {
            alert ('There seems to be an error. wait one minute and try again');
        }
    }

    return {
        construct: function() {
            $(document).ready(function() {
                construct();
            });
        }
    }

}

var webViews = webViewsClass();

function webViewsClass()
{
    var email_input = {},
        view_data = {},
        account_data = {},
        date_start = {},
        date_end = {},
        loading_icon = {},
        tag_wrapper = {},
        devices_wrapper = {};

    function loadAdminPage()
    {
        email_input = $('#web-view-email');
        view_data = $('#web-view-response');
        account_data = $('#account-details-response');
        date_start = $('#date_start');
        date_end = $('#date_end');
        loading_icon = $('#loading-icon-wrapper');
        tag_wrapper = $('#web-view-tags');
        devices_wrapper = $('#web-view-devices');

        setDatePickers();
        getHistory();
        toggleViews();
    }

    function setDatePickers()
    {
        date_start.datepicker({
            dateFormat: "yy-mm-dd"
        });

        date_end.datepicker({
            dateFormat: "yy-mm-dd"
        });
    }

    function getHistory()
    {
        $('#web-view-get').on('click', function() {
            var email = email_input.val(),
                start_date = date_start.val(),
                end_date = date_end.val();

            if (email === '' || start_date === '' || end_date === '') {
                alert('Enter an email and date range');
            } else {
                // clear existing report
                tag_wrapper.html('');
                devices_wrapper.html('');
                view_data.html('');
                account_data.html('');
                loading_icon.css('display', 'block');
                requestAccountData(email);
                requestHistory(email, start_date, end_date);
                requestTags(email, start_date, end_date);
                requestDevices(email, start_date, end_date);
            }
        });
    }

    function toggleViews()
    {
        var show_interactions = $('#show-customer-interactions'),
            show_details = $('#show-customer-details');

        show_interactions.on('click', function() {
            view_data.css('display', 'block');
            account_data.css('display', 'none');
        });

        show_details.on('click', function() {
            view_data.css('display', 'none');
            account_data.css('display', 'block');
        })
    }

    /**
     * @param  string $email
     * @param  string $date_start 2016-02-01
     * @param  string $date_end   2016-02-29
     * @return array
     */
    function requestAccountData(email)
    {
        // get existing credentials
        utilities.ajaxRequest({
            url: utilities.profilesEndPoint,
            data: {
                route: 'reports/customer-details',
                data: {
                    email: email
                }
            },
            callBack: function(response) {
                populateCustomerDetails(response);
            },
            dataType: 'json'
        });
    }

    /**
     * @param  {object} response
     * @return {null}         
     */
    function populateCustomerDetails(response)
    {
        var html = createAccountCredsView(response);
        html += createAccountPubsView(response);
        account_data.append(html)
    }

    /**
     * @param  {object} response
     * @return {string}         
     */
    function createAccountCredsView(response)
    {
        if (typeof response.account == 'undefined') return '';

        var html = '',
            row = {};

        html += '<div class="row"><div class="small-12 columns"><h3>User Account</h3></div></div>';

        for (var i = 0; i < response.account.length; i++) {
            row = response.account[i];

            html += '<div class="row data-table-row">' +
                '<div class="small-3 columns">' +
                    '<p>User: ' + row.id.userName + '</p>' +
                '</div>' +
                '<div class="small-2 columns">' +
                    '<p>Pass: ' + row.password + '</p>' +
                '</div>' +
                '<div class="small-2 columns">' +
                    '<p>authStatus: ' + row.authStatus + '</p>' +
                '</div>' +
                '<div class="small-2 end columns">' +
                    '<p>temp: ' + (row.temp == true ? 'true':'false') + '</p>' +
                '</div>' +
            '</div>';
        }

        return html;
    }

    /**
     * @param  {object} response
     * @return {string}         
     */
    function createAccountPubsView(response)
    {
        if (typeof response.subs == 'undefined') return '';

        var html = '',
            row = {};

        html += '<div class="row"><div class="small-12 columns"><h3>User Subscriptions</h3></div></div>';

        for (var i = 0; i < response.subs.length; i++) {
            row = response.subs[i];

            html += '<div class="row data-table-row">' +
                '<div class="small-1 columns">' +
                    '<p>' + row.pubcode + '</p>' +
                '</div>' +
                '<div class="small-3 columns">' +
                    '<p>' + row.name + '</p>' +
                '</div>' +
                '<div class="small-2 columns">' +
                    '<p>Start: ' + row.date_start + '</p>' +
                '</div>' +
                '<div class="small-2 columns">' +
                    '<p>Expires: ' + row.date_expiration + '</p>' +
                '</div>' +
                '<div class="small-2 columns">' +
                    '<p>Renew: ' + row.renew_method + '</p>' +
                '</div>' +
                '<div class="small-1 end columns">' +
                    '<p>' + (row.backend == 0 ? 'Frontend' : (row.backend == 2 ? 'Reserve' : 'Backend')) + '</p>' +
                '</div>' +
            '</div>';
        }

        return html;
    }

     /**
     * @param  string $email
     * @param  string $date_start 2016-02-01
     * @param  string $date_end   2016-02-29
     * @return array
     */
    function requestHistory(email, date_start, date_end)
    {
        // get existing credentials
        utilities.ajaxRequest({
            url: utilities.profilesEndPoint,
            data: {
                route: 'reports/customer-web-views',
                data: {
                    email: email,
                    date_start: date_start,
                    date_end: date_end
                }
            },
            callBack: function(response) {
                loading_icon.css('display', 'none');
                populateDataTable(response)
            },
            dataType: 'json'
        });
    }

    /**
     * from the returned data, fire off a data_methods function for a given data_type
     * @param  {[type]} data [description]
     * @return {[type]}      [description]
     */
    function populateDataTable(data)
    {
        var method = '';

        for (var i = 0; i < data.length; i++) {
            // get data_type name to call the needed method
            method = data[i].data_type;
            // call the method with the return data row
            data_methods[method](data[i]);
        }
    }

    /**
     * from the returned data, fire off a data_methods function for a given data_type
     */
    var data_methods = {
        email: function(row) {
            var html = '<div class="row data-table-row">' +
                '<div class="small-2 columns">' +
                    '<p>' + row.dateTime + '</p>' +
                '</div>' +
                '<div class="small-1 columns">' +
                    '<p>Email</p>' +
                '</div>' +
                '<div class="small-2 columns">' +
                    '<p>' + row.list_name + '</p>' +
                '</div>' +
                '<div class="small-2 columns">' +
                    '<p>' + row.mailing_name + '</p>' +
                '</div>' +
                '<div class="small-1 columns">' +
                    '<p>' + (row.bounced == 0 ? 'Delivered' : 'Bounced') + '</p>' +
                '</div>' +
                '<div class="small-2 end columns">' +
                    '<p>' + (row.opened == 0 ? 'Not Opened' : 'Opened') + '</p>' +
                '</div>' +
            '</div>';
            view_data.append(html)
        },
        pageview: function(row) {
            var html = '<div class="row data-table-row">' +
                '<div class="small-2 columns">' +
                    '<p>' + row.dateTime + '</p>' +
                '</div>' +
                '<div class="small-1 columns">' +
                    '<p>Pageview</p>' +
                '</div>'+
                '<div class="small-2 columns">' +
                    '<p>' + row.agoraDomain + '</p>' +
                '</div>'+
                '<div class="small-4 end columns">' +
                    '<p>' + row.pagePath + '</p>' +
                '</div>'+
            '</div>';
            view_data.append(html)
        },
        event: function(row) {
            var html = '<div class="row data-table-row">' +
                '<div class="small-2 columns">' +
                    '<p>' + row.dateTime + '</p>' +
                '</div>' +
                '<div class="small-1 columns">' +
                    '<p>Event</p>' +
                '</div>'+
                '<div class="small-2 columns">' +
                    '<p>' + row.agoraDomain + '</p>' +
                '</div>'+
                '<div class="small-2 columns">' +
                    '<p>' + row.eventCategory + '</p>' +
                '</div>'+
                '<div class="small-2 end columns">' +
                    '<p>' + row.eventAction + '</p>' +
                '</div>'+
            '</div>';
            view_data.append(html)
        },
        inbound_call: function(row) {
            var html = '<div class="row data-table-row">' +
                '<div class="small-2 columns">' +
                    '<p>' + row.dateTime + '</p>' +
                '</div>' +
                '<div class="small-1 columns">' +
                    '<p>CS Contact</p>' +
                '</div>'+
                '<div class="small-1 columns">' +
                    '<p>Medium: ' + row.contactMedium.toLowerCase() + '</p>' +
                '</div>'+
                '<div class="small-1 columns">' +
                    '<p>Reason: ' + row.contactReason.toLowerCase() + '</p>' +
                '</div>'+
                '<div class="small-1 columns">' +
                    '<p>Category: ' + row.reasonCategory.toLowerCase() + '</p>' +
                '</div>'+
                '<div class="small-1 columns">' +
                    '<p>Sys: ' + row.systemName.toLowerCase() + '</p>' +
                '</div>'+
                '<div class="small-1 columns">' +
                    '<p>User: ' + row.userIdentification.toLowerCase() + '</p>' +
                '</div>'+
                '<div class="small-1 end columns">' +
                    '<p>Upsell: ' + row.upsellPath.toLowerCase() + '</p>' +
                '</div>'+
            '</div>';
            view_data.append(html);
        }
    }

    /**
     * @param  string $email
     * @param  string $date_start 2016-02-01
     * @param  string $date_end   2016-02-29
     * @return array
     */
    function requestTags(email, date_start, date_end)
    {
        utilities.ajaxRequest({
            url: utilities.profilesEndPoint,
            data: {
                route: 'reports/customer-web-view-tags',
                data: {
                    email: email,
                    date_start: date_start,
                    date_end: date_end
                }
            },
            callBack: function(response) {
                populateTags(response);
            },
            dataType: 'json'
        });
    }

    /**
     * @param  string $email
     * @param  string $date_start 2016-02-01
     * @param  string $date_end   2016-02-29
     * @return array
     */
    function requestDevices(email, date_start, date_end)
    {
        utilities.ajaxRequest({
            url: utilities.profilesEndPoint,
            data: {
                route: 'reports/customer-web-view-device',
                data: {
                    email: email,
                    date_start: date_start,
                    date_end: date_end
                }
            },
            callBack: function(response) {
                populateDevices(response);
            },
            dataType: 'json'
        });
    }

    /**
     * @param  {object} response tags in "tag buckets"
     * @return {null}
     */
    function populateTags(response)
    {
        if (typeof response.sent == 'undefined') return null;

        var html = '';

        for (var i = 0; i < response.sent.length; i++) {
            html += '<button class="tiny round web-view-tag">' + response.sent[i] + '</button>'
        }

        tag_wrapper.append(html);
    }

    /**
     * @param  {object} response tags in "tag buckets"
     * @return {null}
     */
    function populateDevices(response)
    {
        if (typeof response.devices == 'undefined') return null;

        var html = '',
            devices = response.devices;

        for (hash in devices) {
            html += '<button class="tiny round web-view-tag">' + devices[hash].operatingSystem + ': ' + devices[hash].browser + '</button>';
        }

        devices_wrapper.append(html);
    }

    return {
        construct: function() {
            loadAdminPage();
        }
    }
}

return {
    config: function(obj) {
        for (o in obj) {
            if (o == 'profilesEndPoint') {
                utilities.profilesEndPoint = obj[o];

            } else if (o == 'pubFetchEndPoint') {
                utilities.pubFetchEndPoint = obj[o];

            } else if (o == 'campaignFetchEndPoint') {
                utilities.campaignFetchEndPoint = obj[o];

            }
        }
        $(document).ready(function() {
            profiles.init();                
        });
    },
    customTax: function(tax) {
        taxonomy.init(tax);
    },
    postTaxonomy: function(tax) {
        taxonomy.post_taxonomy = tax;
    },
    emailBuilder: function() {
        $(document).ready(function() {
            emailBuilder.init();
        });
    },
    premiumSets: function() {
        $(document).ready(function() {
            premiumSets.init();
        });
    },
    tempOrder: function() {
        tempOrderUi.construct();
    },
    genericEmail: function() {
        genericEmailUi.construct();
    },
    webViews: function() {
        webViews.construct();
    }
};

};

window.ProfilesAdmin = ProfilesAdmin();

})(window, document, jQuery);
