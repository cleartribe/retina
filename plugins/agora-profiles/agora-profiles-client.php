<?php

/**
 * A class for handling communication with the Profiles API
 *
 * @package agora-profiles-client
 * 
 * @author Nate Martin
 * @version 1.0
 */
class Agora_Profiles_Client {

    public $wpdb;

    /**
     * The URL where profiles are kept
     *
     * @var string The API URL
     * @since 1.0
     */
    public $apiurl;

    /**
     * oAuth 2.0 client ID assigned from the Profiles service
     *
     * @var string oAuth 2.0 client ID
     * @since 1.0
     */
    private $_client_id = '';

    /**
     * oAuth 2.0 client secret assigned from the Profiles service
     *
     * @var string oAuth 2.0 client Secret
     * @since 1.0
     */
    private $_client_secret = '';

    /*
        endpoint for making ajax requests
    */
    public $profiles_plugin_request = '';

    /*
        default wordpress taxonomies
    */
    public $wp_default_taxonomies = array(
        'category',
        'post_tag',
        'topics',
    );

    /*
        for registering and referencing custom taxonomy names
    */
    public $custom_taxonomies = array(
        'psychology',
        'objective',
        'sector',
    );

    /**
     * Sets up the environment for the plugin
     *
     * @since 1.0
     * 
     * @return null
     */
    public function __construct() {

        // WP might not be set, as this class can be used outside of WP
        global $wpdb;
        
        if (isset($wpdb)) {
            $this->wpdb = $wpdb;            
        }

        /*
            this is one fugly hack, because
            Class can be used outside of WP and depending one where the file is included
            - like in pubs/get_publications.php - we need an additional ../ to find wp-config.php
            But it all works, so please ignore it's fugliness
        */
        if(! defined('ABSPATH')) {

            $wp_config = '../../../wp-config.php';

            if (is_file($wp_config)) {
                require_once($wp_config);
            } else {

                // try again for next sub folders
                $wp_config = '../../../../wp-config.php';
                if (is_file($wp_config))
                    require_once($wp_config);
                else
                    exit('wp-config.php is not found');
            }
        }

        $this->profiles_plugin_request = $this->get_profiles_endpoint_url();

        // set profiles endpoint
        $this->apiurl = defined('AF_PROFILES_API_URL') ? AF_PROFILES_API_URL : exit('set the AF_PROFILES_API_URL constant');
    }

    public function get_profiles_endpoint_url() {

        return defined('AGORAPROFILESURL')
            ? AGORAPROFILESURL . '/request.php'
            : '';
    }

    public function get_publications_endpoint_url() {
        
        return defined('AGORAPROFILESURL')
            ? AGORAPROFILESURL . '/pubs/manual_fetch.php'
            : '';
    }

    /**
     * Send data to profiles using cURL
     *
     * @since 1.0
     * @author Nate Martin
     * 
     * @param array data
     * @param string route
     * @return null
     */
    public function profiles_request($data, $route, $use_json_data = true)
    {
        if ($use_json_data) {
            $data = 'data=' . urlencode(json_encode($data));
        }

        /*
            override profiles enpoint and use something else
            delimited with '-', url is second item, Example
            endpoint-http://example.com/endpoint.php

        */
        if (preg_match('/endpoint/', $route)) {
            $endpoint = explode('-', $route);
            $endpoint = $endpoint[1];

        } else {
            $endpoint = $this->get_api_endpoint() . trim($route, '/');
        }

        $content_length = is_array($data) ? implode('', $data) : $data;
        $access_token = defined('AF_PROFILES_API_TOKEN') ? AF_PROFILES_API_TOKEN : '';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array(
                'Content-Length' . strlen($content_length),
                'Token: ' . $access_token
            )
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    private function get_api_endpoint() {

        if ($this->apiurl === NULL) {

            $this->apiurl = (
                    isset($_SERVER['HTTP_HOST'])
                    && $_SERVER['HTTP_HOST'] !== 'lfb.org'
                    && isset($_SERVER['SERVER_ADDR'])
                    && $_SERVER['SERVER_ADDR'] === '127.0.0.1'
                )
                ? 'profiles.local/'
                : 'profiles.agorafinancial.com/';
        }

        return $this->apiurl;
    }

    /*
        start or stop a specific cron jobs

        @param string start | stop
        @param string cron command, including timing
        @param string linux command
        @return null
    */
    public function cron_job($action, $cron_command ) {

        $cronjobs = new Agora_profiles_cronjobs();

        if ($action === 'start') {
            $cronjobs->start_cronjob($cron_command);
        } else if ($action === 'stop') {
            $cronjobs->delete_cronjob($cron_command);
        }
    }

    /*
        probably not a good hack.
        this class can be loaded outside of WP and the constants won't work.

        @param
        @return
    */
    public function get_profiles_request_url() {

        $server = $_SERVER['HTTP_HOST'] . '/';
        $content = preg_match('/dailyreckoning/', $server) ? 'dr-content/' : 'wp-content/';
        $path = 'http://' . $server . $content . 'plugins/agora-profiles';

        return defined('AGORAPROFILESURL')
            ? AGORAPROFILESURL
            : $path;
    }

    /*
        probably not a good hack.
        this class can be loaded outside of WP and the constants won't work.

        @param
        @return
    */
    public function get_profiles_plugin_path() {

        return defined('AGORAPROFILESDIR')
            ? AGORAPROFILESDIR
            : dirname(__FILE__) . '/';
    }

    /*
        set an openx variable, 1 key value pair at time

        @param array key value pair
        @return null
    */
    public function set_openx_variable($variable) {
        Agora_profiles_openx::openx_variables($variable);
    }

    /**
     * get publications from the pubs/ directory
     * @return array $pubs
     */
    public function get_cached_publications() {

        $path = AGORAPROFILESDIR . 'pubs/pubs.txt';

        if (! is_file($path))
            return array();

        $pubs = file_get_contents($path);
        $pubs = json_decode($pubs, true);

        if ($pubs === NULL || ! isset($pubs['pubs'])) {
            return array();
        } else {

            $pubs = $pubs['pubs'];

            // sort alphabetically
            $pubcodes = array();

            for ($i = 0; $i < count($pubs); $i++) {

                $pubcodes[$pubs[$i]['pubcode']] = $pubs[$i];
            }

            ksort($pubcodes);

            return array_values($pubcodes);
        }
    }
}