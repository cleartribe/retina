<?php

class AgoraProfilesRewrites {
    
    public function __construct()
    {
        $this->hooks();
    }

    private function hooks()
    {
        add_action('generate_rewrite_rules', function($rules) {
            return $this->rewriteRules($rules);
        });

        add_filter('query_vars', array($this, 'setQueryVars'));

        add_action('template_redirect', array($this, 'rewriteTemplates'));
    }

    public function setQueryVars($vars)
    {
        $vars[] = 'request-endpoint';
        return $vars;
    }

    public function rewriteTemplates()
    {
        if (get_query_var('request-endpoint')) {
            add_filter('template_include', function() {
                include(AGORAPROFILESDIR . 'AgoraProfilesRequestEndpoints.php');
                new AgoraProfilesRequestEndpoints();
                exit;
            });
        }
    }

    private function rewriteRules($rules)
    {
        global $wp_rewrite;

        $new_rules = array(
            'request/endpoint/?$' => 'index.php?request-endpoint=true',
        );

        $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;

        return $wp_rewrite->rules;
    }
}