<?php

if ( ! class_exists( 'Agora_Profiles_Client' ) )
	require_once( 'agora-profiles-client.php' );

class Agora_profiles_campaigns extends Agora_Profiles_Client  {

	private $command = '';
	private $execute = '';
	private $campaigns_folder = '';
	private $campaign_option_name = 'profiles_campaign_client';
	private $target;

	public function __construct() {

		$this->command = 'wget -qO- ' . $this->get_profiles_request_url() . '/campaigns/get_campaigns.php &> /dev/null';
		$this->execute = '0,15,30,45 * * * * ' . $this->command;
		$this->campaigns_folder = $this->get_profiles_plugin_path() . 'campaigns/';
	}

	public function start_cronjob_campaigns() {

		$this->cron_job( 'start', $this->execute );
	}

	public function get_campaigns_from_profiles() {

		$campaigns = $this->profiles_request( '', 'campaigns/campaigndata', FALSE );

		// check if campaign data was returned in json format, if not abort
		$json_check = json_decode( $campaigns, TRUE );

		if ( $json_check !== NULL && isset( $json_check[0]['client'] ) ){
			// for use with PHP
			file_put_contents( $this->campaigns_folder . 'campaigns.txt', $campaigns );

			// create javascript file
			$js_campaigns = 'var agora_financial_campaigns = ' . $campaigns . ';';
			$result = file_put_contents( $this->campaigns_folder . 'campaigns.js', $js_campaigns );
			
			return ( $result > 0 ) ? TRUE : FALSE;
		}

		return FALSE;
	}

	/*
		a user/readers campaign target id are stored in the cookie afcampaigns
		the cookie afcampaigns is set with an ajax request in profiles.js
		the cookie value has leading and trailing "-"
		example: -1-2-3-
		this is for easy regex matching

		@param
		@return
	*/
	public function run_campaigns() {

		$this->target = isset( $_COOKIE['afcampaigns'] ) ? htmlspecialchars( $_COOKIE['afcampaigns'] ) : FALSE;

		if ( ! $this->target )
			return FALSE;

		$campaigns = $this->get_campaigns();

		if ( ! $campaigns )
			return FALSE;

		$campaigns = json_decode( $campaigns, TRUE );

		if ( $campaigns === NULL )
			return FALSE;

		$this->deploy_campaigns( $campaigns, $this->target );
	}

	private function get_campaigns() {

		$campaign_file = $this->campaigns_folder . 'campaigns.txt';

		if ( ! is_file( $campaign_file ) )
			return FALSE;

		return file_get_contents( $campaign_file );
	}

	/*
		iterate over campaign targets and find campaign
		action to take is based on campaign "client"

		@param array campaign data
		@param array user data
		@return null
	*/
	private function deploy_campaigns( $campaigns, $target ) {

		$campaign_ids = explode( '-', trim( $target, '-' ) );

		if ( count( $campaign_ids ) < 1 )
			return FALSE;

		for ( $i = 0; $i < count( $campaign_ids ); $i++ ) {
			
			for ( $j = 0; $j < count( $campaigns ); $j++ ) {
				
				if ( (int) $campaign_ids[$i] === (int) $campaigns[$j]['id'] )
					$this->deploy_campaign_client( $campaigns[$j] );
			}
		}
	}

	/*
		"Campaign Client" is the action to take, or the end result
		Ie, client is openx and we're targeting an Ad
		or client is promo_headline and we're changing headline copy

		there can be multiple clients per campaign

		@param array campaign data
		@return return
	*/
	private function deploy_campaign_client( $campaign ) {
		
		if ( ! isset( $campaign['client'] ) )
			return FALSE;

		// multiple clients per campaign
		for ( $i = 0; $i < count( $campaign['client'] ); $i++ ) {
					
			switch ( $campaign['client'][$i] ) {

				case 'openx':
					$this->deploy_openx();
					break;
			}
		}
	}

	private function deploy_openx() {

		$target = $this->target;

		$this->set_openx_variable( array( 'target' => $target ) );
	}
}