### Emails

Send emails through a profiles endpoint. This is specifically used to send emails through Message Central API.

#### How To Send A Message Central Email

**Message Central** [http://mc2.agora-inc.com/auth/login/validate](http://mc2.agora-inc.com/auth/login/validate)

**Important:** be aware you can not edit emails after they are sent. If you make a mistake, you must start over.

**#1** Log into the MC admin and select a the `API_CUSTOMERSERVICE` list. After you select the list, you should be redirected to the `Content` tab. If not, select the `Content` tab.

**#2** Click `New Content` (this will be your email), name the content, select `Blank Content` and click `Create`.

**#3** Add your email subject and source code (example source code is below), and click save.

**#4** Click the tab `Mailings`.

**#5** Click `New Mailing`, name your mailing, leave `Campaign` defined as No Campaign, and set `Type` as `API-Triggered Mailing` and click Create.

**#6** Click the Sub-Tab `Content` to add content to this specific mailing. In the popup window, select your `Content` you created in step #3.

**#7** Now Click the Sub-Tab `Overview` and click `Ready` to set your email mailing to active status.

**#8** From the `Mailings` page, get the mailing id from URL parameter `?mailingid=12345`. This integer is required for sending emails from your application.

#### Code Example

The class `Agora_profiles_emails` is included in the **Agora Profiles** plugin.  You should be able to use this class anywhere in your theme.

**Requirements:** 

`mc_id`: explained in step #8, which is the URL parameter found on the mailings page in the MC admin.

`email`: an email

**Options:**

You can pass any other variables you want to an email, by adding them to the same array as the `email` and `mc_id` variables.

```php

/**
 * Code used to send the AF password recovery email.
 * Found in the method Agora_Financial_Theme->ajax_lost_password()
 */

$email_data = array(
	// mailing id, from mc.com?mailingid=12345
	'mc_id' => 12345,
	'email' => $email,
	// any other variable you want to pass to the email
	'username' => $username,
	'password' => $password,
	'contactus_url' => site_url( '/contact-us' ),
	'customer_service_phone' => $this->customer_service_phone,
);

$profiles_emails = new Agora_profiles_emails();
$profiles_emails->send( $email_data );
```

#### Email HTML Source

In order to pass variables into an email, you must use the function tag `$T->te_encode()`.

##### [% $rv .= $T->te_encode($JSON->{variable_name})||'Default Value'; %]

As you can see from the code snippet above, we're passing the email recipient's password into the `$profiles_emails->send( $email_data )` with variable `$email_data['password']`. 

In the code below, we are printing the password using the tag `[% $rv .= $T->te_encode($JSON->{password})||'Password could not be found'; %]`. The function includes an or statement `||` which prints out default text, in-case the variable is not found or something goes wrong. In the example, the default text is "Password could not be found".

Below is an example of the email we are sending from the above code.

**The $JSON->{variable_name} must exactly match the variable name that was passed into $profiles_emails->send( $email_data )**

```text
You have requested your lost username and password. <br>
 <br>
Your username is: [% $rv .= $T->te_encode($JSON->{username})||'Username could not be found'; %] <br>
Your password is: [% $rv .= $T->te_encode($JSON->{password})||'Password could not be found'; %] <br>
 <br>
If you have any further questions don't hesitate to contact us at: [% $rv .= $T->te_encode($JSON->{contactus_url})||'agorafinancial.com'; %] <br>
 <br>
or... <br>
 <br>
Phone #: [% $rv .= $T->te_encode($JSON->{customer_service_phone})||'866-361-7662'; %] <br>
International Phone #: 410-454-0499 <br>
Fax Line: 410-864-1650 <br>
E-mail: customerservice@agorafinancial.com <br>
Mailing Address: 808 St. Paul St. Baltimore, MD, 21202 <br>
 <br>
The Agora Financial Customer Service Department is open Monday through Friday, 9 a.m.-5 p.m. EST. Please feel free to contact us, should you have any questions or concerns regarding your account, billing, navigating our website, product information or any related topic. <br>
 <br>
Sincerely, <br>
Agora Financial 
```