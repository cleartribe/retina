<?php namespace plugins\agoraprofiles;

class Cache
{
    private $cache;

    public function __construct()
    {
        if (class_exists('\Memcache')) {
            $this->cache = new \Memcache();
            $this->cache->pconnect('localhost', 11211);            

        } else {

            if (!defined('MEMCACHE_COMPRESSED'))
                define('MEMCACHE_COMPRESSED', false);
            
            $this->cache = new NoCache();
        }
    }

    /**
     * @param string  $key
     * @param string  $value
     * @param integer $expire
     * @param string  $flag
     * @return boolean
     */
    public function set($key, $value, $expire = 300, $flag = '')
    {
        return $this->cache->set($key, $value, MEMCACHE_COMPRESSED, $expire);
    }

    /**
     * @param string  $key
     * @param string  $flag
     * @return string|false
     */
    public function get($key, $flag = '')
    {
        return $this->cache->get($key);
    }

    /**
     * @param string  $key
     * @return false
     */
    public function delete($key)
    {
        return $this->cache->delete($key);
    }

    /**
     * @param string  $key
     * @return false
     */
    public function flush()
    {
        return $this->cache->flush();
    }
}