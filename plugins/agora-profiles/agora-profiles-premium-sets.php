<?php

class Agora_profiles_premium_sets extends Agora_Profiles_Client {

    private $pubs = array();

    private $sites = array(
        array(
            'pubcode' => 'AF_site',
            'org' => 'AF'
        ),
        array(
            'pubcode' => 'DR_site',
            'org' => 'DR'
        ),
        array(
            'pubcode' => 'LFB_site',
            'org' => 'LFB'
        ),
    );

    public function __construct()
    {
        parent::__construct();
        $this->pubs = $this->get_cached_publications();
        $this->hooks();
    }

    public function hooks()
    {
        add_action('admin_menu', array($this, 'adminMenus'));
        add_action('admin_footer', array($this, 'hook_admin_footer'));
        add_action('wp_ajax_nopriv_getPremiumReports', array($this, 'getPremiumReports'));
        add_action('wp_ajax_getPremiumReports', array($this, 'getPremiumReports'));
        add_action('wp_ajax_nopriv_getReportsById', array($this, 'getReportsById'));
        add_action('wp_ajax_getReportsById', array($this, 'getReportsById'));
    }

    public function adminMenus()
    {
        add_menu_page('Premium Sets', 'Premium Sets', 'manage_options', 'af-premium-sets', array($this, 'premiumSetHtml'), 'dashicons-images-alt', '10.6');

        wp_enqueue_script('afp-profiles-admin', AGORAPROFILESURL . '/js/profiles-admin.js', array('jquery'));

        // this will mess up other styles, so only include on email admin page
        if (isset($_GET['page']) && $_GET['page'] == 'af-premium-sets') {
            wp_enqueue_style('foundation-css', AGORAPROFILESURL . '/css/foundation.min.css');
            wp_enqueue_style('profiles-admin', AGORAPROFILESURL . '/css/profiles-admin.css', array('foundation-css'));          
            wp_enqueue_style('profiles-admin-email', AGORAPROFILESURL . '/css/profiles-admin-customer-service.css', array('profiles-admin'));
            wp_enqueue_style('profiles-admin-premium-sets', AGORAPROFILESURL . '/css/profiles-admin-premium-sets.css', array('profiles-admin-email'));          
        }
    }

    public function hook_admin_footer() {

        if ( isset( $_GET['page'] ) && $_GET['page'] == 'af-premium-sets' ) {
            $this->profiles_admin_email_inline_script();            
        }
    }

    /**
     * print inline JS in the admin head for page=af-profiles-emails
     */
    private function profiles_admin_email_inline_script() {
        $script = '<script>';
        $script .=  'var agora_publications = ' . json_encode( $this->pubs ) . '; ';
        $script .=  'ProfilesAdmin.premiumSets();';
        $script .= '</script>';
        echo $script;
    }

    public function premiumSetHtml()
    {
        $pub_code = isset($_COOKIE['afpEmailPub']) ? $_COOKIE['afpEmailPub'] : '';

        ?>

            <div class="row">
                <div class="small-12 columns">
                    <div id="email-admin-header" class="row">
                        <div class="small-2 columns">
                            <h4>Premium Sets</h4>
                        </div>
                        <div class="small-2 columns">
                            <?php echo $this->getPublicationSelector(); ?>
                        </div>
                        <div class="small-3 columns">
                            <button id="add_premium_set" class="small">Create Premium Set</button>
                        </div>
                        <div class="small-5 columns">
                            <div style="visibility:hidden;">*</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top:10px;">
                <div id="premium_set_wrap" class="small-12 columns">

                    <div class="row premium_set_row">
                        <div class="small-3 columns">
                            <input type="text" class="premium_choice_code" placeholder="Choice Code">
                            <p class="premium_choice_missing premium_error">Please enter a choice code</p>
                            <p class="premium_choice_syntax premium_error">Do not use spaces, use underscores "_"</p>
                            <p class="premium_choice_existing premium_error">This choice code name already exists for this pubcode. Choose a different name.</p>
                            <textarea class="premium_description" placeholder="Description"></textarea>
                            <p class="premium_description_error premium_error">Enter a description</p>
                            <button class="tiny secondary delete_premium_set">Delete Premium Set</button>
                        </div>
                        <div class="small-9 columns">

                            <div class="row">
                                <div class="small-12 columns premiums_collection">
                                    <div class="row premium_input_row">
                                        <div class="small-5 columns">
                                            <?php echo $this->getReportsSelector($pub_code); ?>
                                        </div>
                                        <div class="small-5 columns">
                                            <input type="text" class="premium_report_link" placeholder="Report Link" disabled >
                                        </div>
                                        <div class="small-2 columns">
                                            <button class="tiny secondary delete_premium">Delete</button>
                                        </div>
                                    </div>                                
                                </div>
                            </div>

                            <div class="row">
                                <div class="small-6 columns">
                                    <button class="tiny secondary add_premium">Add Premium</button>
                                </div>
                                <div class="small-6 columns">
                                    <button class="small save_premium_set">Save Premium Set</button>
                                    <button class="tiny secondary copy_premium_set">Copy</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        <?php
    }

    /**
     * get the publications and create a drop down menu
     * @return string $html
     */
    private function getPublicationSelector()
    {
        $this->pubs = array_values(array_merge($this->sites, $this->pubs));

        $html = '';
        $html .= '<select name="premium_pub" class="email_selector">';

        for ($i = 0; $i < count($this->pubs); $i++) {
            $html .= '<option value="' . $this->pubs[$i]['pubcode'] . '" >' . $this->pubs[$i]['pubcode'] . '</option>';
        }

        $html .= '</select>';

        return $html;
    }

    /**
     * For Ajax request
     * @param  string $pub_code
     * @return string $html
     */
    public function getPremiumReports()
    {
        if (!isset($_POST['pub_code']))
            wp_die();

        echo $this->getReportsSelector($_POST['pub_code']);
        wp_die();
    }

    /**
     * For Ajax request
     * @param  string $pub_code
     * @return string $html
     */
    public function getReportsById()
    {
        if (!isset($_POST['report_ids']))
            wp_die();

        $ids = json_decode(stripslashes($_POST['report_ids']));

        if ($ids === null)
            wp_die();

        // probably duplicate ids
        $ids = array_values(array_unique($ids));

        wp_send_json($this->querySelectReportsById($ids));        
        wp_die();
    }

    /**
     * @param  string $pub_code
     * @return string $html
     */
    private function getReportsSelector($pub_code)
    {
        $reports = $this->getPublicationReports($pub_code);

        if (count($reports) === 0)
            return '';

        $html = '';
        $html .= '<select name="premium_report" class="email_selector">';

        for ($i = 0; $i < count($reports); $i++) {
            $option = '<option value="' . $reports[$i]->post_title . '" ';
                $option .= 'data-report-guid="' . $reports[$i]->guid . '" ';
                $option .= 'data-post-id="' . $reports[$i]->post_id . '" >';
                $option .= $reports[$i]->post_title;
            $option .= '</option>';

            $html .= $option;
        }

        $html .= '</select>';

        return $html;
    }

    /**
     * @param  string $pub_code
     * @return array
     */
    private function getPublicationReports($pub_code)
    {
        global $wpdb;

        // First: get general publication reports
        $sql = 'SELECT p.ID, p.post_title ';
        $sql .= 'FROM wp_posts AS p ';
        $sql .= 'INNER JOIN wp_postmeta AS pm ON (p.ID = pm.post_id) ';
        $sql .= 'INNER JOIN wp_term_relationships AS tr ON (tr.object_id = p.ID) ';
        $sql .= 'INNER JOIN wp_term_taxonomy AS tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id) ';
        $sql .= 'INNER JOIN wp_terms AS t ON (t.term_id = tt.term_id) ';
        $sql .= 'WHERE pm.meta_key = "_pubcode" ';
        $sql .= 'AND pm.meta_value = %s ';
        $sql .= 'AND p.post_type = "post" ';
        $sql .= 'AND p.post_status = "publish" ';
        $sql .= 'AND tt.taxonomy = "category" ';
        $sql .= 'AND t.name = "report" ';

        $query = $wpdb->get_results(
            $wpdb->prepare($sql, $pub_code)
        ); 

        $report_ids = array();

        for ($i = 0; $i < count($query); $i++) {
            $report_ids[$i] = (int) $query[$i]->ID;
        }

        $report_ids = array_values(array_unique($report_ids));

        return $this->querySelectReportsById($report_ids);
    }

    private function querySelectReportsById($ids)
    {
        global $wpdb;

        if (count($ids) === 0)
            return array();

        $placeholders = $this->getWhereInString($ids);

        // Second: Get reports that have PDF attachements
        $sql = 'SELECT pm1.post_id, p.ID AS report_id, p2.post_title, p.guid ';
        $sql .= 'FROM wp_posts AS p ';
        $sql .= 'INNER JOIN wp_postmeta AS pm1 ON (pm1.meta_value = p.ID) ';
        $sql .= 'INNER JOIN wp_posts AS p2 ON (pm1.post_id = p2.ID) ';
        $sql .= 'WHERE pm1.post_id IN(' . $placeholders . ') ';
        $sql .= 'AND pm1.meta_key = "_post_pdf_id" ';

        $reports = $wpdb->get_results(
            $wpdb->prepare($sql, $ids)
        );

        return $reports;
    }

    /**
     * creat a "Where In" placeholders
     * (%d, %d, %d)
     * @param  array $items
     * @return string
     */
    private function getWhereInString($items)
    {
        if (count($items) === 0)
            return '';

        $placeholders = '';

        for ($i = 0; $i < count($items); $i++) {
            $placeholders .= '%s,';
        }

        return trim($placeholders, ',');
    }
}
