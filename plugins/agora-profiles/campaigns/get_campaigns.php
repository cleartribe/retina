<?php

require_once( '../agora-profiles-campaigns.php' );

$afp_campaigns = new Agora_profiles_campaigns();
$result = $afp_campaigns->get_campaigns_from_profiles();

header('Content-Type: application/json');

echo json_encode( array( $result ) );