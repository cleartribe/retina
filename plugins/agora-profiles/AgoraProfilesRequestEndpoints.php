<?php

class AgoraProfilesRequestEndpoints {

    public function __construct()
    {
        $this->fireRoute();
    }

    private function fireRoute()
    {
        if (!ipWhiteList())
            exit;

        if (!isset($_POST['route']))
            exit;
        
        if (method_exists($this, $_POST['route']))
            call_user_func(array($this, $_POST['route']));

    }

    private function logOutUser()
    {
        if (!isset($_POST['username']))
            exit;

        $username = $_POST['username'];

        $user = get_user_by('login', $username);

        if (!isset($user->ID))
            exit;

        update_user_meta($user->ID, 'logout_user', 1);
    }
}