<?php

class AgoraProfilesAjaxRequests {
    
    public function __construct()
    {
        $this->ajaxHooks();
    }

    private function ajaxHooks()
    {
        add_action('wp_ajax_logoutCustomer', function() {
            $this->logoutCustomer();
        });        
    }

    private function logoutCustomer()
    {
        $username = isset($_POST['user_login']) ? $_POST['user_login'] : '';

        if ($username === '') {
            echo 'username is not set';
            exit;
        }

        $user = get_user_by('login', $username);

        if (!isset($user->ID)) {
            echo 'could not find user record';
            exit;
        }

        update_user_meta($user->ID, 'logout_user', 1);

        $this->logoutLfbCustomer($username);

        echo 'logged out user';
        exit;
    }

    private function logoutLfbCustomer($username)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://lfb.org/request/endpoint');
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('route' => 'logOutUser', 'logout_user' => $username));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}