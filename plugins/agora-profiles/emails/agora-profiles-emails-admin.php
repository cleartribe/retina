<?php

class Agora_profiles_emails_admin  {

    private $emails;
    private $profiles_client;
    private $pubs;

    private $sites = array(
        array(
            'pubcode' => 'AF_site',
            'org' => 'AF'
        ),
        array(
            'pubcode' => 'DR_site',
            'org' => 'DR'
        ),
        array(
            'pubcode' => 'LFB_site',
            'org' => 'LFB'
        ),
    );

    private $email_types = array(
        'order_confirmation' => 'Order Confirmation',
        'cs_team_alert' => 'CS Team Alert',
        'generic_emails' => 'Generic Emails',
    );

    public function __construct(Agora_profiles_emails $emails, Agora_Profiles_Client $client)
    {
        $this->emails = $emails;
        $this->profiles_client = $client;

        $this->pubs = $this->profiles_client->get_cached_publications();
        $this->hooks();
    }

    private function hooks()
    {
        add_action('admin_menu', array($this, 'hook_admin_menu'));
        add_action('admin_footer', array($this, 'hook_admin_footer'));
    }

    public function hook_admin_menu() {

        add_menu_page('Emails', 'Emails', 'manage_options', 'af-profiles-emails', array($this, 'emails_admin_html'), 'dashicons-email-alt', '10.5');

        wp_enqueue_script('afp-profiles-admin', AGORAPROFILESURL . '/js/profiles-admin.js', array('jquery'));

        // this will mess up other styles, so only include on email admin page
        if (isset($_GET['page']) && $_GET['page'] == 'af-profiles-emails') {
            wp_enqueue_style('foundation-css', AGORAPROFILESURL . '/css/foundation.min.css');
            wp_enqueue_style('profiles-admin-email', AGORAPROFILESURL . '/css/profiles-admin-customer-service.css', array('foundation-css'));
        }
    }

    public function hook_admin_footer() {

        if (isset($_GET['page']) && $_GET['page'] == 'af-profiles-emails') {
            $this->profiles_admin_email_inline_script();            
        }
    }

    /**
     * print inline JS in the admin head for page=af-profiles-emails
     */
    private function profiles_admin_email_inline_script() {
        $script = '<script>';
        $script .=  'var agora_publications = ' . json_encode($this->pubs) . '; ';
        $script .=  'ProfilesAdmin.emailBuilder();';
        $script .= '</script>';
        echo $script;
    }

    /**
     * general emails interface
     */
    public function emails_admin_html() {
        
        ?>

        <div class="row">
            <div class="small-12 columns">

                <!-- header / nav bar -->
                <div id="email-admin-header" class="row">
                    <div class="small-1 columns">
                        <h4>Emails</h4>
                    </div>
                    <div class="small-3 columns">
                        <?php echo $this->get_email_type_selector(); ?>
                    </div>
                    <div class="small-2 columns">
                        <?php echo $this->get_publication_selector(); ?>
                    </div>
                    <div class="small-2 columns">
                        <button id="email_add_new" class="small">Create Email</button>
                    </div>
                    <div class="small-2 columns">
                        <button id="email_variables" class="small">Variables</button>
                    </div>
                    <div class="small-2 columns">
                        <button id="email_help" class="small secondary">Help</button>
                    </div>
                </div>

                <!-- Display Email Variables -->
                <div id="email-variables-display">
                    <div id="email-variables-header" class="row table_header">
                        <div class="small-12 columns">
                            <div class="row">
                                <div class="small-3 columns"><strong>Variable Name</strong></div>
                                <div class="small-3 columns"><strong>Variable Value</strong></div>
                                <div class="small-6 columns">
                                    <div class="row">
                                        <div class="small-3 columns"><button id="add_variable" class="tiny">Add Variable</button></div>
                                        <div class="small-9 columns"><button id="close_variable" class="small">X</button></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="email-variables-wrap" class="row">
                        <div id="email-vars-table" class="small-12 columns table-rows-wrapper email-variables">
                            <div class="row email-variable-row">
                                <div class="small-3 columns email_variable_name">
                                    <input type="text" class="email_variable_name">
                                    <p class="email_var_name_error">This Variable name is taken, choose a different name</p>
                                    <p class="email_var_format_error">Do not use spaces in your name, use underscores "_"</p>
                                </div>
                                <div class="small-8 columns email_variable_value">
                                    <input type="text" class="email_variable_value" disabled="true">
                                </div>
                                <div class="small-1 columns email_variable_value">
                                    <button class="tiny secondary delete_variable">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Display Email Variables -->

                <!-- Display Email Table -->
                <div id="email-displayer-header" class="row table_header">
                    <div class="small-12 columns">
                        <div class="row">
                            <div class="small-3 columns"><strong>Email Name</strong> <button class="tiny toggle_order" data-column="name" data-order="decending">&#94;</button></div>
                            <div class="small-3 columns"><strong>Subject</strong> <button class="tiny toggle_order" data-column="subject" data-order="decending">&#94;</button></div>
                            <div class="small-6 columns"><strong>Created</strong> <button class="tiny toggle_order" data-column="created" data-order="decending">&#94;</button></div>
                        </div>
                    </div>
                </div>

                <div id="email-displayer-wrap" class="row">
                    <div id="email-displayer" class="small-12 columns table-rows-wrapper">
                        <div class="row">
                            <div class="small-3 columns email_display_name"></div>
                            <div class="small-3 columns email_display_subject"></div>
                            <div class="small-2 columns email_display_created"></div>
                            <div class="small-4 columns email_display_buttons"></div>                                                   
                        </div>
                    </div>
                </div>
                <!-- END Display Email Table -->
                
                <!-- Email Documentation -->
                <div id="email-documentation" class="row">
                    <div class="small-12 columns">
                        <div class="row">
                            <div class="small-4 columns"><h3>Help</h3></div>
                            <div class="small-8 columns"><button id="emailBacktotop" class="tiny">Back to Top</button></div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <p><strong>Important:</strong> in order to use the shortcodes below, you must include the order receipt text output in the shortcode tag</p>
                                <p><strong>Example:</strong></p>
                                <textarea>
<p>
    Dear Customer, <br>
    Your order details are <br>
    [username]Username: {{value}}[/username] <br>
    [password]Password: {{value}}[/password] <br>
    [ordertotal currency="$"]Order Total: {{value}}[/ordertotal] <br>
</p>
                                </textarea>

                                <p>The above code will output;</p>
                                <blockquote>
                                    <p>
                                        Dear Customer, <br>
                                        Your order details are <br>
                                        Username: afcustomer <br>
                                        Password: secret <br>
                                        Order Total: $1,000 <br>
                                    </p>
                                </blockquote>

                                <hr>

                                <p><strong>Important:</strong> if a tag does not include content (text between opening and closing tag), you must use a forward slash in the opening tag;<br><strong>[nameFirst /]</strong></p>
                            </div>
                        </div>
                        <hr>
                        <strong>Variable Tag</strong>
                        <p style="margin: 5px;font-size:14px;">Add the name of the variable you created above. In this case, the variable name is foobar. This will return the value you asigned to the variable name.</p>
                        <div class="row">
                            <div class="small-12 columns">[variable name="foobar" pubcode="ost" /]</div>
                        </div>

                        <hr>
                        <strong>Conditional Content Tag</strong>
                        <p style="margin: 5px;font-size:14px;">Show content for a specific choice code and Pub. <strong>Both "code" and "pub"</strong> attributes are required.</p>
                        <p style="margin: 5px;font-size:14px;">
                            <strong>IMPORTANT:</strong> The order form confirmation page must have an order description containing the "termcode" in this exact format;
                            <br/>
                            <strong>termcode:79AR-LIR</strong> (choice code and then pubcode separated with a dash and no spaces)
                        </p>
                        <div class="row">
                            <div class="small-12 columns">
                                [choice_code code="79AR" pub="LIR"]
                                    &lt;a href=&quot;#&quot;&gt;Special Report&lt;/a&gt;
                                [/choice_code]
                            </div>
                        </div>

                        <hr>
                        <strong>Premium Set Tag</strong>
                        <p style="margin: 5px;font-size:14px;">Show a Premium Set's reports for a specific choice code and Pub. <strong>Both "code" and "pub"</strong> attributes are required.</p>
                        <p style="margin: 5px;font-size:14px;">
                            <strong>IMPORTANT:</strong> The order form confirmation page must have an order description containing the "termcode" in this exact format;
                            <br/>
                            <strong>termcode:79AR-LIR</strong> (choice code and then pubcode separated with a dash and no spaces)
                        </p>
                        <p style="margin: 5px;font-size:14px;">
                            <strong>**</strong> The short code will print out the entire table, including <strong>&lt;table&gt;</strong> tags. <br>
                            <strong>**</strong> To over ride the choice code sent with the conversion pixel, add the attribute <em>choice_code</em>, ie choice_code="79AR"
                        </p>
                        <div class="row">
                            <div class="small-12 columns">
                                [premium_set pub_code="OST"]
                            </div>
                        </div>

                        <hr>

                        <div id="shortcodes_order_confirmation" class="view_shortcodes">
                            <div class="row">
                                <div class="small-12 columns">[address1]Address1: {{value}}[/address1]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[address2]Address2: {{value}}[/address2]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[ccnumber]Credit Card Number: {{value}}[/ccnumber]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[cctype]Credit Card Type: {{value}}[/cctype]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[city]City: {{value}}[/city]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[country]Country: {{value}}[/country]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[email]Email: {{value}}[/email]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[itemprice currency="$"]Price: {{value}}[/itemprice]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[itemname]Item Name: {{value}}[/itemname]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[itemdescription]Item Description: {{value}}[/itemdescription]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[ordertotal currency="$"]Order Total: {{value}}[/ordertotal]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[orderquantity]Order Quantity: {{value}}[/orderquantity]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[nameFirst]First Name: {{value}}[/nameFirst]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[nameLast]Last Name: {{value}}[/nameLast]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[ordernumber]Order Number: {{value}}[/ordernumber]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[phone]Phone Number: {{value}}[/phone]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[promocode]Promo Code: {{value}}[/promocode]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[password]Password: {{value}}[/password]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[username]Username: {{value}}[/username]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[state]State: {{value}}[/state]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[taxlocation]Tax Location: {{value}}[/taxlocation]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[taxprice currency="$"]Tax Price: {{value}}[/taxprice]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[zipcode]Zipcode: {{value}}[/zipcode]</div>
                            </div>
                        </div>

                        <div id="shortcodes_cs_team_alert" class="view_shortcodes">
                            <div class="row">
                                <div class="small-12 columns">[advantage_id]Advantage ID: {{value}}[/advantage_id]</div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">[pub_code]Pub Code: {{value}}[/pub_code]</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Email Documentation -->

                <!-- email editor -->
                <div class="reveal-modal-bg"></div>
                <div id="email-admin-editor" class="reveal-modal xlarge">
                    <a id="close_email_editor" class="close-reveal-modal">×</a>
                    <div id="email-editor-interface">
                        <div class="row">
                            <div class="small-12 columns">
                                <div class="small-4 columns">
                                    <input id="email_name" type="text" placeholder="Email Name" />
                                    <p id="editor_error_name" class="email_editor_error">Enter an email Name</p>
                                    <p id="editor_error_name_format" class="email_editor_error">Email Name cannot have spaces, use underscores. <strong>Example: ost_newwar_0115</strong></p>
                                    <p id="editor_error_name_taken" class="email_editor_error">This email name is taken. Choose a unique name</p>
                                    <p id="editor_error_name_edit" class="email_editor_error">You can not edit the name of this email</p>
                                </div>
                                <div class="small-4 columns">
                                    <?php echo $this->get_email_type_selector(); ?>
                                </div>
                                <div class="small-4 columns">
                                    <?php echo $this->get_publication_selector(); ?>
                                </div>
                            </div>
                        </div>

                        <!-- email parameters -->
                        <div class="row">
                            <div class="small-12 columns">
                                <div class="small-4 columns">
                                    <input id="email_subject" type="text" placeholder="Email Subject" />
                                    <p id="editor_error_subject" class="email_editor_error">Enter an email subject</p>
                                </div>
                                <div class="small-4 columns">
                                    <input id="email_from_email" type="text" placeholder="From: email address" />
                                    <p id="editor_error_from_email" class="email_editor_error">Enter a From email address</p>
                                </div>
                                <div class="small-4 columns">
                                    <input id="email_from_name" type="text" placeholder="From: Name" />
                                    <p id="editor_error_from_name" class="email_editor_error">Enter a From name</p>
                                </div>
                            </div>
                        </div>

                        <!-- email content area -->
                        <div class="row">
                            <div class="small-12 columns">
                                <textarea id="email_content"></textarea>
                                <p id="editor_error_content" class="email_editor_error">Enter email content</p>
                            </div>
                        </div>

                        <!-- email content area -->
                        <div class="row">
                            <div class="small-10 columns">
                            </div>
                            <div class="small-2 columns text-right">
                                <button id="email_confirm" class="small">Confirm</button>
                            </div>
                        </div>

                    </div> <!-- end #email-editor-interface -->

                    <div id="email-confirmation" class="row">
                        <div class="small-12 columns">
                            <p class="preview">Name: <span id="email_preview_name" class="preview"></span></p>
                            <p class="preview">Type: <span id="email_preview_type" class="preview"></span></p>
                            <p class="preview">Pub: <span id="email_preview_pub" class="preview"></span></p>
                            <p class="preview">From Email: <span id="email_preview_from_email" class="preview"></span></p>
                            <p class="preview">From Name: <span id="email_preview_from_name" class="preview"></span></p>
                            <p class="preview">Subject: <span id="email_preview_subject" class="preview"></span></p>
                            <iframe id="email_preview"></iframe>

                            <div id="profiles_code_wrap">
                                <p id="profiles_code_instructions" class="preview"><strong>Email Code:</strong> To send a test email, add the param <strong><em>"test": true</em></strong></p>
                                <p id="profiles_code_marketing" class="preview">
                                    <strong>Email Code:</strong> Add this code to the &lt;head&gt; of your confirmation pages, below the <strong>order.js</strong> file. <br><br>
                                    <strong>IMPORTANT:</strong> The order form confirmation page must have an order description containing the "termcode" in this exact format;
                                    <br/>
                                    <strong>termcode:79AR-LIR</strong> (choice code and then pubcode separated with a dash and no spaces)
                                </p>
                                <textarea id="profiles_code"></textarea>                                
                            </div>

                            <div class="row">
                                <div class="small-10 columns">
                                    <button id="confirm_edit" name="edit_email" class="secondary small">Edit</button>
                                </div>
                                <div class="small-2 columns">
                                    <button id="email_save" class="small">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!--   end #email-admin-editor -->

            </div> <!--   end .small-12.columns -->
        </div> <!--   end .row -->

        <?php
    }

    /**
     * get the email types and create a drop down menu
     * @return string $html
     */
    private function get_email_type_selector() {

        $html = '';
        $html .= '<select name="email_type" class="email_selector">';

        foreach ($this->email_types as $key => $value) {
            $selected = ($key === 'order_confirmation') ? 'selected="selected"' : '';
            $html .= '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
        }

        $html .= '</select>';

        return $html;
    }

    /**
     * get the publications and create a drop down menu
     * @return string $html
     */
    private function get_publication_selector() {

        $this->pubs = array_values(array_merge($this->sites, $this->pubs));

        $html = '';
        $html .= '<select name="email_pub" class="email_selector">';

        for ($i = 0; $i < count($this->pubs); $i++) {
            $html .= '<option value="' . $this->pubs[$i]['pubcode'] . '" >' . $this->pubs[$i]['pubcode'] . '</option>';
        }

        $html .= '</select>';

        return $html;
    }
}
