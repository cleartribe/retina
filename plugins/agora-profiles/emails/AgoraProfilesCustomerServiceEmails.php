<?php

class AgoraProfilesCustomerServiceEmails  {

    private $emails;
    private $profiles_client;
    private $pubs;

    private $email_types = array(
        'order_confirmation' => 'Order Confirmation',
    );

    public function __construct(Agora_profiles_emails $emails, Agora_Profiles_Client $client)
    {
        $this->pubs = $client->get_cached_publications();
        $this->hooks();
    }

    private function hooks()
    {
        add_action('admin_menu', function() {
            add_submenu_page('cs-ts-admin', 'Send Order Confirmation Emails', 'Send Order Confirmation Emails', 'read', 'cs-ts-emails', function() {
                $this->sendEmailsHtml();
            });
        });

        add_action('admin_footer', function() {
            if (isset($_GET['page']) && $_GET['page'] == 'cs-ts-emails') {
                echo('
                    <script>
                        var agora_publications = ' . json_encode($this->pubs) . '; 
                        ProfilesAdmin.tempOrder();
                    </script>
                ');
            }
        });
    }

    public function sendEmailsHtml()
    {
        ?>
            <div id="temp-account-interface" class="row">
                <div class="small-12 columns">

                    <h3>Create A Temp Account And Send Order Confirmation Email</h3>
                    <br><br>

                    <div class="row">
                        <div class="small-3 columns">
                            <p class="input-descrp">Select Pub Code</p>
                        </div>
                        <div class="small-4 end columns">
                            <?php echo $this->get_publication_selector(); ?>                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-3 columns">
                            <p class="input-descrp">Customer Email Address</p>
                        </div>
                        <div class="small-4 end columns">
                            <input id="temp-email" type="text" placeholder="Email">
                        </div>
                    </div>
                    <div id="existing-creds" class="row">
                        <div class="small-3 columns">
                            <p class="input-descrp">Customer Logins</p>
                        </div>
                        <div id="creds-wrapper" class="small-9 columns"></div>
                    </div>
                    <div class="row">
                        <div class="small-3 columns">
                            <p class="input-descrp">Publication Cost</p>
                        </div>
                        <div class="small-4 end columns">
                            <input id="temp-order" type="text" placeholder="$49">
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-3 columns">
                            <p class="input-descrp">Select Email</p>
                        </div>
                        <div class="small-4 columns">
                            <select name="email_name" class="email_selector"></select>
                        </div>
                        <div class="small-3 end columns">
                            <button id="view-email" class="tiny secondary temp-email-button">Preview Email</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-3 columns">
                            <p class="input-descrp">Select Premium Set</p>
                        </div>
                        <div class="small-4 columns">
                            <select name="premium_name" class="email_selector"></select>
                        </div>
                        <div class="small-3 end columns">
                            <button id="view-premium" class="tiny secondary temp-email-button">View Premium Set</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="small-12 columns">
                            <h3>Optional Fields</h3>
                        </div>
                    </div>

                    <div class="row">
                        <div class="small-3 columns">
                            <p class="input-descrp">Item Description</p>
                        </div>
                        <div class="small-4 end columns">
                            <span style="font-size:12px;"><em>Data will be shown in email's "Products" field with price</em></span>
                            <textarea name="itemdescription" id="itemdescription"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="small-7 end columns">
                            <p id="error-message" class="email_editor_error" style="text-align:right;">Enter the customer's email address publication cost</p>
                            <button id="create-temp-order" class="small">Create Temp Order & Send Email</button>
                        </div>
                    </div>

                </div> <!--   end .small-12.columns -->
            </div> <!--   end .row -->

            <!-- email/premium previewer -->
            <div class="reveal-modal-bg"></div>
            <div id="temp-data-preview" class="reveal-modal xlarge">
                <a id="close_email_editor" class="close-reveal-modal">×</a>
                <div id="confirm_temp_account" class="temp-data-preview">
                    <div id="confirm_temp_success" style="display:none;">
                        <h2>Success! The Temp Account Was Created</h2>
                        <p>Username: <strong><span id="confirm_temp_user"></span></strong></p>
                        <p>Password: <strong><span id="confirm_temp_pass"></span></strong></p>                        
                    </div>
                    <div id="confirm_temp_fail" style="display:none;">
                        <h2>Failed! We could not create the temp account</h2>
                        <p>You will need to create a temp account in Advantage</p>
                    </div>
                </div>
                <div id="premium_set_preview" class="temp-data-preview"></div>
                <iframe id="preview_email" class="temp-data-preview"></iframe>
            </div> <!--   end #email-admin-editor -->

        <?php
    }

    /**
     * get the publications and create a drop down menu
     * @return string $html
     */
    private function get_publication_selector() {

        $html = '';
        $html .= '<select name="email_pub" class="email_selector">';

        for ($i = 0; $i < count($this->pubs); $i++) {
            $selected = ($i === 0) ? 'selected="selected" ' : '';
            $html .= '<option value="' . $this->pubs[$i]['pubcode'] . '" ' . $selected . '>' . $this->pubs[$i]['pubcode'] . '</option>';
        }

        $html .= '</select>';

        return $html;
    }
}
