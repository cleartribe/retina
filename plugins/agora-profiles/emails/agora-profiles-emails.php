<?php

class Agora_profiles_emails {

	private $profiles_client;

	public function __construct( Agora_Profiles_Client $client ) {
		$this->profiles_client = $client;
	}

	/**
	 * send a single or multiple emails to profiles
	 *
	 * required:
	 * $email_data = array(
	 * 		'mc_id' => 12345 // mailing id, from mc.com?mailingid=17433
	 *   	'email' => 'foo@bar.com'
	 * );
	 *
	 * @param  array $email_data email and email options
	 * @return null
	 */
	public function send_email( $email_data ) {

		if ( ! isset( $email_data['mc_id'] ) || ! isset( $email_data['mc_id'] ) )
			exit( 'missing email data' );

		$this->profiles_client->profiles_request( $email_data, 'email/sendlist', TRUE );
	}
}
