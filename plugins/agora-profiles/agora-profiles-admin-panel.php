<?php

class Agora_profiles_admin_panel extends Agora_Profiles_Client {

    /**
     * flag for promos
     *
     * @var int
     * @since 1.0
     */
    public $is_promos_article = 0;

    /*
        number of posts to send to profiles for batch uploading
    */
    private $post_send_limit = 300;
    private $post_send_margin = 10;

    /*
        post types that we do not want to save to profiles
    */
    private $scrubs = array(
        // global
        'revision', 
        'attachment',
        'nav_menu_item',
        'forum',
        '_cf_snippet',

        // LFB 
        'shopping_cart', 
        'wpcf7_contact_form',
        'lfb-wg-posts', 
        'reply', 
        'shop_coupon', 
        'tablepress_table', 
        'topic', 
        'shop_order', 
        'tuckers-take', 
        'washington-whispers', 

        // Agora Financial
        'guest-author',
        'awqsf',
        'http',
        'af_product',

        // research
        'html-tools-page',
    );

    /*
        set time out for article uploader
        in seconds
    */
    private $article_timeout_seconds = 20;
    private $article_timeout_start = 0;

    public function __construct() {
        global $wpdb;
        $this->wpdb = $wpdb;
        $this->article_timeout_start = time();

        // Promo Specific
        $this->is_promos_article = (site_url() == 'http://research.agorafinancial.com/research') ? 1 : 0;

        $this->hooks();
    }

    /**
     * A method for invoiking all necessary WordPress hooks
     *
     * @since 1.0
     * 
     * @return null
     */
    public function hooks() {
        add_action('admin_init', array($this, 'hook_admin_init'));
        add_action('admin_menu', array($this, 'hook_admin_menu'));
        add_action('admin_head', array($this, 'hook_admin_head'));
    }

    public function hook_admin_init() {
        $this->save_profiles_single_article();
        $this->_save_profile_articles_bulk();
        $this->_save_js_includes();
        $this->_save_header_js_includes();
        $this->_save_profiles_js_init();
    }

    public function hook_admin_menu() {
        $this->admin_menus();
        wp_enqueue_script('afp-profiles-admin', AGORAPROFILESURL . '/js/profiles-admin.js', array('jquery'));
        wp_enqueue_style('afp-profiles-style', AGORAPROFILESURL . '/css/profiles-admin.css');
    }

    public function hook_admin_head() {
        $this->profiles_admin_inline_script();
    }

    public function save_profiles_single_article() {

        $pts = get_post_types();

        foreach($pts as $pt) {
            $test = 'publish_' . $pt;
            add_action($test, array($this, 'profiles_save_post'));
        }
    }

    /**
     * Hook in WordPress admin pages
     *
     * @uses agora-core
     * @return null
     */
    public function admin_menus() {
        add_menu_page(__( 'Profiles Settings', 'af-profiles-settings' ), __( 'Profiles', 'af-profiles-settings' ), 'administrator', 'af-profiles-settings', array($this, 'settings_html'), plugin_dir_url( __FILE__ ) . '/images/af-icon.png', "1.5");
    }

    public function profiles_admin_inline_script() {

        $script = '<script>';
        $script .=  'var siteUrl = "' . site_url() . '/";' . PHP_EOL;
        $script .=  'ProfilesAdmin.config({' . PHP_EOL;
        $script .=      '"profilesEndPoint": "' . $this->get_profiles_endpoint_url() . '",' . PHP_EOL;
        $script .=      '"pubFetchEndPoint": "' . $this->get_profiles_request_url() . '/pubs/get_publications.php' . '",' . PHP_EOL;
        $script .=      '"campaignFetchEndPoint": "' . $this->get_profiles_request_url() . '/campaigns/get_campaigns.php' . '"' . PHP_EOL;
        $script .=  '})';
        $script .= '</script>';

        echo $script;
    }

    /**
     * Admin HTML page for Settings
     *
     * @since 1.0
     * 
     * @return null
     */
    public function settings_html() {

        $articles_nonce = wp_create_nonce('profiles_article_uploader');
        $js_nonce = wp_create_nonce('profiles_js_include');
        $profiles_init_nonce = wp_create_nonce('profiles_js_init_include');

        echo '<div class="wrap">';
            echo '<h2>Profiles Settings</h2>';
            echo '<div class="metabox-holder">';
                echo '<div class="post-box-container column-1 normal">';
                    echo '<div id="normal-sortables" class="meta-box-sortables">';
                        echo '<div class="postbox">';
                            
                            echo '<h3 class="hndle"><span>Profiles Bulk Upload</span></h3>';
                            echo '<div class="inside">';
                                echo '<p>Use the tools below to populate the Profiles database.</p>';

                                echo '<table class="form-table">';
                            
                                    echo '<tr>';
                                        echo '<th scope="row">';
                                            echo 'Bulk Article Uploader';
                                        echo '</th>';
                                        echo '<td>';
                                            echo '<form action="" method="post">';
                                                echo sprintf('<input type="hidden" name="profiles_article_uploader" value="%s">', $articles_nonce);
                                                echo '<input type="submit" class="button-secondary" name="submit" value="Engage" />';
                                            echo '</form>';
                                        echo '</td>';
                                    echo '</tr>';
                                    

                                echo '</table>';
                            echo '</div>';
                        echo '</div>';
                    echo '</div>';
                echo '</div>';
            echo '</div>';

            $scripts = get_option('header-profiles-js');
            echo '<div class="metabox-holder">';
                echo '<div class="post-box-container column-1 normal">';
                    echo '<div id="normal-sortables" class="meta-box-sortables">';
                        echo '<div class="postbox">';
                            
                            echo '<h3 class="hndle"><span>Header Javascript</span></h3>';
                            echo '<div class="inside">';
                                echo '<p>Javascript blocks to be included in wp_head().</p>';
                                echo '<table class="form-table">';
                                    echo '<tr>';
                                        echo '<td>';
                                            echo '<form action="" method="post">';
                                                echo sprintf('<textarea name="header-profiles-js" style="width:99.5%%; height: 300px;" rows="10">%s</textarea>', stripslashes($scripts));
                                                echo sprintf('<input type="hidden" name="profiles_js_include" value="%s">', $js_nonce);
                                                echo '<input type="submit" class="button button-primary" name="submit" value="Save" />';
                                            echo '</form>';
                                        echo '</td>';
                                    echo '</tr>';
                                echo '</table>';
                            echo '</div>';
                        echo '</div>';
                    echo '</div>';
                echo '</div>';
            echo '</div>';

            $scripts = get_option('af-profiles-js');
            echo '<div class="metabox-holder">';
                echo '<div class="post-box-container column-1 normal">';
                    echo '<div id="normal-sortables" class="meta-box-sortables">';
                        echo '<div class="postbox">';
                            
                            echo '<h3 class="hndle"><span>Footer Javascript</span></h3>';
                            echo '<div class="inside">';
                                echo '<p>Javascript blocks to be included in wp_footer().</p>';
                                echo '<table class="form-table">';
                                    echo '<tr>';
                                        echo '<td>';
                                            echo '<form action="" method="post">';
                                                echo sprintf('<textarea name="af-profiles-js" style="width:99.5%%; height: 300px;" rows="10">%s</textarea>', stripslashes($scripts));
                                                echo sprintf('<input type="hidden" name="profiles_js_include" value="%s">', $js_nonce);
                                                echo '<input type="submit" class="button button-primary" name="submit" value="Save" />';
                                            echo '</form>';
                                        echo '</td>';
                                    echo '</tr>';
                                echo '</table>';
                            echo '</div>';
                        echo '</div>';
                    echo '</div>';
                echo '</div>';
            echo '</div>';

            $is_profiles_js = get_option('af-profiles-init-js');
            if ($is_profiles_js) {
                $is_profiles_js = 'checked';
            }
            else {
                $is_profiles_js = '';
            }
            echo '<div class="metabox-holder">';
                echo '<div class="post-box-container column-1 normal">';
                    echo '<div id="normal-sortables" class="meta-box-sortables">';
                        echo '<div class="postbox">';
                            echo '<h3 class="hndle"><span>Do You Want To Automatically Add The Profiles Javascript?</span></h3>';
                            echo '<div class="inside">';
                                echo '<p>Toggling this on means you want the profiles config, afid and reader logger to be automatically added.</p>';
                                echo '<table class="form-table">';
                                    echo '<tr>';
                                        echo '<td>';
                                            echo '<form action="" method="post">';
                                                echo '<input type="checkbox" name="af-profiles-init-js" '.$is_profiles_js.'>Yes automatically add the profiles tracking script <br /><br />';
                                                echo sprintf('<input type="hidden" name="profiles_js_init_include" value="%s">', $profiles_init_nonce);
                                                echo '<input type="submit" class="button button-primary" name="submit" value="Save" />';
                                            echo '</form>';
                                        echo '</td>';
                                    echo '</tr>';
                                echo '</table>';
                            echo '</div>';
                        echo '</div>';
                    echo '</div>';
                echo '</div>';
            echo '</div>';
        echo '</div>';


        echo '<div class="metabox-holder">';
            echo '<div class="post-box-container column-1 normal">';
                echo '<div class="meta-box-sortables">';
                    echo '<div class="postbox">';
                        
                        echo '<h3 class="hndle"><span>Publications</span></h3>';
                        echo '<div class="inside">';
                                echo '<input id="afpgetpubs" type="submit" class="button button-primary" value="Get Publications" />';
                        echo '</div>';
                    echo '</div>';
                echo '</div>';
            echo '</div>';
        echo '</div>';

        echo '<div class="metabox-holder">';
            echo '<div class="post-box-container column-1 normal">';
                echo '<div class="meta-box-sortables">';
                    echo '<div class="postbox">';
                        
                        echo '<h3 class="hndle"><span>Campaigns</span></h3>';
                        echo '<div class="inside">';
                                echo '<input id="afpgetcmps" type="submit" class="button button-primary" value="Get Campaigns" />';
                        echo '</div>';
                    echo '</div>';
                echo '</div>';
            echo '</div>';
        echo '</div>';
    }

    /**
     * Saves JS to database from admin screen
     *
     * @since 1.0.1
     * 
     * @return null
     */
    public function _save_js_includes() {

        if(!isset($_POST['af-profiles-js']))
            return false;

        if(!wp_verify_nonce($_POST['profiles_js_include'], 'profiles_js_include'))
            return false;

        if(isset($_POST['af-profiles-js']) && $_POST['af-profiles-js'] != false && $_POST['af-profiles-js'] != null && strlen($_POST['af-profiles-js']) > 0)
            update_option('af-profiles-js', $_POST['af-profiles-js']);
        else
            delete_option('af-profiles-js');
    }

    /**
     * Saves JS to database from admin screen
     *
     * @since 1.0.1
     * 
     * @return null
     */
    public function _save_header_js_includes() {

        if(!isset($_POST['header-profiles-js']))
            return false;

        if(!wp_verify_nonce($_POST['profiles_js_include'], 'profiles_js_include'))
            return false;

        if(isset($_POST['header-profiles-js']) && $_POST['header-profiles-js'] != false && $_POST['header-profiles-js'] != null && strlen($_POST['header-profiles-js']) > 0)
            update_option('header-profiles-js', $_POST['header-profiles-js']);
        else
            delete_option('header-profiles-js');
    }

    /**
     * Saves profiles automatic add JS to database from admin screen
     *
     * @since 1.0.1
     * 
     * @return null
     */
    public function _save_profiles_js_init() {
        
        if (!isset($_POST['profiles_js_init_include'])) 
            return false;

        if(!wp_verify_nonce($_POST['profiles_js_init_include'], 'profiles_js_init_include'))
            return false;

        if(isset($_POST['af-profiles-init-js']) && $_POST['af-profiles-init-js'] != false && $_POST['af-profiles-init-js'] != null && strlen($_POST['af-profiles-init-js']) > 0) {
        
            update_option('af-profiles-init-js', $_POST['af-profiles-init-js']);

        } else {
            delete_option('af-profiles-init-js');
        }
    }

    /**
     * save new or updated articles to the profiles API
     * on add_action save_post
     *
     * @since 1.0
     * @author Nate Martin
     * 
     * @param int $post_id
     * @return boolean|null
     */
    public function profiles_save_post($post_id) {

        $post = (array) get_post($post_id);

        if (!isset($post['post_type']) || in_array($post['post_type'], $this->scrubs))
            return FALSE;

        $post_data = array(array(
            'post_name' => $post['post_name'],
            'post_date' => $post['post_date'],
            'domain' => $this->get_profile_site_url($post),
            'url' => $this->get_profile_permalink($post, (int) $post_id),
            'ID' => $post_id,
            'post_id' => $post_id,
            'is_promo' => FALSE,
            'pub_code' => $this->get_pub_code($post, site_url()),
            'post_type' => $post['post_type'],
            'author_id' => $post['post_author']
        ));

        $post_data = $this->query_select_all_taxonomies($post_data);
        $post_data = $this->set_required_articles_keys($post_data);

        $response = $this->profiles_request($post_data, '/articles/articledata');
    }

    /**
     * get pub code from various sources.
     * use the $method flag to determine which method
     * you want to use to get the pub code
     *
     * @author Nate Martin
     * 
     * @since 1.0
     * @param array || object article data or whatever you need
     * @param string method flag
     * @return string
     */
    private function get_pub_code($article_data, $method) {

        $pub_code = '';

        switch($method) {

            case 'http://research.agorafinancial.com/research' :
                $pub_code = explode('_', $article_data['post_name']);
                $pub_code = (count($pub_code) === 3) ? $pub_code[0] : '';
                break;
            case 'http://agorafinancial.com'                    :
                $post_id = $article_data['ID'];
                $pub_code = get_post_meta($post_id, '_pubcode', true);
                break;
            default                                             :
                break;
        }

        return $pub_code;
    }

    /**
     * A method to bulk save taxonomy data to Profiles API
     *
     * @since 1.0
     * 
     * @return boolean|object
     */
    public function _save_profile_articles_bulk() {

        if(! isset($_POST['profiles_article_uploader']))
            return false;

        if(! wp_verify_nonce($_POST['profiles_article_uploader'], 'profiles_article_uploader'))
            return false;

        $response = $this->load_articles();
    }

    /*
        required keys to be sent to profiles

        post_name
        post_date
        domain
        url
        post_id
        is_promo
        pub_code
        post_type
        author_id
        tags

        @param
        @return
    */
    public function load_articles() {

        // get total posts for entire WP install
        $total_posts = $this->query_select_count_total_posts();
        $total_posts = isset($total_posts[0]['total_posts']) ? (int) $total_posts[0]['total_posts'] : 0;

        $option_name = 'afp_batch_post_upload_nextpost';

        $next_post = (int) get_option($option_name, 0);

        // increment next_post to exact number if at total post limit
        $next_post = (($next_post + $this->post_send_limit) > $total_posts)
            // if next post is over limit
            ? $next_post - ($total_posts - $next_post - $this->post_send_margin)
            : $next_post;
        // check if negative
        $next_post = ($next_post < 0) ? 0 : $next_post;

        $posts = $this->query_select_all_posts($next_post);

        $posts = $this->remove_post_types($posts);
        $posts = $this->query_select_all_taxonomies($posts);
        $posts = $this->set_required_articles_keys($posts);

        $response = $this->profiles_request($posts, '/articles/articledata');
        $response = json_decode($response, TRUE);

        // check if php is about to timeout (around 20 seconds)
        if (time() - $this->article_timeout_start > $this->article_timeout_seconds) {
            echo '<h2>Uploader has timed out. Click engage or refresh page</h2>';
            return FALSE;
        }

        /*
            if we have reached the limit and can stop sending to profiles
            and delete the options record
        */
        if ($next_post + $this->post_send_limit + $this->post_send_margin > $total_posts) {
            delete_option($option_name);
            echo '<h2>Articles Have Finished Loading</h2>';
            // done, don't send anymore articles
            return FALSE;
        }

        if ($next_post > 0) {
            // increment
            $next_post = $next_post + $this->post_send_limit;
        } else {
            // set new, default value
            $next_post = $this->post_send_limit;
        }
        
        update_option($option_name, $next_post);

        // loop to continue sending articles
        if ($response !== NULL) {
            $this->load_articles();
        }
    }

    public function query_select_count_total_posts() {

        $query = $this->wpdb->get_results(
            "
            SELECT count(ID) AS total_posts
            FROM wp_posts
            ",
            ARRAY_A
        );

        return $query;
    }

    private function query_select_all_posts($next_post) {

        $query = $this->wpdb->get_results(
            $this->wpdb->prepare(
                "
                SELECT ID, post_author, post_date, post_name, post_type
                FROM wp_posts
                ORDER BY id
                LIMIT %d, %d
                ",
                $next_post,
                $this->post_send_limit
            ),
            ARRAY_A
        );

        return $query;
    }

    /*
        remove unwanted posts, or posts that are not articles
        @param array article data
        @return array article data
    */
    private function remove_post_types($posts) {

        for ($i = 0; $i < count($posts); $i++) {

            for ($j = 0; $j < count($this->scrubs); $j++) {
                
                if ($posts[$i]['post_type'] == $this->scrubs[$j]) {
                    // remove post item
                    unset($posts[$i]);
                    // re dim array
                    $posts = array_values($posts);
                    // rollback
                    $i--;

                    break;
                }
            }
        }

        return $posts;
    }

    /*
        select taxonomies for each article
        @param array article data
        @return array article data
    */
    private function query_select_all_taxonomies($posts) {

        $taxonomies = array_merge($this->wp_default_taxonomies, $this->custom_taxonomies);

        // iterate over each post to get taxanomy for each post
        for ($i = 0; $i < count($posts); $i++) {

            foreach($taxonomies as $tax) {

                $terms[$tax] = array();

                // query terms based on post id and tag name
                $query = wp_get_object_terms((int) $posts[$i]['ID'], $tax);
                
                if (count($query) === 0)
                    continue;

                // iterate over each set of terms and store in terms array
                foreach ($query as $key) {
                    if (isset($key->name))
                        $terms[$tax][] = $key->name;
                }

            }

            $posts[$i]['tags'] = json_encode($terms);
        }

        return $posts;
    }
    /*
        required keys to be sent to profiles

        post_name
        post_date
        domain
        url
        post_id
        is_promo
        pub_code
        post_type
        author_id
        tags

        @param array article data
        @return array article data
    */
    private function set_required_articles_keys($posts) {

        for ($i = 0; $i < count($posts); $i++) {

            $posts[$i]['post_id'] = $posts[$i]['ID'];
            $posts[$i]['domain'] = $this->get_profile_site_url($posts[$i]);
            $posts[$i]['url'] = $this->get_profile_permalink($posts[$i], $posts[$i]['ID']);
            $posts[$i]['is_promo'] = $this->is_promos_article;
            $posts[$i]['pub_code'] = $this->get_pub_code($posts[$i], site_url());

            if (isset($posts[$i]['post_author']))
                $posts[$i]['author_id'] = $posts[$i]['post_author'];

            // get rid of unessary keys
            if (isset($posts[$i]['post_author']))
                unset($posts[$i]['post_author']);

            if (isset($posts[$i]['ID']))
                unset($posts[$i]['ID']);
        }

        return $posts;
    }

    /*
        substititue for site_url()
        this is for handling special cases like research.lfb.org

        @param object post
        @return string domain
    */
    public function get_profile_site_url($post) {

        $domain = site_url();

        if ($this->is_promos_article) {

            $pub_code = $this->get_pub_code($post, $domain);

            if ($pub_code == 'lfb' || $pub_code == 'lfl' || $pub_code == 'iii') {
                $domain = preg_replace('/research.agorafinancial.com/', 'research.lfb.org', $domain);           
            }
            
            $pattern = array(
                '#https://#',
                '#http://#',
            );

            $replacement = array(
                '',
                '',
            );

            $domain = preg_replace($pattern, $replacement, $domain);

            // remove the "research" directory
            $domain = trim($domain, '/');
            $domain = explode('/', $domain);

            // this should just return the primary domain
            if (isset($domain[1]) && $domain[1] === 'research') {
                $domain = $domain[0];

            // if research directory does not exist, return back to normal
            } else {
                $domain = implode('/', $domain);
            }
        }

        return $domain;
    }

    /*
        substititue for get_permalink()
        this is for handling special cases like research.lfb.org

        @param array post data
        @param int post id
        @return string domain
    */
    public function get_profile_permalink($post, $post_id) {

        $permalink = get_permalink($post_id);

        if ($this->is_promos_article) {

            $pub_code = $this->get_pub_code($post, site_url());

            if ($pub_code == 'lfb' || $pub_code == 'lfl' || $pub_code == 'iii') {
                $permalink = preg_replace('/research.agorafinancial.com/', 'research.lfb.org', $permalink);         
            }
        }

        return $permalink;
    }
}
