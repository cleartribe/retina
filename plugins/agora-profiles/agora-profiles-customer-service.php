<?php

// require first as other classes extend it
require_once(AGORAPROFILESDIR . 'emails/agora-profiles-emails.php');

// require all classes in emails folder
foreach (glob(AGORAPROFILESDIR . 'customerService/*.php') as $filename) {
    if(is_file($filename))
        require_once($filename);
}

// require all classes in emails folder
foreach (glob(AGORAPROFILESDIR . 'emails/*.php') as $filename) {
    if(is_file($filename))
        require_once($filename);
}

// only initialize for admin panel
if (is_admin()) {
    $profiles_client = new Agora_Profiles_Client();
    $profiles_emails = new Agora_profiles_emails($profiles_client);

    // create admin
    new Agora_profiles_emails_admin($profiles_emails, $profiles_client);
}