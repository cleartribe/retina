<?php

if (!class_exists('Agora_Profiles_Client'))
    require_once('agora-profiles-client.php');

class Agora_profiles_publications extends Agora_Profiles_Client {

    private $command = '';
    private $execute = '';
    private $pubs_folder = '';

    public function __construct() {
        parent::__construct();

        $this->command = 'wget -qO- ' . $this->get_profiles_request_url() . '/pubs/get_publications.php &> /dev/null';
        $this->execute = '0 14 * * * ' . $this->command;
        $this->pubs_folder = $this->get_profiles_plugin_path() . 'pubs/';
    }

    public function start_cronjob_pubs() {

        $this->cron_job('start', $this->execute);
    }

    /*
        return true or false if pubs.js file was successfully written
        @param null
        @return boolean
    */
    public function get_publications_from_profiles() {

        $pubs = $this->profiles_request('', 'publications/alldata', false);

        $json = json_decode($pubs, true);

        if ($json === null)
            return false;

        if (!isset($json['pubs']))
            return false;

        // creat pubs txt file
        file_put_contents($this->pubs_folder . 'pubs.txt', $pubs);

        // clear any cached value of the pubs.txt file
        $cache = new \plugins\agoraprofiles\Cache();
        $cache->set('pubs.txt', $pubs, 60 * 60 * 24 * 28);

        // create javascript file
        $js_pubs = 'var agora_financial_pubs = ' . $pubs . ';';
        $result = file_put_contents($this->pubs_folder . 'pubs.js', $js_pubs);
            
        return ($result > 0) ? true : false ;
    }
}
