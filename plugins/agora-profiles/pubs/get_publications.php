<?php


require_once( '../agora-profiles-publications.php' );

$afp_publications = new Agora_profiles_publications();
$result = $afp_publications->get_publications_from_profiles();

header('Content-Type: application/json');

echo json_encode( array( $result ) );