<?php

class Agora_profiles_openx extends Agora_Profiles_Client  {

	static $variables = array();

	public static function openx_variables( $variables ) {
		self::$variables[] = $variables;
	}

	public function openx_script( $data ) {

		$zoneid = isset( $data['zoneid'] ) ? $data['zoneid'] : -1;

		$js = '<div data-profile-event-name="openx">';
		$js .= '<script>' . PHP_EOL;
			$js .= "var m3_u = (location.protocol=='https:'?'https://ads.agorafinancial.com/www/delivery/ajs.php':'http://ads.agorafinancial.com/www/delivery/ajs.php');" . PHP_EOL;
			$js .= "var m3_r = Math.floor(Math.random()*99999999999);" . PHP_EOL;
			$js .= "if (!document.MAX_used) document.MAX_used = ',';" . PHP_EOL;
				$js .= "document.write (\"<scr\"+\"ipt type='text/javascript' src='\"+m3_u);" . PHP_EOL;
				$js .= $this->set_variables( $data ) . PHP_EOL;
				$js .= "document.write ('&amp;cb=' + m3_r + '&');" . PHP_EOL;
				$js .= "if (document.MAX_used != ',') document.write (\"&amp;exclude=\" + document.MAX_used);" . PHP_EOL;
				$js .= "document.write (\"&amp;loc=\" + escape(window.location));" . PHP_EOL;
				$js .= "if (document.referrer) document.write (\"&amp;referer=\" + escape(document.referrer));" . PHP_EOL;
				$js .= "if (document.context) document.write (\"&context=\" + escape(document.context));" . PHP_EOL;
				$js .= "if (document.mmm_fo) document.write (\"&amp;mmm_fo=1\");" . PHP_EOL;
				$js .= "document.write (\"'><\/scr\"+\"ipt>\");" . PHP_EOL;
		$js .= "</script>" . PHP_EOL;
		$js .= "<noscript>" . PHP_EOL;
			$js .= "<a href='http://ads.agorafinancial.com/www/delivery/ck.php?n=145920&amp;cb=743d71391db5d69318a06d948320fd32' target='_blank'>" . PHP_EOL;
				$js .= sprintf( "<img src='http://ads.agorafinancial.com/www/delivery/avw.php?zoneid=%d&amp;cb=743d71391db5d69318a06d948320fd32&amp;n=145920' border='0' alt='' />", $zoneid ) . PHP_EOL;
			$js .= "</a>" . PHP_EOL;
		$js .= "</noscript>" . PHP_EOL;
		$js .= "</div>";

		return $js;
	}

	private function set_variables( $data ) {

		$zoneid = isset( $data['zoneid'] ) ? $data['zoneid'] : -1;
		$pubs = isset( $data['pubs'] ) ? $data['pubs'] : -1;
		// general hard coded variables
		$js = '';
		$js .= sprintf( "document.write (\"?zoneid=%d\");", $zoneid ) . PHP_EOL;
		$js .= ( isset( $_COOKIE['r'] ) && $_COOKIE['r'] == 'milo' ) ? 'document.write("&r=milo");' . PHP_EOL : '';
		$js .= ( isset( $_COOKIE['cls'] ) && $_COOKIE['cls'] >= 1 ) ? 'document.write("&cls=' . esc_attr($_COOKIE['cls']) . '");' . PHP_EOL : '';
		
        if (isset($_COOKIE['sourceid'] ) && !isset( $_GET['sourceid'])) {
            $js .= 'document.write("&sourceid=' . esc_attr($_COOKIE['sourceid']) . '");' . PHP_EOL;
        } elseif (isset($_GET['sourceid'])) {
            $js .= 'document.write("&sourceid=' . esc_attr($_GET['sourceid']) . '");' . PHP_EOL;
        }

        if ($pubs) {
            $js .= 'document.write("&pubs=' . esc_attr($pubs) . '");' . PHP_EOL;
        }


		// variables set by openx_variables() from other classes
		for ( $i = 0; $i < count( self::$variables ); $i++ ) {
					
			foreach ( self::$variables[$i] as $key => $value ) {
				$js .= 'document.write("&' . $key . '=' . $value . '");' . PHP_EOL;
			}
		}

		return $js;
	}
}