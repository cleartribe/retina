<?php

class Agora_profiles_cronjobs {

	private $pubs_folder = '';

	public function __construct() {

		$this->pubs_folder = AGORAPROFILESDIR . 'pubs/';
	}

	public function start_cronjob( $cron_command ) {

		// check if crontab is set
		$existing_cron = shell_exec( 'crontab -l' );

		$save_crontab = FALSE;

		// no cron is set, meaning no string was returned by shell_exec
		if ( $existing_cron === NULL ) {
			$save_crontab = TRUE;

		// if some other existing cron jobs exist
		} else if ( strlen( $existing_cron ) > 0 ) {

			if ( preg_match( '/' . preg_quote( $cron_command, '/' ) . '/', $existing_cron ) ) {
				$save_crontab = FALSE;
			} else {
				$save_crontab = TRUE;
				$cron_command = $existing_cron . PHP_EOL . $cron_command;
			}
		}

		if ( $save_crontab ) {
			file_put_contents( $this->pubs_folder . 'crontab.txt', $cron_command . PHP_EOL );
			shell_exec( 'crontab < ' . $this->pubs_folder . 'crontab.txt' );
		}
	}

	/*
		a method for deleteing this specific cronjob
		we must use this method instead of straight delete
		so we don't delete other jobs
	*/
	public function delete_cronjob( $cron_command ) {

		// check if crontab is set
		$existing_cron = shell_exec( 'crontab -l' );

		$save_crontab = FALSE;

		// no cron is set, meaning no string was returned by shell_exec
		if ( strlen( $existing_cron ) > 5 ) {

			// check if this cronjob is set
			if ( preg_match( '/' . preg_quote( $cron_command, '/' ) . '/', $existing_cron ) ) {

				// if so, remove it
				$existing_cron = preg_replace( '/' . preg_quote( $cron_command, '/' ) . '/', '', $existing_cron );
				$save_crontab = TRUE;
				$cron_command = $existing_cron . PHP_EOL;
			}
		}

		if ( $save_crontab ) {
			file_put_contents( $this->pubs_folder . 'crontab.txt', $cron_command . PHP_EOL );
			shell_exec('crontab < ' . $this->pubs_folder . 'crontab.txt');
		}
	}
}