<?php
/**
 * A class for handling user event tracking to profiles
 *
 * @package agora-profiles-client
 * @author Nate Martin
 * @author TJ Tate
 * @version 1.0
 */
class Agora_profiles_post_editor extends Agora_Profiles_Client {

	public function __construct() {

		$this->hooks();
	}

	public function hooks() {
		
		add_action( 'edit_form_after_editor', array( $this, 'wp_post_editor_init' ) );
	}

	public function wp_post_editor_init( $post ) {
		$post_tags = $this->get_custom_post_taxonomies( $post->ID );
		$this->print_custom_post_tag_script( $post_tags );
	}

	private function get_custom_post_taxonomies( $post_id ) {

		$taxonomies = array_merge( $this->wp_default_taxonomies, $this->custom_taxonomies );

		$tags = array();

		foreach( $taxonomies as $tax ) {

			// query terms based on post id and tag name
			$tags[] = wp_get_object_terms( $post_id, $tax );
		}

		$tags = $this->organize_tags_by_type( $tags );

		return $tags;
	}

	private function print_custom_post_tag_script( $post_tags ) {

		$taxonomies = array();
		$html = '';

		foreach ( $post_tags as $taxonomy => $tags ) {

			$taxonomies[] = strtolower( $taxonomy );

			// create comma seperated list of tags for each taxonomy type
			$tag_group = array();

			for ( $i = 0; $i < count( $tags ); $i++ ) {
				$tag_group[$i] = $tags[$i];
			}

			$tag_group = implode( ',', $tag_group );

			// add tag group to taxonomy hidden input
			$html .= '<input type="hidden" name="tax_input[' . $taxonomy . ']" value ="' . $tag_group . '">';
		}

		$script = '<script>var af_taxonomy_types = ' . json_encode( $taxonomies ) . ';</script>';
		$html = $script . $html;

		echo $html;
	}

	/*
		the wp_get_object_terms will return error messages outside of
		normal tag data structure, so must do some bullshit to deal with it

		@param array tags data
		@return array tags data
	*/
	private function organize_tags_by_type( $tags ) {

		// organize tags by tag type
		$tag_types = $this->custom_taxonomies;

		// initialize tag group to store tags by type
		for ( $i = 0; $i < count( $tag_types ); $i++ ) {
			$tag_group[$tag_types[$i]] = array();
		}

		for ( $i = 0; $i < count( $tags ); $i++ ) {

			// cast to array due to error inconsistencies
			$tags[$i] = (array) $tags[$i];

			for ( $j = 0; $j < count( $tags[$i] ); $j++ ) {

				// must test for every attribute
				if ( isset( $tags[$i][$j] ) && isset( $tags[$i][$j]->taxonomy ) ) {
					$type = $tags[$i][$j]->taxonomy;

					// now test to see if taxonomy type is a "profiles custom taxonomy"
					if ( isset( $tag_group[$type] ) ) {
						// finally add the damn tag to group
						$tag_group[$type][] = $tags[$i][$j]->name;
					}
				}
			}					
		}

		return $tag_group;
	}
}
