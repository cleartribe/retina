<?php

class AgoraProfilesCustomerServiceAdmin  {

    private $cs_slugs = array (
        'cs-ts-admin',
        'cs-ts-emails',
        'cs-ts-generic-emails',
        'cs-ts-webviews'
    );

    public function __construct(Agora_Profiles_Client $client)
    {
        $this->hooks();
    }

    private function hooks()
    {
        add_action('admin_menu', array($this, 'hookAdminMenu'));
        add_action('admin_menu', array($this, 'enqueueScripts'));
    }

    public function enqueueScripts()
    {
        add_action('admin_head', function() {

            // for Identifying the Admin and sending event data to GA
            echo "
                <script>
                    var current_wp_user_id = " . get_current_user_id() . ";
                </script>
            ";

            // random projects GA Account
            echo "
                <script>
                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                    ga('create', 'UA-3366695-17', 'auto');
                    ga('send', 'pageview');
                </script>
            ";
        });

        wp_enqueue_script('jquery-ui', 'http://code.jquery.com/ui/1.11.4/jquery-ui.js', array('jquery'));
        wp_enqueue_script('afp-profiles-admin', AGORAPROFILESURL . '/js/profiles-admin.js', array('jquery'));

        // this will mess up other styles, so only include on email admin page
        if (isset($_GET['page']) && in_array($_GET['page'], $this->cs_slugs)) {
            wp_enqueue_style('jquery-ui', 'http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
            wp_enqueue_style('foundation-css', AGORAPROFILESURL . '/css/foundation.min.css');
            wp_enqueue_style('profiles-admin-email', AGORAPROFILESURL . '/css/profiles-admin-customer-service.css', array('foundation-css'));
        }
    }

    public function hookAdminMenu()
    {
        add_menu_page('Customer Service', 'Customer Service', 'read', 'cs-ts-admin', function() {
            $this->getCustomerServiceHtml();
        }, 'dashicons-businessman', '10.7');
    }

    public function getCustomerServiceHtml()
    {
        echo('
            <div class="row">
                <div class="small-12 columns">
                    <h4 class="cs-ts-h4">Send order confirmation emails to customers</h4>
                    <p><a class="button" href="/wp-admin/admin.php?page=cs-ts-emails">Send Order Confirmation Emails</a></p>
                    <p><a class="button" href="/wp-admin/admin.php?page=cs-ts-generic-emails">Send Generic Emails</a></p>
                    <h4 class="cs-ts-h4">View a customer\'s web interactions with Agora Financial websites and emails</h4>
                    <p><a class="button" href="/wp-admin/admin.php?page=cs-ts-webviews">View Web & Email History</a></p>
                </div>
            </div>
        ');
    }
}
