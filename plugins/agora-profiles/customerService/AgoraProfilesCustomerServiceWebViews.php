<?php

class AgoraProfilesCustomerServiceWebViews  {

    private $profiles_client;

    public function __construct(Agora_Profiles_Client $client)
    {
        $this->pubs = $client->get_cached_publications();
        $this->hooks();
    }

    private function hooks()
    {
        add_action('admin_menu', function() {
            add_submenu_page('cs-ts-admin', 'User Web Views', 'User Web Views', 'read', 'cs-ts-webviews', function() {
                $this->getAdminHtml();
            });
        });

        add_action('admin_footer', function() {
            if (isset($_GET['page']) && $_GET['page'] == 'cs-ts-webviews') {
                echo('
                    <script>
                        ProfilesAdmin.webViews();
                    </script>
                ');
            }
        });
    }

    public function getAdminHtml()
    {
        ?>
            <div class="row">
                <div class="small-4 end columns">
                    <h3>View A Customer's Agora Financial Website Interaction History</h3>
                    <p>Using a customer's email address, you can view which pages and emails a customer has viewed.</p>
                    <p><strong>Enter an email address or Advantage ID</strong></p>
                    <input type="text" id="web-view-email" value="">
                    <div class="row">
                        <div class="small-6 columns">
                            <input id="date_start" type="text" placeholder="Start Date" value="">
                        </div>
                        <div class="small-6 columns">
                            <input id="date_end" type="text" placeholder="End Date" value="">
                        </div>
                    </div>
                    <button id="web-view-get" class="small">Get History</button>
                </div>
                <div class="small-8 end columns">
                    <div class="row" style="margin-top:30px;">
                        <div class="small-12 columns"><p><strong>Content Tags</strong></p></div>
                        <div id="web-view-tags" class="small-12 columns"></div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns"><p><strong>Operating System & Browser</strong></p></div>
                        <div id="web-view-devices" class="small-12 columns"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <button id="show-customer-interactions" class="tiny web-views-tab secondary">Customer Interactions</button>
                    <button id="show-customer-details" class="tiny web-views-tab secondary">Account Details</button>
                </div>
                <div id="web-view-response" class="small-12 columns"></div>
                <div id="account-details-response" class="small-12 columns"></div>
            </div>
            <div id="loading-icon-wrapper">
                <img id="web-view-loading-icon" src="<?php echo AGORAPROFILESURL; ?>/images/loading-arrows.gif" alt="">
            </div>
        <?php
    }
}
