<?php
/*
Plugin Name: Agora Profiles
Description: A WordPress plugin to integrate with the Agora Financial Profiles project
Version: 1.0.2
Author: Nate Martin, TJ Tate
*/
if(!defined('ABSPATH'))
    die;

define('AGORAPROFILESDIR', plugin_dir_path(__FILE__));
define('AGORAPROFILESURL', 'https://'.$_SERVER['HTTP_HOST'].'/wp-content/plugins/agora-profiles');

require_once(AGORAPROFILESDIR . 'Cache.php');

require_once(AGORAPROFILESDIR . 'agora-profiles-functions.php');

// add rewrites that are specific to Profiles
require_once(AGORAPROFILESDIR . 'AgoraProfilesRewrites.php');
new AgoraProfilesRewrites();

require_once(AGORAPROFILESDIR . 'AgoraProfilesPluginInit.php');
new AgoraProfilesPluginInit();

require_once(AGORAPROFILESDIR . 'AgoraProfilesAjaxRequests.php');
new AgoraProfilesAjaxRequests();

// somethings we must initialize before loading agora-profiles-client
if (is_admin()) {
    require_once(AGORAPROFILESDIR . 'agora-profiles-cronjobs.php');
}

// Add the customer service Role/User
require_once(AGORAPROFILESDIR . 'AgoraProfilesRoles.php');
new AgoraProfilesRoles();

require_once(AGORAPROFILESDIR . 'agora-profiles-client.php');
require_once(AGORAPROFILESDIR . 'agora-profiles-customer-service.php');
require_once(AGORAPROFILESDIR . 'agora-profiles-admin-panel.php');
require_once(AGORAPROFILESDIR . 'agora-profiles-post-editor.php');
require_once(AGORAPROFILESDIR . 'agora-profiles-user-tracking.php');
require_once(AGORAPROFILESDIR . 'agora-profiles-taxonomies.php');
require_once(AGORAPROFILESDIR . 'agora-profiles-publications.php');
require_once(AGORAPROFILESDIR . 'agora-profiles-campaigns.php');
require_once(AGORAPROFILESDIR . 'agora-profiles-openx.php');
require_once(AGORAPROFILESDIR . 'agora-profiles-premium-sets.php');

// initialize client first, all other classes rely on it
$profiles_client = new Agora_Profiles_Client();
new Agora_profiles_taxonomies();

// only initialize for admin panel
if (is_admin()) {
    
    // inits must remain in this order
    new Agora_Profiles_cronjobs();
    new Agora_profiles_admin_panel();
    new Agora_profiles_post_editor();
    new Agora_profiles_premium_sets();

    $is_profiles_admin = isset($_GET['page']) ? (($_GET['page'] == 'af-profiles-settings') ? true : false) : false;

    if ($is_profiles_admin) {
        // initialize cron jobs here, as we don't want them in other static requests
        $afp_publications = new Agora_profiles_publications();
        $afp_publications->start_cronjob_pubs();

        $afp_campaigns = new Agora_profiles_campaigns();
        $afp_campaigns->start_cronjob_campaigns();
    }

    $profiles_emails = new Agora_profiles_emails($profiles_client);

    // add customer service menu items and features
    new AgoraProfilesCustomerServiceAdmin($profiles_client);
    new AgoraProfilesCustomerServiceWebViews($profiles_client);
    new AgoraProfilesCustomerServiceEmails($profiles_emails, $profiles_client);
    new AgoraProfilesGenericEmails($profiles_emails, $profiles_client);
}

// must be called after class Agora_profiles_campaigns
$afp_openx = new Agora_profiles_openx();

// public / theme facing
new Agora_Profiles_User_Tracking();
//$afp_campaigns = new Agora_profiles_campaigns();
//$afp_campaigns->run_campaigns();

