<?php

class AgoraProfilesRoles {

    /**
     * customer service & telesales user role
     * @var string
     */
    private $cs_ts_role = 'af_cs_ts';

    /**
     * users who are allowed to see the wp-admin dashboard
     * @var array
     */
    private $allow_wp_admin = array(
        'moderate_comments',
    );

    public function __construct()
    {
        // add the CS/TS role to allow wp-admin roles
        $this->allow_wp_admin[] = $this->cs_ts_role;

        $this->hooks();
    }

    private function hooks()
    {
        add_action('admin_init', array($this, 'blockWpAdminAccess'));
        add_action('admin_init', array($this, 'setCustomRoles'));
    }

    public function blockWpAdminAccess()
    {
        if (defined('DOING_AJAX') && DOING_AJAX)
            return false;

        // if the current user can view the wp-admin, then do nothing
        for ($i = 0; $i < count($this->allow_wp_admin); $i++) {
            if (current_user_can($this->allow_wp_admin[$i]))
                return true;
        }

        // for all other users, block the wp-admin
        wp_redirect(home_url());
        exit;
    }

    public function setCustomRoles()
    {
        // this role is for Super users and subscribers
        // Users must be manually added to this role
        // remove_role($this->cs_ts_role);

        add_role($this->cs_ts_role, 'Customer Service / Telesales', array(
            'read' => true
        ));

    }
}