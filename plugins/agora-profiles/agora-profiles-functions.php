<?php

if (!function_exists('isCsRep')) {
    
    /**
     * These are the keys that are used to interpret the navigation link data that is stored
     * in the DB
     *
     * The purpose of this is to cut down on the amount of data being stored
     *
     * @return array
     */
    function isCsRep()
    {
        // for admin
        if (current_user_can('activate_plugins'))
            return true;

        return current_user_can('af_cs_ts');
    }
}

if (!function_exists('ipWhiteList')) {
    
    function ipWhiteList()
    {
        return in_array($_SERVER['REMOTE_ADDR'], array(
            // '127.0.0.1',    // dev
            '52.1.125.188', // profiles
            '98.129.41.40', // af
            '98.129.41.47', // dr
            '98.129.148.152',
            '198.101.190.232'
        ));
    }
}