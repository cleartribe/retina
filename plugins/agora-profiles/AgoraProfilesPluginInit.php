<?php

class AgoraProfilesPluginInit {

    public function __construct()
    {
        add_action('init', array($this, 'loadPluggable'));
        add_action('init', array($this, 'logoutUsers'));
                
    }

    public function loadPluggable()
    {
        if (!function_exists('wp_get_current_user'))
            require_once(ABSPATH . 'wp-includes/pluggable.php');
    }

    public function logoutUsers()
    {
        $user = wp_get_current_user();

        if (!isset($user->ID))
            return null;

        $logout = get_user_meta($user->ID, 'logout_user', true);

        // var_dump($logout);
        if (!$logout)
            return null;

        if ($logout != '1')
            return null;

        delete_user_meta($user->ID, 'logout_user');
        wp_logout();
        wp_redirect('/');
        exit;
    }
}