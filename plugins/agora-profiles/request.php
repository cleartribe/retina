<?php

require_once( 'agora-profiles-client.php' );

class AF_profiles_request extends Agora_Profiles_Client {

	public function __construct() {
		parent::__construct();

		$route = isset( $_POST['route'] ) ? $_POST['route'] : FALSE;
		$data = isset( $_POST['data'] ) ? $_POST['data'] : FALSE;

		if ( ! $route )
			exit( 'route data not set' );

		$this->request_route( $route, $data );
	}

	private function request_route( $route, $data ) {

		if ( $route == 'afid/identity' ) {
			$data = $this->profiles_afid_identity();
			$response = $this->profiles_request( $data, $route, FALSE );

		} else if ( $route == 'articles/readerlog' ) {
			$data = $this->profiles_article_readerlog();
			$response = $this->profiles_request( $data, $route );

		} else {
			$response = $this->profiles_request( $data, $route );
		}

		header('Content-type: application/json');
		echo $response;
	}

	private function profiles_afid_identity() {

		$afbsr = isset( $_POST['afbsr'] ) ? $_POST['afbsr'] : '';
		$afmsc = isset( $_POST['afmsc'] ) ? $_POST['afmsc'] : '';
		$afadv = isset( $_POST['afadv'] ) ? $_POST['afadv'] : '';

		$postdata = array(
		    'afbsr' => $afbsr,
		    'afmsc' => $afmsc,
		    'afadv' => $afadv
		);

		$data = array();

		foreach ( $postdata as $key => $value ) {

			if ( $value !== '' ) {
				$data[] = $key . '=' . $value . '&';
			}
		}

		$data = implode( '', $data );
		$data = trim( $data, '&' );

		return $data;
	}

	private function profiles_article_readerlog() {

		$data = isset( $_POST['data'] ) ? $_POST['data'] : '';
		$data['ip_address'] = isset( $_SERVER['REMOTE_ADDR'] ) ? $_SERVER['REMOTE_ADDR'] : 0;

		return $data;
	}
}

new AF_profiles_request();
