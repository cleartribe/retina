<?php
/**
 * A class for handling user event tracking to profiles
 *
 * @package agora-profiles-client
 * @author Nate Martin
 * @author TJ Tate
 * @version 1.0
 */
class Agora_Profiles_User_Tracking extends Agora_Profiles_Client {

	public $page_props_delimiter;

	/**
	 * Sets up the environment for the user tracking
	 *
	 * @since 1.0.2
	 * @author TJ Tate
	 * @return null
	 */
	public function __construct() {

		$this->page_props_delimiter = ',';

		$this->hooks();
	}

	/**
	 * A method for invoking all necessary WordPress hooks
	 *
	 * @since 1.0.2
	 * @author TJ Tate
	 * @return null
	 */
	public function hooks() {
		# Ajax Functions for retrieving AFID and associating articles with users for tracking
		add_action( 'wp_ajax_nopriv_user_profiles_request', array( $this, 'user_profiles_request' ) );
		add_action( 'wp_ajax_user_profiles_request', array( $this, 'user_profiles_request' ) );
		
		# Theme Support		
		add_action( 'wp_head', array( $this, 'set_js_logged_in_var' ));
		add_action( 'wp_footer', array( $this, 'global_js' ), 200 );
	}



	/**
	 * Sets the javascript variable if a user is logged in
	 *
	 * @since 1.0.2
	 * @author TJ Tate
	 * @return null
	 */
	public function set_js_logged_in_var () {		

		global $wp_query;

		$loggedin = $this->is_logged_in();
		$authors = $this->get_authors();
		$categories = $this->get_categories();
		$tags = $this->get_tags();
		$post_type = $this->get_post_type();
		$sentiment_tags = $this->get_sentiment_tags();

		$js = $this->get_client_defined_js('header-profiles-js');

		?>
		<script>

			<?php echo $js; ?>

			var afga_page_props = {
				is_logged_in : <?php echo $loggedin; ?>,
				tags : '<?php echo $tags; ?><?php echo $sentiment_tags; ?>',
				authors : <?php echo json_encode($authors); ?>,
				categories : <?php echo json_encode($categories); ?>,
				post_type : <?php echo json_encode($post_type); ?>
			}
		</script>
		<?php 
	}


	public function get_authors () {
		global $wp_query;
		
		if (is_author()) {
			return $wp_query->queried_object->data->display_name;
		}

		if (is_search() || is_page() || is_home() || is_category() || is_archive() || is_404() || get_query_var( 'topics' ) || 'got' == get_post_type() )
			return '';

		$authors = '';
	   
    	if (isset($wp_query->queried_object->post_author)) {
			$authors = get_the_author_meta('display_name', $wp_query->queried_object->post_author);
		}
					
			return $authors;
	}


	public function get_categories () {
		global $wp_query;
		//all categories
		
		if (is_author() || is_page() || is_home() || is_search() || get_query_var( 'faq-category' ) || get_query_var( 'faq-search' ))
			return '';

		if (is_category() || get_query_var( 'topics' ))
			return $wp_query->queried_object->name;

		$categories = '';

		if ('product' == get_post_type() && !is_archive()) {
			global $product;
      		$children = $product->get_children();
      		$sample_id = $children[0];
      		$sample = new WC_Product_Simple( $sample_id );
			$sample_terms = get_the_terms( $sample_id, 'product_cat' );

			foreach ($sample_terms as $sample_term) {
				if(strpos($sample_term->name,"'") !== false) {
					$categories .= addslashes($sample_term->name) . $this->page_props_delimiter;
				}
				else {
					$categories .= $sample_term->name . $this->page_props_delimiter;
				}
			}
		}

		if (isset($wp_query->queried_object)) {
		
			$category_IDs = wp_get_post_categories($wp_query->queried_object->ID);

			if ($category_IDs != null) {
				

				$categories = '';
				foreach ($category_IDs as $category_ID) {
					if(strpos(get_cat_name($category_ID),"'") !== false) {
						$categories .= addslashes(get_cat_name($category_ID) . $this->page_props_delimiter);
					}
					else {
						$categories .= get_cat_name($category_ID) . $this->page_props_delimiter;
					}
				}
			}
			
			$topic_IDs =  wp_get_post_terms($wp_query->queried_object->ID, 'topics');

			if ($topic_IDs != null) {
			
				//if there are no errors 
				if ( isset( $topic_IDs->errors ) && ! $topic_IDs->errors ) {
					foreach ($topic_IDs as $topic_ID) {
						if(strpos($topic_ID->name,"'") !== false) {
							$categories .= addslashes($topic_ID->name . $this->page_props_delimiter);
						}
						else {
							$categories .= $topic_ID->name . $this->page_props_delimiter;
						}
					}
				}
			}
		}

		return $categories;
	}


	public function get_tags () {
		global $wp_query;
		//all tags

		if (is_author() || is_search() || is_home() || is_category() || get_query_var( 'topics' ) || get_query_var( 'faq-category' ) || get_query_var( 'faq-search' ))
			return '';

		$tags = '';

		if (isset($wp_query->queried_object)) {

			$tag_IDs = get_the_tags($wp_query->queried_object->ID);

			if ($tag_IDs != null) {
				foreach ($tag_IDs as $tag_ID) {
					if(strpos($tag_ID->name,"'") !== false) {
						$tags.= 'tags:'.addslashes($tag_ID->name . $this->page_props_delimiter);
					}
					else {
						$tags.= 'tags:'.$tag_ID->name . $this->page_props_delimiter;
					}
				}
				
			}

		}
		
		return $tags;
		
	}

	/**
	 * Get the Sentiment tags for a post.
	 * 
	 * Get the Sentiment tags for a post. Sentiment tags are a way of tagging content across all of Agora Financial campaigns, promos, and content.
	 * 
	 * @author De'Yonte W.<dwilkinson@agorafinancial.com>
	 * @since 1-21-2015
	 * @return string Separated list of Sentiment tags combilned with a delimiter to separate the tags.
	 */
	public function get_sentiment_tags () {
		global $wp_query;

		if (is_author() || is_search() || is_home() || is_category() || get_query_var( 'topics' ) || get_query_var( 'faq-category' ) || get_query_var( 'faq-search' ))
			return '';

		$sentiment_tags = '';

		if (isset($wp_query->queried_object)) {
			// loop through each sentiment custom taxonomy
			foreach( $this->custom_taxonomies as $sentiment_taxonomny ){
				$tag_IDs = get_the_terms( $wp_query->queried_object->ID, $sentiment_taxonomny );

				if ($tag_IDs != false && !$tag_IDs instanceof Wp_Error) {
					foreach ($tag_IDs as $tag_ID) {
						if(strpos($tag_ID->name,"'") !== false) {
							$sentiment_tags.= 'sent:'.addslashes($tag_ID->name . $this->page_props_delimiter);
						}
						else {
							$sentiment_tags.= 'sent:'.$tag_ID->name . $this->page_props_delimiter;	
						}
					}
					
				}
			}

		}
		
		return $sentiment_tags;
		
	}


	public function get_post_type () {
		global $post;

		if (is_home())
			return '';

		if (is_404())
			return '404';

		if (is_search())
			return 'search';

		if (is_category())
			return 'category';

		if (is_author())
			return 'author';
		
		if (get_query_var( 'topics' ))
			return 'topic';

		return $post->post_type;
	}

	/**
	 * Checks to see if user has wordpress logged in cookie set
	 *
	 * @since 1.0.2
	 * @author TJ Tate
	 * @return boolean
	 */
	public function is_logged_in () {
		if ( is_user_logged_in() ) {
			return 'true';
		}
		else {
			return 'false';
		}        
	}

	/**
	 * Prints saved JS from admin and prints other 
	 *
	 * @since 1.0.2
	 * 
	 * @author TJ Tate
	 * @return null
	 */
	public function global_js() {
        $client_domain = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : 'agorafinancial.com';
		$is_profiles_js = get_option( 'af-profiles-init-js' );
		$isSSL = is_ssl();
		$scheme = ( $isSSL ) ? 'https' : 'http';
        $profiles_js_filename = defined( 'AF_PROFILES_JS_FILENAME' ) ? AF_PROFILES_JS_FILENAME : 'profiles.compressed.js';
		$profiles_script = defined( 'AF_PROFILES_API_URL' )
			? AF_PROFILES_API_URL . 'js/' . $profiles_js_filename
			: $scheme . '://profiles.agorafinancial.com/js/' . $profiles_js_filename;

		// hot fix to overwrite to add https to AF_PROFILES_API_URL
		if ( $isSSL ) {
			$profiles_script = preg_replace( '/http\:/', 'https:', $profiles_script );
		}

		?>

		<script>
            var profiles_client = '<?php echo $client_domain; ?>';
            var profiles_server = '<?php echo (defined("AF_PROFILES_API_URL") ? AF_PROFILES_API_URL : "profiles.agorafinancial.com"); ?>';


			//initialize google analytics external script
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			/*	load profiles.compressed.js
				asynchronously load profiles script incase server is slow
			*/
			(function() {
				var ce = document.createElement('script'),
					s = document.getElementsByTagName('script')[0],
					version = (typeof profilesVersion != 'undefined') ? profilesVersion : '1.0.0';

				ce.type = 'text/javascript';
				ce.async = true;
				version = '?v=' + version;
				ce.src = '<?php echo $profiles_script; ?>' + version;

				if (ce.addEventListener) {
					ce.addEventListener("load", function() { fire_profiles(); }, false);					
				} else if (ce.attachEvent) {
					ce.attachEvent("onload", function() { fire_profiles(); });					
				}

				function fire_profiles() {
					if (typeof profiles_script_callback != 'undefined' ){
						profiles_script_callback();						
					}
				}

				s.parentNode.insertBefore(ce, s);					
			})();

			function profilesIsReady(callback) {
				var sto = {},
					polltries = 0,
					profilesReady = false;

				function pollProfiles(callback) {

					sto = window.setTimeout(function(){
						if (typeof Profiles == 'object' || polltries === 300) {
							callback();
							// shut down settimeout
							window.clearTimeout(sto);
							sto = null;
							return;
						}
						polltries++;
						pollProfiles(callback);
					}, 10);
				}

				pollProfiles(callback);
			}

            <?php if ($is_profiles_js): ?>
            	// must load profile data in callback
	        	function profiles_script_callback() {
	            	Profiles.config({
		                'ajaxurl': '<?php echo $this->get_profiles_endpoint_url(); ?>',
		                'wpaction': 'user_profiles_request',
		                'wpnonce': '<?php echo wp_create_nonce( "aT55VfiH3UUrZXAZ" ) ?>'
	                });
	                Profiles.getID({
	                    'afadv.save': false
	                });
	                Profiles.load({
	                	'campaignTarget': {
	                		'request': true
	                	},
	                	'readerLog': {}
	                });

	                <?php if ( isset( $_COOKIE['afadv'] ) ): ?>
	                	Profiles.subscriptions();
	                <?php endif; ?>

			        <?php echo $this->get_client_defined_js('af-profiles-js'); ?>

					<?php // profiles notifications need to be defined down here below the above get_options() echo ?>
					if (typeof profilesGetNotifications != 'undefined' && profilesGetNotifications) {
						Profiles.notifications();						
					}
	        	}
            <?php endif; ?>

         
        </script>

    <?php
	}

	private function get_client_defined_js($js) {
		$js = get_option($js);
		$js = ( $js == '' || $js == false || $js == null ) ? '' : $js;
		
		// this is being echoed inside script tags, so don't add script tags
		$pattern = array(
			'#<script>#',
			'#</script>#',
		);

		$replacement = array(
			'',
			'',
		);

		$js = preg_replace( $pattern, $replacement, $js );
		return stripslashes( $js );;
	}
}